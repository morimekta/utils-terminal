package net.morimekta.terminal;

import net.morimekta.io.tty.TTYMode;
import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ArgParser;
import net.morimekta.terminal.args.Argument;
import net.morimekta.terminal.args.annotations.ArgIgnore;
import net.morimekta.terminal.args.annotations.ArgOptions;
import net.morimekta.terminal.progress.DefaultSpinners;
import net.morimekta.terminal.progress.Progress;
import net.morimekta.terminal.progress.ProgressManager;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static net.morimekta.terminal.args.ArgHelp.argHelp;

public class ProgressTester {
    @ArgOptions(shortChar = 'h', usage = "Show program usage.")
    public boolean   help;
    @ArgIgnore
    public List<URI> urls = new ArrayList<>();

    public void addUrl(String url) {
        try {
            urls.add(URI.create(url));
        } catch (Exception e) {
            throw new ArgException("Failed to add URL: " + url, e);
        }
    }

    private String fileName(URI uri) {
        return Path.of(uri.getPath()).getFileName().toString();
    }

    public void run(String[] args) {
        var parser = ArgParser
                .argParser("pget", "0", "Parallel 'wget'")
                .generateArgs(this)
                .add(Argument.argument("url", "URLs to download", this::addUrl)
                             .repeated())
                .build();
        try {
            parser.parse(args);
            if (help) {
                argHelp(parser).printHelp(System.out);
                return;
            }
        } catch (ArgException e) {
            argHelp(parser).printSingleLineUsage(System.err);
            System.err.println();
            e.printStackTrace(System.err);
            System.exit(1);
        }

        try (Terminal term = new Terminal(TTYMode.RAW)) {
            List<Future<String>> out;
            try (var pm = new ProgressManager(term, DefaultSpinners.ASCII, 3)) {
                var httpClient = HttpClient.newHttpClient();
                out = urls.stream()
                          .map(url -> {
                              var fileName = Paths.get(fileName(url));

                              return pm.addTask("GET " + fileName, task -> {
                                  var request = HttpRequest.newBuilder().GET().uri(url).build();
                                  var response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());
                                  if (response.statusCode() == 200) {
                                      var contentLength = response.headers()
                                                                  .firstValueAsLong("Content-Length")
                                                                  .orElse(0);
                                      byte[] buffer = new byte[64 * 1024];
                                      int b;
                                      long downloaded = 0;
                                      try (var body = response.body();
                                           var os = new BufferedOutputStream(
                                                   Files.newOutputStream(fileName, CREATE, TRUNCATE_EXISTING))) {
                                          while ((b = body.read(buffer)) != -1) {
                                              os.write(buffer, 0, b);
                                              downloaded += b;
                                              if (contentLength > 0) {
                                                  task.onNext(new Progress(
                                                          Math.min(downloaded, contentLength),
                                                          contentLength));
                                              } else {
                                                  task.onNext(new Progress(0, contentLength));
                                              }
                                          }
                                          task.onComplete();
                                          return "Task: " + url + " completed " + downloaded + " bytes";
                                      } catch (IOException e) {
                                          task.onError(e);
                                          task.onComplete();
                                          return "Task: " + url + " failed: " + e.getMessage();
                                      }
                                  } else {
                                      String content;
                                      try (var body = response.body()) {
                                          content = new String(body.readAllBytes(), UTF_8);
                                      }
                                      task.onError(new IOException(content));
                                      task.onComplete();
                                      return "Task: " + url + " failed: " + response.statusCode();
                                  }
                              });
                          })
                          .collect(Collectors.toList());
                pm.waitAbortable();
            }
            out.forEach(it -> {
                try {
                    term.lp().println(it.get());
                } catch (Exception e) {
                    e.printStackTrace(term.printStream());
                }
            });
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public static void main(String... args) {
        new ProgressTester().run(args);
    }
}
