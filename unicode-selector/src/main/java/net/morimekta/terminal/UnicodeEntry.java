package net.morimekta.terminal;

import net.morimekta.collect.UnmodifiableList;
import net.morimekta.strings.chr.Char;
import net.morimekta.strings.chr.Unicode;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.lang.Character.isHighSurrogate;
import static java.lang.Character.isSurrogate;
import static java.lang.String.format;
import static net.morimekta.strings.StringUtil.rightPad;

public class UnicodeEntry {
    private final List<Unicode> unicode;
    private final String        out;

    public UnicodeEntry(int codepointOffset, int num) {
        List<Unicode> unicode = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            unicode.add(Unicode.unicode(codepointOffset + i));
        }
        this.unicode = UnmodifiableList.asList(unicode);
        var builder = new StringBuilder();
        for (var u : this.unicode) {
            String cs = Unicode.LTR_OVERRIDE + u.toString() + Unicode.LTR_OVERRIDE;

            // Actual control chars.
            if (u.codepoint() < 0x100 && u.printableWidth() == 0) {
                cs = "<*>";
            }

            builder.append("| ");
            if (u.codepoint() < 0x10000) {
                builder.append(format(
                        "0x%04x:  %s  %s",
                        u.codepoint(),
                        rightPad("'" + u.asString() + "'", 8),
                        rightPad(
                                isSurrogate((char) u.codepoint())
                                ? (isHighSurrogate((char) u.codepoint())
                                   ? "<H" + format("%03x", u.codepoint() - 0xd800) + ">"
                                   : "<L" + format("%03x", u.codepoint() - 0xdc00) + ">")
                                : "\"" + cs + "\"",
                                8)));
            } else {
                builder.append(format(
                        "0x%08x:  %s  %s",
                        u.codepoint(),
                        rightPad("'" + u.asString() + "'", 14),
                        rightPad("\"" + cs + "\"", 8)));
            }
        }
        builder.append('|');
        this.out = builder.toString();
    }

    @Override
    public String toString() {
        return out;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UnicodeEntry)) return false;
        UnicodeEntry that = (UnicodeEntry) o;
        return Objects.equals(unicode, that.unicode);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(unicode);
    }

    public void print(PrintStream out) {
        out.println();
        for (Char c : unicode) {
            String cs = Unicode.LTR_OVERRIDE + c.toString() + Unicode.LTR_OVERRIDE;

            // Actual control chars.
            if (c.codepoint() < 0x100 && c.printableWidth() == 0) {
                cs = "<*>";
            }

            if (c.codepoint() < 0x10000) {
                out.format(" - 0x%04x: %s  %s",
                           c.codepoint(),
                           rightPad("'" + c.asString() + "'", 8),
                           rightPad("\"" + cs + "\"", 8));
            } else {
                out.format(" - 0x%08x: %s  %s",
                           c.codepoint(),
                           rightPad("'" + c.asString() + "'", 8),
                           rightPad("\"" + cs + "\"", 8));
            }
            out.println();
        }
    }
}
