package net.morimekta.terminal;

import net.morimekta.strings.chr.Char;
import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ArgHelp;
import net.morimekta.terminal.args.ArgParser;
import net.morimekta.terminal.args.annotations.ArgOptions;
import net.morimekta.terminal.selection.Selection;
import net.morimekta.terminal.selection.SelectionReaction;

public class UnicodeSelector {
    @ArgOptions(shortChar = 'S', usage = "Unicode id to start")
    public int     start = 0;
    @ArgOptions(shortChar = 'L', usage = "Number of codes to show")
    public int     limit = 0x4000;
    @ArgOptions(shortChar = 'h', usage = "Show help info")
    public boolean help;

    public void run(String[] args) {
        try {
            var parser = ArgParser
                    .argParser("unicode-browser", "", "")
                    .generateArgs(this)
                    .parse(args);
            if (help) {
                ArgHelp.argHelp(parser).build().printHelp(System.out);
                return;
            }
            parser.validate();
        } catch (ArgException e) {
            if (e.getParser() != null) {
                ArgHelp.argHelp(e.getParser()).build().printPreamble(System.err);
                System.err.println();
                System.err.println(e.getMessage());
                System.exit(1);
                return;
            } else {
                throw e;
            }
        }

        try (var term = new Terminal()) {
            var width = term.tty().getTerminalSize().cols;
            var each = (width - 1) / (limit < 0x10000 ? 28 : 34);
            var source = new UnicodeEntrySource(limit, each);
            UnicodeEntry ue;
            try (var select = Selection
                    .newBuilder(source)
                    .terminal(term)
                    .initial(start / each)
                    .on(Char.CR, "select", (idx, e, sel) -> {
                        if (sel.confirm("Use this?")) {
                            return SelectionReaction.SELECT;
                        } else {
                            return SelectionReaction.STAY;
                        }
                    })
                    .on('q', "quit", SelectionReaction.EXIT)
                    .build()) {
                ue = select.runSelection();
            }
            if (ue != null) {
                term.printStream().println();
                ue.print(term.printStream());
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        new UnicodeSelector().run(args);
    }
}
