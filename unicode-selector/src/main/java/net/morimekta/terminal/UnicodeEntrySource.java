package net.morimekta.terminal;

import net.morimekta.terminal.selection.EntrySource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UnicodeEntrySource implements EntrySource<UnicodeEntry> {
    private final Map<Integer, UnicodeEntry> entries;
    private final int                        size;
    private final int                        each;

    public UnicodeEntrySource(int max, int each) {
        this.size = max;
        this.each = each;
        this.entries = new HashMap<>();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public UnicodeEntry get(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException();
        }
        return entries.computeIfAbsent(i, (k) -> new UnicodeEntry(i * each, each));
    }

    @Override
    public int indexOf(UnicodeEntry unicodeEntry) {
        return entries.entrySet()
                      .stream()
                      .filter(e -> e.getValue().equals(unicodeEntry))
                      .findFirst()
                      .map(Map.Entry::getKey)
                      .orElse(-1);
    }

    @Override
    public List<UnicodeEntry> load(int offset, int maxEntries) {
        List<UnicodeEntry> result = new ArrayList<>();
        if (offset < 0 || offset >= size) {
            throw new IndexOutOfBoundsException(offset + " * " + maxEntries + " of " + size);
        }
        int endOffset = Math.min(size, offset + maxEntries);
        for (int i = offset; i < endOffset; i++) {
            if (i < size) {
                result.add(get(i));
            } else {
                break;
            }
        }
        return result;
    }
}
