package net.morimekta.terminal.args.annotations;

import net.morimekta.terminal.args.ArgNameFormat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specify the argument naming rule for this class.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ArgNaming {
    /**
     * @return The argument name format.
     */
    ArgNameFormat value();
}
