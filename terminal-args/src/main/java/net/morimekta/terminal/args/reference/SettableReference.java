package net.morimekta.terminal.args.reference;

public interface SettableReference extends Reference {
    void set(Object o);
}
