package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

import java.math.BigInteger;

/**
 * A converter to big integer values.
 */
public class BigIntegerParser implements ValueParser<BigInteger> {
    @Override
    public BigInteger parse(String s) {
        try {
            if (s.startsWith("0x")) {
                return parseInternal(s.substring(2), 16);
            } else {
                return parseInternal(s, 10);
            }
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid big integer value %s", s, nfe);
        }
    }

    private BigInteger parseInternal(String s, int radix) {
        if (s.matches("^0+$")) {
            return new BigInteger("0");
        }
        s = s.replaceAll("^0+", "");
        return new BigInteger(s, radix);
    }
}
