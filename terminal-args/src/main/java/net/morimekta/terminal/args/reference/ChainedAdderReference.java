package net.morimekta.terminal.args.reference;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.function.Supplier;

public class ChainedAdderReference implements AdderReference {
    private final SettableReference            chained;
    private final Class<?>                     itemType;
    private final Supplier<Collection<Object>> mkCollection;

    public ChainedAdderReference(
            SettableReference chained,
            Class<?> itemType,
            Supplier<Collection<Object>> mkCollection) {
        this.chained = chained;
        this.itemType = itemType;
        this.mkCollection = mkCollection;
    }

    @Override
    public void add(Object value) {
        @SuppressWarnings("unchecked")
        var collection = (Collection<Object>) chained.get();
        if (collection == null) {
            collection = mkCollection.get();
            chained.set(collection);
        }
        collection.add(value);
    }

    @Override
    public Class<?> getItemType() {
        return itemType;
    }

    @Override
    public String getName() {
        return chained.getName();
    }

    @Override
    public String getUsage() {
        return "Add value to " + getName();
    }

    @Override
    public Object get() {
        return chained.get();
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotation) {
        return chained.isAnnotationPresent(annotation);
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotation) {
        return chained.getAnnotation(annotation);
    }

    @Override
    public Class<?> getType() {
        return chained.getType();
    }

    @Override
    public Type getGenericType() {
        return chained.getGenericType();
    }
}
