package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * A converter to file instances, with validator &amp; error message.
 */
public class DirParser implements ValueParser<Path> {
    @Override
    public Path parse(String s) {
        Path result = Paths.get(s);
        try {
            if (!Files.exists(result)) {
                throw new ArgException("No such directory %s", s);
            }
            var real = result.toAbsolutePath().toRealPath();
            if (!Files.isDirectory(real)) {
                throw new ArgException("%s is not a directory", s);
            }
        } catch (IOException e) {
            throw new ArgException("%s is not a valid path", s, e);
        }
        return result;
    }
}
