package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ValueParser;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * A converter to path values.
 */
public class PathParser implements ValueParser<Path> {
    @Override
    public Path parse(String s) {
        return Paths.get(s);
    }
}
