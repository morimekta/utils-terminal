package net.morimekta.terminal.args.reference;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public interface Reference {
    String getName();

    String getUsage();

    Object get();

    boolean isAnnotationPresent(Class<? extends Annotation> annotation);

    <A extends Annotation> A getAnnotation(Class<A> annotation);

    Class<?> getType();

    Type getGenericType();
}
