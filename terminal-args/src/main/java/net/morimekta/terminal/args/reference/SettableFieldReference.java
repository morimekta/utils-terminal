package net.morimekta.terminal.args.reference;

import net.morimekta.terminal.args.ArgException;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;

public class SettableFieldReference extends FieldReference implements SettableReference {
    public SettableFieldReference(
            Object instance,
            Field field,
            AccessibleObject annotationRef) {
        super(instance, field, annotationRef);
    }

    @Override
    public void set(Object o) {
        try {
            field.set(instance, o);
        } catch (IllegalAccessException e) {
            throw new ArgException(e.getMessage(), e);
        }
    }
}
