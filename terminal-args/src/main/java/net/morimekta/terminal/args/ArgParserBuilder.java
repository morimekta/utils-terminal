package net.morimekta.terminal.args;

import java.util.List;

/**
 * Base interface for building an argument parser.
 */
public interface ArgParserBuilder {
    /**
     * @return The built argument parser.
     */
    ArgParser build();

    /**
     * Build and parse arguments.
     *
     * @param args Argument list to parse.
     * @return The argument parser after parsing.
     */
    default ArgParser parse(List<String> args) {
        return build().parse(args);
    }

    /**
     * Build and parse arguments.
     *
     * @param args Arguments to parse.
     * @return The argument parser after parsing.
     */
    default ArgParser parse(String... args) {
        return build().parse(args);
    }
}
