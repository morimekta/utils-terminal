package net.morimekta.terminal.args;

import net.morimekta.strings.NamingUtil;
import net.morimekta.strings.StringUtil;

/**
 * Name formatting for arguments. This can modify how arguments are named
 * based on field or method names.
 */
public enum ArgNameFormat {
    /**
     * Name arguments like <code>--lisp-naming</code>.
     */
    LISP("-", NamingUtil.Format.LISP, false),
    /**
     * Name arguments like <code>--snake_casing</code>.
     */
    SNAKE("_", NamingUtil.Format.SNAKE, false),
    /**
     * Name arguments like <code>--camelCasing</code>.
     */
    CAMEL("", NamingUtil.Format.CAMEL, true);

    private final String            sep;
    private final NamingUtil.Format nameFormat;
    private final boolean           capitalizeName;

    ArgNameFormat(String sep, NamingUtil.Format nameFormat, boolean capitalizeName) {
        this.sep = sep;
        this.nameFormat = nameFormat;
        this.capitalizeName = capitalizeName;
    }

    /**
     * @param name The argument name (not including '--' ) to format.
     * @return Argument name formatted using specified rule.
     */
    public String format(String name) {
        return NamingUtil.format(name, nameFormat);
    }

    /**
     * @param prefix Argument prefix.
     * @param name The argument name (not including '--' ) to format.
     * @return Argument name formatted using specified rule.
     */
    public String join(String prefix, String name) {
        if (prefix.isEmpty()) {
            return name;
        }
        if (capitalizeName) {
            return prefix + sep + StringUtil.capitalize(name);
        } else {
            return prefix + sep + name;
        }
    }
}
