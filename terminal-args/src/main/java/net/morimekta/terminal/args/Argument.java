package net.morimekta.terminal.args;

import net.morimekta.terminal.args.impl.ArgumentImpl;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * An argument option for the arg parser.
 * <p>
 * Argument options are arguments set by its own value, no "-" prefix. Arguments
 * are the purest form of args that can be set.
 */
public interface Argument extends Arg {
    /**
     * Build an argument option.
     *
     * @param name   The argument name.
     * @param usage  Usage string for the argument.
     * @param setter Argument setter / applier.
     * @return The builder.
     */
    static Builder argument(
            String name,
            String usage,
            Consumer<String> setter) {
        return ArgumentImpl.argument(name, usage, setter);
    }

    /**
     * Builder for making argument options.
     */
    interface Builder extends Arg.Builder<Argument> {
        /**
         * @param value Default value for the argument.
         * @return The builder.
         */
        Builder defaultValue(Object value);

        /**
         * @param predicate Predicate to match so see if this argument applies
         *                  to string value.
         * @return The builder.
         */
        Builder when(Predicate<String> predicate);

        /**
         * @param pattern Value pattern for matching argument.
         * @return The builder.
         */
        Builder when(Pattern pattern);

        /**
         * @param pattern Value pattern to skip matching argument.
         * @return The builder.
         */
        Builder whenNot(Pattern pattern);

        /**
         * Argument can be repeated.
         *
         * @return The builder.
         */
        Builder repeated();

        /**
         * Argument is required.
         *
         * @return The builder.
         */
        Builder required();

        /**
         * Argument is hidden.
         *
         * @return The builder.
         */
        Builder hidden();
    }
}
