/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.impl;

import net.morimekta.terminal.args.Arg;

/**
 * Arguments are part of the args list that is not designated with an
 * option name. E.g. files to read etc. Since a single program can have
 * multiple arguments, it's handled as a 'first accepted'.
 * <p>
 * If the argument parsing hits a non '-' prefixed string, or has passed
 * the 'stop' option ('--'), it will try to parse it as an argument. It
 * will go through the available arguments until one has consumed it by
 * returning &gt; 0 on {@link Arg#apply(java.util.List)}.
 */
public abstract class BaseArg implements Arg {
    private final String  name;
    private final String  usage;
    private final String  defaultValue;
    private final boolean repeated;
    private final boolean required;
    private final boolean hidden;

    protected BaseArg(String name,
                      String usage,
                      String defaultValue,
                      boolean repeated,
                      boolean required,
                      boolean hidden) {
        this.name = name;
        this.usage = usage;
        this.defaultValue = defaultValue;
        this.repeated = repeated;
        this.required = required;
        this.hidden = hidden;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getUsage() {
        return usage;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isRepeated() {
        return repeated;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public boolean isHidden() {
        return hidden;
    }
}
