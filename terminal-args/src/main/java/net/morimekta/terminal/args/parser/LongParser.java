package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

/**
 * A converter to long values.
 */
public class LongParser implements ValueParser<Long> {
    @Override
    public Long parse(String s) {
        try {
            if (s.startsWith("0x")) {
                return Long.parseLong(s.substring(2), 16);
            } else if (s.startsWith("0")) {
                return Long.parseLong(s, 8);
            }
            return Long.parseLong(s);
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid long value %s", s, nfe);
        }
    }
}
