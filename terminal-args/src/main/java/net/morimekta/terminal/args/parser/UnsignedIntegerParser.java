package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

/**
 * A converter to unsigned integer values.
 */
public class UnsignedIntegerParser implements ValueParser<Integer> {
    @Override
    public Integer parse(String s) {
        try {
            if (s.startsWith("0x")) {
                return Integer.parseUnsignedInt(s.substring(2), 16);
            } else if (s.startsWith("0")) {
                return Integer.parseUnsignedInt(s, 8);
            }
            return Integer.parseUnsignedInt(s);
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid unsigned integer value %s", s, nfe);
        }
    }
}
