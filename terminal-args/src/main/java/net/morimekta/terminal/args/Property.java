package net.morimekta.terminal.args;

import net.morimekta.terminal.args.impl.PropertyImpl;

/**
 * A property argument definition.
 */
public interface Property extends Option {
    /**
     * @return The meta key name.
     */
    String getMetaKey();

    /**
     * Basic interface for putting values onto a map, properties or config.
     */
    @FunctionalInterface
    interface Putter {
        /**
         * Put the property into place.
         *
         * @param key   The property key.
         * @param value The property value.
         */
        void put(String key, String value);
    }

    /**
     * Builder for defining a property argument.
     */
    interface Builder extends Arg.Builder<Property> {
        /**
         * @param metaKey The meta key name.
         * @return The builder.
         */
        Builder metaKey(String metaKey);

        /**
         * @param metaVar The meta var name.
         * @return The builder.
         */
        Builder metaVar(String metaVar);

        /**
         * Set the property option as hidden.
         *
         * @return The builder.
         */
        Builder hidden();
    }

    /**
     * Instantiate a property with long name.
     *
     * @param name   Argument name for the property option.
     * @param usage  Usage string.
     * @param putter Property key-value putter.
     * @return The property builder.
     */
    static Builder propertyLong(String name, String usage, Putter putter) {
        return new PropertyImpl.BuilderImpl(name, null, usage, putter);
    }

    /**
     * Instantiate a property with long name.
     *
     * @param shortName Argument char for the property option.
     * @param usage     Usage string.
     * @param putter    Property key-value putter.
     * @return The property builder.
     */
    static Builder propertyShort(char shortName, String usage, Putter putter) {
        return new PropertyImpl.BuilderImpl(null, shortName, usage, putter);
    }

    /**
     * Instantiate a property with long name.
     *
     * @param name      Argument name for the property option.
     * @param shortChar Argument char for the property option.
     * @param usage     Usage string.
     * @param putter    Property key-value putter.
     * @return The property builder.
     */
    static Builder property(String name, char shortChar, String usage, Putter putter) {
        return new PropertyImpl.BuilderImpl(name, shortChar, usage, putter);
    }
}
