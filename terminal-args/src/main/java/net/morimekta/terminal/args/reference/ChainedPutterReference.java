package net.morimekta.terminal.args.reference;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.function.Supplier;

public class ChainedPutterReference implements PutterReference {
    private final SettableReference             chained;
    private final Class<?>                      keyType;
    private final Class<?>                      valueType;
    private final Supplier<Map<Object, Object>> mkMap;

    public ChainedPutterReference(
            SettableReference chained,
            Class<?> keyType,
            Class<?> valueType,
            Supplier<Map<Object, Object>> mkMap) {
        this.chained = chained;
        this.keyType = keyType;
        this.valueType = valueType;
        this.mkMap = mkMap;
    }

    @Override
    public void put(Object key, Object value) {
        @SuppressWarnings("unchecked")
        var map = (Map<Object, Object>) chained.get();
        if (map == null) {
            map = mkMap.get();
            chained.set(map);
        }
        map.put(key, value);
    }

    @Override
    public Class<?> getKeyType() {
        return keyType;
    }

    @Override
    public Class<?> getValueType() {
        return valueType;
    }

    @Override
    public String getName() {
        return chained.getName();
    }

    @Override
    public String getUsage() {
        return "Put value to " + getName();
    }

    @Override
    public Object get() {
        return chained.get();
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotation) {
        return chained.isAnnotationPresent(annotation);
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotation) {
        return chained.getAnnotation(annotation);
    }

    @Override
    public Class<?> getType() {
        return chained.getType();
    }

    @Override
    public Type getGenericType() {
        return chained.getGenericType();
    }
}
