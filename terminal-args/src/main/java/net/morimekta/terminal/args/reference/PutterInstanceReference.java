package net.morimekta.terminal.args.reference;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Map;

public class PutterInstanceReference implements PutterReference {
    private final String              name;
    private final Class<?>            keyType;
    private final Class<?>            valueType;
    private final Map<Object, Object> map;
    private final Reference           ref;

    public PutterInstanceReference(
            String name,
            Class<?> keyType,
            Class<?> valueType,
            Map<Object, Object> map,
            Reference annotationRef) {
        this.name = name;
        this.keyType = keyType;
        this.valueType = valueType;
        this.map = map;
        this.ref = annotationRef;
    }

    @Override
    public void put(Object key, Object value) {
        map.put(key, value);
    }

    @Override
    public Class<?> getKeyType() {
        return keyType;
    }

    @Override
    public Class<?> getValueType() {
        return valueType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getUsage() {
        return "Put value in " + getName() + ".";
    }

    @Override
    public Object get() {
        return map;
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotation) {
        return ref.isAnnotationPresent(annotation);
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotation) {
        return ref.getAnnotation(annotation);
    }

    @Override
    public Class<?> getType() {
        return map.getClass();
    }

    @Override
    public Type getGenericType() {
        return new ReferenceParameterizedType(map.getClass(), keyType, valueType);
    }
}
