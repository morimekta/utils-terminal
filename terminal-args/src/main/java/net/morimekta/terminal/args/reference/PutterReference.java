package net.morimekta.terminal.args.reference;

public interface PutterReference extends Reference {
    void put(Object key, Object value);

    Class<?> getKeyType();

    Class<?> getValueType();
}
