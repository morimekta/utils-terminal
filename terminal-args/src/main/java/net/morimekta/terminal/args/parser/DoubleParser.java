package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

/**
 * A converter to double values.
 */
public class DoubleParser implements ValueParser<Double> {
    @Override
    public Double parse(String s) {
        try {
            return Double.parseDouble(s);
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid double value %s", s, nfe);
        }
    }
}
