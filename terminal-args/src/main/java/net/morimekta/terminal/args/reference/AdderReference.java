package net.morimekta.terminal.args.reference;

public interface AdderReference extends Reference {
    void add(Object o);

    Class<?> getItemType();
}
