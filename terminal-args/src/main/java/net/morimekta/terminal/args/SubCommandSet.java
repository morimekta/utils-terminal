package net.morimekta.terminal.args;

import java.util.List;

/**
 * A set of subcommands with come helper methods and builder.
 *
 * @param <SubCommandDef> The type generic for the subcommand definition.
 */
public interface SubCommandSet<SubCommandDef> extends Arg {
    /**
     * @return Subcommands in the set ordered as declared.
     */
    List<SubCommand<? extends SubCommandDef>> getSubCommands();

    /**
     * @param name Name of subcommand to get.
     * @return The subcommand or null if not found.
     */
    SubCommand<? extends SubCommandDef> getSubCommandByName(String name);

    /**
     * @param name Name of subcommand to get argument parser for.
     * @return The argument parser for given subcommand.
     */
    ArgParser parserForSubCommand(String name);

    /**
     * Builder for subcommands.
     *
     * @param <SubCommandDef> The type generic for the subcommand definition.
     */
    interface Builder<SubCommandDef> extends ArgParserBuilder {
        /**
         * Make subcommand optional.
         *
         * @return The builder.
         */
        Builder<SubCommandDef> optionalCommand();

        /**
         * Set the default subcommand, if none is selected.
         *
         * @param name Name of default subcommand.
         * @return The builder.
         */
        Builder<SubCommandDef> defaultCommand(String name);

        /**
         * Add subcommand.
         *
         * @param subCommand The subcommand to add.
         * @return The builder.
         */
        Builder<SubCommandDef> add(
                SubCommand<? extends SubCommandDef> subCommand);

        /**
         * Add subcommand from builder.
         *
         * @param subCommand The subcommand to add.
         * @return The builder.
         */
        default Builder<SubCommandDef> add(
                SubCommand.Builder<? extends SubCommandDef> subCommand) {
            return add(subCommand.build());
        }
    }
}
