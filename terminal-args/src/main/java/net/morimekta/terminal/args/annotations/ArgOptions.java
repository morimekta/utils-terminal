package net.morimekta.terminal.args.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Standard argument options.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface ArgOptions {
    /**
     * @return The option flag name, not including the leading '--'.
     */
    String name() default "";

    /**
     * @return Short char to use for the option.
     */
    char shortChar() default '\0';

    /**
     * @return The usage string.
     */
    String usage() default "";
}
