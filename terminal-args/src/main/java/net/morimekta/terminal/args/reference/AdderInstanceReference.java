package net.morimekta.terminal.args.reference;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

public class AdderInstanceReference implements AdderReference {
    private final String             name;
    private final Class<?>           itemType;
    private final Collection<Object> collection;
    private final Reference          annotationRef;

    public AdderInstanceReference(
            String name,
            Class<?> itemType,
            Collection<Object> collection,
            Reference annotationRef) {
        this.name = name;
        this.itemType = itemType;
        this.collection = collection;
        this.annotationRef = annotationRef;
    }

    @Override
    public Class<?> getItemType() {
        return itemType;
    }

    @Override
    public void add(Object o) {
        collection.add(o);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getUsage() {
        return "Add value to " + getName() + ".";
    }

    @Override
    public Object get() {
        return collection;
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotation) {
        return annotationRef.isAnnotationPresent(annotation);
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotation) {
        return annotationRef.getAnnotation(annotation);
    }

    @Override
    public Class<?> getType() {
        return collection.getClass();
    }

    @Override
    public Type getGenericType() {
        return new ReferenceParameterizedType(collection.getClass(), itemType);
    }
}
