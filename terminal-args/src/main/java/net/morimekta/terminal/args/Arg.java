package net.morimekta.terminal.args;

import java.util.List;

/**
 * Base interface for all arguments and options.
 */
public interface Arg {
    /**
     * The argument name. This is visible in the single-line and usage
     * print-outs.
     *
     * @return The argument name.
     */
    String getName();

    /**
     * The argument usage description.
     *
     * @return The usdage description.
     */
    String getUsage();

    /**
     * A default value descriptor.
     *
     * @return The default value or null.
     */
    String getDefaultValue();

    /**
     * If the argument can be repeated (for arguments means to be multi-valued).
     *
     * @return True if the argument is repeated.
     */
    boolean isRepeated();

    /**
     * If the argument is required (must be set).
     *
     * @return True if the argument is required.
     */
    boolean isRequired();

    /**
     * True if the argument should be hidden by default. Passing showHidden to
     * printUsage will print the option. It will be hidden from singleLineUsage
     * regardless.
     *
     * @return If the argument is hidden.
     */
    boolean isHidden();

    /**
     * Get the argument's single line usage string.
     *
     * @return The single-line usage string.
     */
    String getSingleLineUsage();

    /**
     * Prefix part of the usage message.
     *
     * @return The usage prefix.
     */
    String getPrefix();

    /**
     * Called on all the arguments after the parsing is done to validate
     * if all requirements have been passed. Should throw an
     * {@link ArgException} if is did not validate with the appropriate
     * error message.
     */
    void validate() throws ArgException;

    /**
     * Try to apply to the argument. The method should return 0 if argument
     * is rejected, otherwise the number of argument strings that was consumed,
     * including the argument string that triggered the call. And handle its
     * value or values.
     *
     * @param args The argument list, containing at least the triggering argument
     *             string, plus any extra remaining arguments.
     * @return The number of args consumed.
     * @throws ArgException If the passed arguments are not valid.
     */
    int apply(List<String> args);

    /**
     * Builder for making an argument instance.
     *
     * @param <A> The arg type.
     */
    interface Builder<A extends Arg> {
        /**
         * @return The built arg instance.
         */
        A build();
    }
}
