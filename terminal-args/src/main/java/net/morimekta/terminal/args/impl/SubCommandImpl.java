/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.impl;

import net.morimekta.collect.UnmodifiableList;
import net.morimekta.terminal.args.ArgParser;
import net.morimekta.terminal.args.SubCommand;

import java.util.List;
import java.util.function.Function;

/**
 * Sub command base class.
 *
 * @param <SubCommandDef> The sub-command instance type.
 */
public class SubCommandImpl<SubCommandDef> implements SubCommand<SubCommandDef> {
    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getUsage() {
        return usage;
    }

    @Override
    public boolean isHidden() {
        return hidden;
    }

    @Override
    public List<String> getAliases() {
        return aliases;
    }

    @Override
    public SubCommandDef newInstance(ArgParser.Builder parser) {
        return instanceFactory.apply(parser);
    }

    @Override
    public String toString() {
        return "SubCommand{" + getName() + "}";
    }

    public static class BuilderImpl<SubCommandDef> implements SubCommand.Builder<SubCommandDef> {
        private final String                                               name;
        private final String                                               usage;
        private final Function<ArgParser.Builder, ? extends SubCommandDef> instanceFactory;

        private UnmodifiableList<String> aliases = UnmodifiableList.listOf();
        private boolean                  hidden;

        public BuilderImpl(String name,
                           String usage,
                           Function<ArgParser.Builder, ? extends SubCommandDef> instanceFactory) {
            this.name = name;
            this.usage = usage;
            this.instanceFactory = instanceFactory;
        }

        @Override
        public BuilderImpl<SubCommandDef> alias(String... aliases) {
            this.aliases = this.aliases.append(aliases);
            return this;
        }

        @Override
        public BuilderImpl<SubCommandDef> hidden() {
            this.hidden = true;
            return this;
        }

        @Override
        public SubCommand<SubCommandDef> build() {
            return new SubCommandImpl<>(name, usage, hidden, instanceFactory, aliases);
        }
    }

    private SubCommandImpl(String name,
                           String usage,
                           boolean hidden,
                           Function<ArgParser.Builder, ? extends SubCommandDef> instanceFactory,
                           List<String> aliases) {
        this.name = name;
        this.usage = usage;
        this.instanceFactory = instanceFactory;
        this.hidden = hidden;
        this.aliases = aliases;
    }

    private final String       name;
    private final String       usage;
    private final List<String> aliases;
    private final boolean      hidden;

    private final Function<ArgParser.Builder, ? extends SubCommandDef> instanceFactory;
}
