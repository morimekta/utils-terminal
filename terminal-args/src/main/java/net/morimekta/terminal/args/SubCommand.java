package net.morimekta.terminal.args;

import net.morimekta.terminal.args.impl.SubCommandImpl;

import java.util.List;
import java.util.function.Function;

/**
 * Defining a subcommand for the argument parser.
 *
 * @param <SubCommandDef> The subcommand type definition.
 */
public interface SubCommand<SubCommandDef> {
    /**
     * The sub-command name.
     *
     * @return The name.
     */
    String getName();

    /**
     * The basic usage description.
     *
     * @return The usage description.
     */
    String getUsage();

    /**
     * If the sub-command is hidden by default.
     *
     * @return True if hidden.
     */
    boolean isHidden();

    /**
     * Get the list of sub-command aliases.
     *
     * @return The aliases.
     */
    List<String> getAliases();

    /**
     * Instantiate the selected commands' implementation.
     *
     * @param builder The argument parser builder that should contain the subcommand.
     * @return The new sub-command instance.
     */
    SubCommandDef newInstance(ArgParser.Builder builder);

    /**
     * Builder for making a subcommand.
     *
     * @param <SubCommandDef> The subcommand type definition.
     */
    interface Builder<SubCommandDef> {
        /**
         * @param aliases Aliases to set for the subcommand.
         * @return The builder.
         */
        Builder<SubCommandDef> alias(String... aliases);

        /**
         * @return Set the subcommand as hidden.
         */
        Builder<SubCommandDef> hidden();

        /**
         * @return The built subcommand.
         */
        SubCommand<SubCommandDef> build();
    }

    /**
     * Make a subcommand builder.
     *
     * @param name            Name or subcommand.
     * @param usage           Usage string for the subcommand.
     * @param instanceFactory Instance factory for instantiating the subcommand.
     * @param <SubCommandDef> The subcommand type definition.
     * @return The subcommand builder.
     */
    static <SubCommandDef> SubCommand.Builder<SubCommandDef> subCommand(
            String name,
            String usage,
            Function<ArgParser.Builder, ? extends SubCommandDef> instanceFactory) {
        return new SubCommandImpl.BuilderImpl<>(name, usage, instanceFactory);
    }
}
