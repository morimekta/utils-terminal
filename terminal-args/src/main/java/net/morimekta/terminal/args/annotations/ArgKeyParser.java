package net.morimekta.terminal.args.annotations;

import net.morimekta.terminal.args.ValueParser;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specify argument value parser for key of a property.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface ArgKeyParser {
    /**
     * @return The key parser class.
     */
    Class<? extends ValueParser<?>> value();
}
