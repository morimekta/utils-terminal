package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

import java.math.BigDecimal;

/**
 * A converter to big integer values.
 */
public class BigDecimalParser implements ValueParser<BigDecimal> {
    @Override
    public BigDecimal parse(String s) {
        try {
            return new BigDecimal(s);
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid big decimal value %s", s, nfe);
        }
    }
}
