package net.morimekta.terminal.args.impl;

import net.morimekta.terminal.args.Arg;
import net.morimekta.terminal.args.ArgParserBuilder;
import net.morimekta.terminal.args.Option;

import java.util.List;
import java.util.Map;

abstract class ArgParserBuilderImpl implements ArgParserBuilder {
    protected ArgParserBuilderImpl(ArgParserImpl parent,
                                   String program,
                                   String version,
                                   String description,
                                   List<Option> options,
                                   List<Arg> arguments,
                                   Map<String, Option> longNameOptions,
                                   Map<Character, Option> shortOptions) {
        this.parent = parent;
        this.program = program;
        this.version = version;
        this.description = description;
        this.options = options;
        this.arguments = arguments;
        this.longNameOptions = longNameOptions;
        this.shortOptions = shortOptions;
    }

    protected final ArgParserImpl          parent;
    protected final String                 program;
    protected final String                 version;
    protected final String                 description;
    protected final List<Option>           options;
    protected final List<Arg>              arguments;
    protected final Map<String, Option>    longNameOptions;
    protected final Map<Character, Option> shortOptions;
}
