package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

/**
 * A converter to float values.
 */
public class FloatParser implements ValueParser<Float> {
    @Override
    public Float parse(String s) {
        try {
            return Float.parseFloat(s);
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid float value %s", s, nfe);
        }
    }
}
