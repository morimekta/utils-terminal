/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.impl;

import net.morimekta.terminal.args.Option;

import java.util.List;

/**
 * Argument definition base interface.
 */
public abstract class BaseOption extends BaseArg implements Option {
    private final String shortNames;
    private final String metaVar;

    protected BaseOption(String name,
                         String shortNames,
                         String metaVar,
                         String usage,
                         String defaultValue,
                         boolean repeated,
                         boolean required,
                         boolean hidden) {
        super(name, usage, defaultValue, repeated, required, hidden);
        this.shortNames = shortNames == null ? "" : shortNames;
        this.metaVar = metaVar;
    }

    @Override
    public String getShortNames() {
        return shortNames;
    }

    @Override
    public String getMetaVar() {
        return metaVar;
    }

    @Override
    public abstract int applyShort(String opts, List<String> args);

    @Override
    public String getSingleLineUsage() {
        StringBuilder sb = new StringBuilder();
        if (!isRequired()) {
            sb.append('[');
        } else if (isRepeated()) {
            sb.append("(");
        }

        if (shortNames.length() > 0) {
            sb.append('-')
              .append(shortNames.charAt(0));
        } else {
            sb.append(getName());
        }

        if (getMetaVar() != null) {
            sb.append(' ')
              .append(getMetaVar());
        }

        if (isRepeated()) {
            sb.append(" ...");
        }

        if (!isRequired()) {
            sb.append(']');
        } else if (isRepeated()) {
            sb.append(")");
        }

        return sb.toString();
    }

    @Override
    public String getPrefix() {
        StringBuilder sb = new StringBuilder();
        if (getName() != null) {
            sb.append(getName());

            if (getShortNames().length() > 0) {
                sb.append(" (");
                boolean first = true;
                for (char c : getShortNames().toCharArray()) {
                    if (first) {
                        first = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append('-')
                      .append(c);
                }

                sb.append(")");
            }
        } else {
            sb.append('-')
              .append(getShortNames().subSequence(0, 1));

            if (getShortNames().length() > 1) {
                sb.append(" (");

                boolean first = true;
                for (char c : getShortNames().substring(1).toCharArray()) {
                    if (first) {
                        first = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append('-')
                      .append(c);
                }
                sb.append(")");
            }
        }
        if (getMetaVar() != null) {
            sb.append(' ')
              .append(getMetaVar());
        }
        return sb.toString();
    }

    /**
     * Handy getter mostly to be used in exception naming.
     *
     * @return The argument's name or first short name.
     */
    protected String nameOrShort() {
        if (getName() != null) {
            return getName();
        }
        return "-" + getShortNames().charAt(0);
    }
}
