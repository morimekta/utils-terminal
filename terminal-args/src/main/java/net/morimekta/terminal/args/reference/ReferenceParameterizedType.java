package net.morimekta.terminal.args.reference;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

public class ReferenceParameterizedType implements ParameterizedType {
    private final Class<?> rawType;
    private final Type[]   genericTypes;

    public ReferenceParameterizedType(Class<?> rawType, Type... genericTypes) {
        this.rawType = rawType;
        this.genericTypes = genericTypes;
    }

    @Override
    public Type[] getActualTypeArguments() {
        return Arrays.copyOf(genericTypes, genericTypes.length);
    }

    @Override
    public Type getRawType() {
        return rawType;
    }

    @Override
    public Type getOwnerType() {
        return null;
    }
}
