package net.morimekta.terminal.args.reference;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collection;

public class AdderMethodReference extends MethodReference implements AdderReference {
    private final Method adder;

    public AdderMethodReference(
            Object instance,
            Method getter,
            Method adder,
            AccessibleObject annotationRef) {
        super(instance, getter, annotationRef);
        this.adder = adder;
    }

    @Override
    public Class<?> getItemType() {
        return adder.getParameterTypes()[0];
    }

    @Override
    public String getName() {
        return adder.getName();
    }

    @Override
    public void add(Object o) {
        safeInvoke(adder, instance, o);
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotation) {
        return adder.isAnnotationPresent(annotation) || super.isAnnotationPresent(annotation);
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotation) {
        if (adder.isAnnotationPresent(annotation)) {
            return adder.getAnnotation(annotation);
        }
        return super.getAnnotation(annotation);
    }

    @Override
    public Class<?> getType() {
        if (getter != null) {
            return getter.getReturnType();
        }
        return Collection.class;
    }

    @Override
    public Type getGenericType() {
        if (getter != null) {
            return getter.getGenericReturnType();
        }
        return new ReferenceParameterizedType(getType(), getItemType());
    }

}
