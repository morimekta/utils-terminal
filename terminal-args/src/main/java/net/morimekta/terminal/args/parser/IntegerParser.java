package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

/**
 * A converter to integer values.
 */
public class IntegerParser implements ValueParser<Integer> {
    @Override
    public Integer parse(String s) {
        try {
            if (s.startsWith("0x")) {
                return Integer.parseInt(s.substring(2), 16);
            } else if (s.startsWith("0")) {
                return Integer.parseInt(s, 8);
            }
            return Integer.parseInt(s);
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid integer value %s", s, nfe);
        }
    }
}
