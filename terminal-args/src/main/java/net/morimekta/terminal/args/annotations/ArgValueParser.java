package net.morimekta.terminal.args.annotations;

import net.morimekta.terminal.args.ValueParser;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specify the value parser for an argument, or for a list item type or
 * property (map) value type.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
public @interface ArgValueParser {
    /**
     * @return The value parser class.
     */
    Class<? extends ValueParser<?>> value();
}
