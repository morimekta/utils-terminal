package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

/**
 * A converter to short values.
 */
public class ByteParser implements ValueParser<Byte> {
    @Override
    public Byte parse(String s) {
        try {
            if (s.startsWith("0x")) {
                return Byte.parseByte(s.substring(2), 16);
            } else if (s.startsWith("0")) {
                return Byte.parseByte(s, 8);
            }
            return Byte.parseByte(s);
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid integer value %s", s, nfe);
        }
    }
}
