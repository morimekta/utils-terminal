package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

/**
 * A converter to unsigned long values.
 */
public class UnsignedLongParser implements ValueParser<Long> {
    @Override
    public Long parse(String s) {
        try {
            if (s.startsWith("0x")) {
                return Long.parseUnsignedLong(s.substring(2), 16);
            } else if (s.startsWith("0")) {
                return Long.parseUnsignedLong(s, 8);
            }
            return Long.parseUnsignedLong(s);
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid unsigned long value %s", s, nfe);
        }
    }
}
