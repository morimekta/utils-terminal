package net.morimekta.terminal.args.reference;

import net.morimekta.terminal.args.ArgException;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class SettableMethodReference extends MethodReference implements SettableReference {
    private final Method setter;

    public SettableMethodReference(
            Object instance,
            Method getter,
            Method setter,
            AccessibleObject annotationRef) {
        super(instance, getter, annotationRef);
        this.setter = setter;
    }

    @Override
    public String getName() {
        return setter.getName();
    }

    @Override
    public void set(Object o) {
        safeInvoke(setter, instance, o);
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotation) {
        return setter.isAnnotationPresent(annotation) || super.isAnnotationPresent(annotation);
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotation) {
        if (setter.isAnnotationPresent(annotation)) {
            return setter.getAnnotation(annotation);
        }
        return super.getAnnotation(annotation);
    }

    @Override
    public Class<?> getType() {
        var p0 = setter.getParameters()[0];
        return p0.getType();
    }

    @Override
    public Type getGenericType() {
        var p0 = setter.getParameters()[0];
        return p0.getParameterizedType();
    }
}
