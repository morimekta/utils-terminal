package net.morimekta.terminal.args;

import net.morimekta.terminal.args.impl.FlagImpl;

import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;
import static net.morimekta.terminal.args.impl.OptionUtils.requireValidLongName;
import static net.morimekta.terminal.args.impl.OptionUtils.requireValidShortNames;
import static net.morimekta.terminal.args.impl.OptionUtils.requireValidUsage;

/**
 * Similar to {@link Option}, but without any value argument. Can only
 * toggle boolean values.
 */
public interface Flag extends Option {
    /**
     * The alternative (negating) long name for the flag.
     *
     * @return The negating name.
     */
    String getNegateName();

    /**
     * Builder for flag options.
     */
    interface Builder extends Arg.Builder<Flag> {
        /**
         * @param negateName The negate name for the flag.
         * @return The builder.
         */
        Builder negateName(String negateName);

        /**
         * @param negateShortName Short name char for negating the flag.
         * @return The builder.
         */
        Builder negateShortName(char negateShortName);

        /**
         * Set default TRUE for the flag.
         *
         * @return The builder.
         */
        Builder defaultOn();

        /**
         * Set default FALSE for the flag.
         *
         * @return The builder.
         */
        Builder defaultOff();

        /**
         * Set the default value for the flag.
         *
         * @param value The flag default value.
         * @return The builder.
         */
        Builder defaultValue(boolean value);

        /**
         * Allow repeated applications of the flag.
         *
         * @return The builder.
         */
        Builder repeated();

        /**
         * Set the flag as hidden.
         *
         * @return The builder.
         */
        Builder hidden();
    }

    /**
     * Build a flag option.
     *
     * @param name   Long name for the flag.
     * @param usage  Usage string for the flag.
     * @param setter Setter for the flag value.
     * @return The builder.
     */
    static Builder flagLong(
            String name,
            String usage,
            Consumer<Boolean> setter) {
        return new FlagImpl.BuilderImpl(
                requireValidLongName(name),
                null,
                requireValidUsage(usage),
                requireNonNull(setter, "setter == null"));
    }

    /**
     * Build a flag option.
     *
     * @param shortNames Short name for the flag.
     * @param usage      Usage string for the flag.
     * @param setter     Setter for the flag value.
     * @return The builder.
     */
    static Builder flagShort(
            String shortNames,
            String usage,
            Consumer<Boolean> setter) {
        return new FlagImpl.BuilderImpl(
                null,
                requireValidShortNames(shortNames),
                requireValidUsage(usage),
                requireNonNull(setter, "setter == null"));
    }

    /**
     * Build a flag option.
     *
     * @param name       Long name for the flag.
     * @param shortNames Short name for the flag.
     * @param usage      Usage string for the flag.
     * @param setter     Setter for the flag value.
     * @return The builder.
     */
    static Builder flag(
            String name,
            String shortNames,
            String usage,
            Consumer<Boolean> setter) {
        return new FlagImpl.BuilderImpl(
                requireValidLongName(name),
                requireValidShortNames(shortNames),
                requireValidUsage(usage),
                requireNonNull(setter, "setter == null"));
    }
}
