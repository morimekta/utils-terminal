/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args;

import java.util.Arrays;
import java.util.Locale;

/**
 * Argument exception. Usually a result of the parsing, but may be caused by
 * invalid initialization.
 */
public class ArgException extends RuntimeException {
    private static final long serialVersionUID = 723809669050202722L;

    /**
     * @param format String message format.
     * @param args   String format args.
     */
    public ArgException(String format, Object... args) {
        super(formatIfArgs(format, args), causeFromArgs(args));
        this.parser = null;
    }

    /**
     * @param parser The parser where the exception occurred.
     * @param format String message format.
     * @param args   String format args.
     */
    public ArgException(ArgParser parser, String format, Object... args) {
        super(formatIfArgs(format, args), causeFromArgs(args));
        this.parser = parser;
    }

    /**
     * @param cause  Throwable cause of exception.
     * @param format String message format.
     * @param args   String format args.
     * @deprecated Use {@link #ArgException(String, Object...)} instead.
     */
    @Deprecated(forRemoval = true, since = "4.1.0")
    @SuppressWarnings("unused")
    public ArgException(Throwable cause, String format, Object... args) {
        super(formatIfArgs(format, args), cause);
        this.parser = null;
    }

    /**
     * @param parser The parser where the exception occurred.
     * @param cause  Throwable cause of exception.
     * @param format String message format.
     * @param args   String format args.
     * @deprecated Use {@link #ArgException(ArgParser, String, Object...)} instead.
     */
    @Deprecated(forRemoval = true, since = "4.1.0")
    @SuppressWarnings("unused")
    public ArgException(ArgParser parser, Throwable cause, String format, Object... args) {
        super(formatIfArgs(format, args), cause);
        this.parser = parser;
    }

    /**
     * @return The underlying parser, if any.
     */
    public ArgParser getParser() {
        return parser;
    }

    // --- Private ---

    /**
     * No comment.
     */
    private final ArgParser parser;

    private static Throwable causeFromArgs(Object... args) {
        if (args.length > 0 && args[args.length - 1] instanceof Throwable) {
            return (Throwable) args[args.length - 1];
        }
        return null;
    }

    private static String formatIfArgs(String format, Object... args) {
        if (args.length > 0 && args[args.length - 1] instanceof Throwable) {
            args = Arrays.copyOf(args, args.length - 1);
        }
        if (args.length > 0) {
            return String.format(Locale.US, format, args);
        }
        return format;
    }
}
