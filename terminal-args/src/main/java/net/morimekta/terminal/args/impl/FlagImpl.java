/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.impl;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.Flag;

import java.util.List;
import java.util.function.Consumer;

/**
 * Similar to {@link OptionImpl}, but without any value argument. Can only
 * toggle boolean values.
 */
public class FlagImpl extends BaseOption implements Flag {
    @Override
    public String getShortNames() {
        if (negateShortName != null) {
            return super.getShortNames() + negateShortName;
        }
        return super.getShortNames();
    }

    @Override
    public String getNegateName() {
        return this.negateName;
    }

    @Override
    public String getSingleLineUsage() {
        // Do not show "long" info in single-line usage if the unary option
        // has a short name.
        if (getShortNames().length() > 0) {
            return null;
        }

        return super.getSingleLineUsage();
    }

    @Override
    public void validate() {
    }

    @Override
    public int applyShort(String opts, List<String> args) {
        if (applied && !isRepeated()) {
            throw new ArgException("%s is already applied", nameOrShort());
        }
        applied = true;

        if (negateShortName != null && opts.charAt(0) == negateShortName) {
            setter.accept(false);
        } else {
            setter.accept(true);
        }
        return 0;
    }

    @Override
    public int apply(List<String> args) {
        if (getName() == null) {
            throw new IllegalStateException("No long option for -[" + getShortNames() + "]");
        }

        if (applied && !isRepeated()) {
            throw new ArgException("%s is already applied", nameOrShort());
        }
        applied = true;

        String current = args.get(0);
        if (current.equals(getName())) {
            setter.accept(true);
        } else if (current.equals(getNegateName())) {
            setter.accept(false);
        } else if (current.startsWith(getName() + "=")) {
            String value = current.substring(getName().length() + 1);
            setter.accept(Boolean.parseBoolean(value));
        } else {
            throw new IllegalArgumentException("Argument not matching flag " + nameOrShort() + ": " + current);
        }
        return 1;
    }

    // --------------

    public static class BuilderImpl implements Flag.Builder {
        private final String            name;
        private final String            shortNames;
        private final String            usage;
        private final Consumer<Boolean> setter;
        private       String            negateName;
        private       Character         negateShortName;
        private       String            defaultValue;
        private       boolean           hidden;
        private       boolean           repeated;

        public BuilderImpl(String name, String shortNames, String usage, Consumer<Boolean> setter) {
            this.name = name;
            this.shortNames = shortNames;
            this.usage = usage;
            this.setter = setter;
        }

        @Override
        public Flag build() {
            return new FlagImpl(
                    name,
                    shortNames,
                    usage,
                    setter,
                    defaultValue,
                    negateName,
                    negateShortName,
                    repeated,
                    hidden);
        }

        @Override
        public BuilderImpl negateName(String negateName) {
            this.negateName = negateName;
            return this;
        }

        @Override
        public Flag.Builder negateShortName(char negateShortName) {
            this.negateShortName = negateShortName;
            return this;
        }

        @Override
        public BuilderImpl defaultOn() {
            this.defaultValue = "on";
            return this;
        }

        @Override
        public BuilderImpl defaultOff() {
            this.defaultValue = "off";
            return this;
        }

        @Override
        public BuilderImpl defaultValue(boolean value) {
            this.defaultValue = String.valueOf(value);
            return this;
        }

        @Override
        public Flag.Builder repeated() {
            this.repeated = true;
            return this;
        }

        @Override
        public BuilderImpl hidden() {
            this.hidden = true;
            return this;
        }
    }

    private final Consumer<Boolean> setter;
    private final String            negateName;
    private final Character         negateShortName;
    private       boolean           applied = false;

    private FlagImpl(String name,
                     String shortNames,
                     String usage,
                     Consumer<Boolean> setter,
                     String defaultValue,
                     String negateName,
                     Character negateShortName,
                     boolean repeated,
                     boolean hidden) {
        super(name, shortNames, null, usage, defaultValue, repeated, false, hidden);
        this.setter = setter;
        this.negateName = negateName;
        this.negateShortName = negateShortName;
    }
}
