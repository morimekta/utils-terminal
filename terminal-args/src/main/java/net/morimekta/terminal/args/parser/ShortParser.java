package net.morimekta.terminal.args.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

/**
 * A converter to short values.
 */
public class ShortParser implements ValueParser<Short> {
    @Override
    public Short parse(String s) {
        try {
            if (s.startsWith("0x")) {
                return Short.parseShort(s.substring(2), 16);
            } else if (s.startsWith("0")) {
                return Short.parseShort(s, 8);
            }
            return Short.parseShort(s);
        } catch (NumberFormatException nfe) {
            throw new ArgException("Invalid integer value %s", s, nfe);
        }
    }
}
