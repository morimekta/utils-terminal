package net.morimekta.terminal.args.reference;

import net.morimekta.terminal.args.ArgException;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class MethodReference implements Reference {
    final Object           instance;
    final Method           getter;
    final AccessibleObject annotationRef;

    public MethodReference(
            Object instance,
            Method getter,
            AccessibleObject annotationRef) {
        this.instance = instance;
        this.getter = getter;
        this.annotationRef = annotationRef;
    }

    @Override
    public String getName() {
        return getter.getName();
    }

    @Override
    public String getUsage() {
        return "Apply " + getName() + "().";
    }

    @Override
    public Object get() {
        if (getter == null) {
            return null;
        }
        return safeInvoke(getter, instance);
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotation) {
        return annotationRef.isAnnotationPresent(annotation) ||
               (getter != null && getter.isAnnotationPresent(annotation));
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotation) {
        if (getter != null && getter.isAnnotationPresent(annotation)) {
            return getter.getAnnotation(annotation);
        }
        if (annotationRef.isAnnotationPresent(annotation)) {
            return annotationRef.getAnnotation(annotation);
        }
        return null;
    }

    @Override
    public Class<?> getType() {
        return getter.getReturnType();
    }

    @Override
    public Type getGenericType() {
        return getter.getGenericReturnType();
    }

    static Object safeInvoke(Method method, Object instance, Object... params) {
        try {
            return method.invoke(instance, params);
        } catch (InvocationTargetException e) {
            if (e.getCause().getMessage() == null) {
                throw new ArgException(
                        e.getCause().getClass().getSimpleName(),
                        e.getCause());
            }
            throw new ArgException(e.getCause().getMessage(), e.getCause());
        } catch (IllegalAccessException e) {
            throw new ArgException(e.getMessage(), e.getCause());
        }
    }
}
