/**
 * Package containing utilities for handling advanced terminal I/O.
 */
module net.morimekta.terminal.args {
    exports net.morimekta.terminal.args;
    exports net.morimekta.terminal.args.annotations;
    exports net.morimekta.terminal.args.parser;

    requires transitive net.morimekta.io;
    requires net.morimekta.collect;
    requires net.morimekta.strings;

    // --- Exported for testing.
    exports net.morimekta.terminal.args.impl to net.morimekta.terminal.args.test;
    exports net.morimekta.terminal.args.reference to net.morimekta.terminal.args.test, org.mockito;
}