module net.morimekta.terminal.args.test {
    exports net.morimekta.terminal.args.test;
    exports net.morimekta.terminal.args.test.impl;
    exports net.morimekta.terminal.args.test.impl.test;
    exports net.morimekta.terminal.args.test.parser;
    exports net.morimekta.terminal.args.test.reference;

    requires net.morimekta.collect;

    requires transitive net.morimekta.strings;
    requires transitive net.morimekta.terminal.args;
    requires transitive net.morimekta.testing;
    requires transitive net.morimekta.testing.junit5;
    requires transitive net.morimekta.io;

    requires transitive org.junit.jupiter;
    requires transitive org.junit.jupiter.api;
    requires org.hamcrest;
    requires org.mockito.junit.jupiter;
    requires org.mockito;
    requires net.bytebuddy;
    requires net.bytebuddy.agent;

    requires kotlin.stdlib;
    requires jul.to.slf4j;
    requires java.logging;
}