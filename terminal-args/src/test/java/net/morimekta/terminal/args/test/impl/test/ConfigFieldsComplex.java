package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.ArgNameFormat;
import net.morimekta.terminal.args.annotations.ArgIsHidden;
import net.morimekta.terminal.args.annotations.ArgIsRequired;
import net.morimekta.terminal.args.annotations.ArgNaming;
import net.morimekta.terminal.args.annotations.ArgOptions;
import net.morimekta.terminal.args.annotations.ArgValueParser;

import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

@ArgNaming(ArgNameFormat.SNAKE)
public class ConfigFieldsComplex {
    // parser from class annotation.
    public Resolution resolution;
    @ArgIsRequired
    @ArgValueParser(DPIParser.class)
    public DPI        dpi;

    @ArgOptions(shortChar = 'M')
    public       Map<String, Integer>    map;
    @ArgOptions(usage = "Truly put into concurrent")
    @SuppressWarnings("unused")
    public       HashMap<String, String> hashMap;
    @ArgIsHidden
    @SuppressWarnings("unused")
    public final Map<String, String>     hiddenMap = new HashMap<>();

    @ArgOptions(shortChar = 'D', usage = "Add duration to list")
    public       Collection<Duration> durations;
    @ArgIsRequired
    public final LinkedHashSet<Long>  longs = new LinkedHashSet<>();
    @ArgIsHidden
    @SuppressWarnings("unused")
    public       List<String>         hiddenList;

    @ArgOptions(name = "fields")
    public final ConfigFieldsSimple simple = new ConfigFieldsSimple();
    public       ConfigFieldsSimple other  = new ConfigFieldsSimple();
}
