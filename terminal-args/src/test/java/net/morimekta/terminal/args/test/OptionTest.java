/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.test;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.Option;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.terminal.args.Option.option;
import static net.morimekta.terminal.args.Option.optionLong;
import static net.morimekta.terminal.args.Option.optionShort;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class OptionTest {
    @Test
    public void testConstrictor() {
        AtomicReference<String> ref = new AtomicReference<>();
        Option opt = optionLong("--opt", "Option", ref::set).build();

        assertEquals("--opt", opt.getName());
        assertEquals("", opt.getShortNames());
        assertEquals("[--opt OPT]", opt.getSingleLineUsage());
        assertEquals("--opt OPT", opt.getPrefix());
        assertEquals("Option", opt.getUsage());
        assertFalse(opt.isHidden());
        assertFalse(opt.isRequired());
        assertFalse(opt.isRepeated());

        opt = Option.option("--opt", "O", "Option", ref::set)
                .metaVar("o").repeated().required().build();
        assertEquals("--opt", opt.getName());
        assertEquals("O", opt.getShortNames());
        assertEquals("(-O o ...)", opt.getSingleLineUsage());
        assertEquals("--opt (-O) o", opt.getPrefix());
        assertFalse(opt.isHidden());
        assertTrue(opt.isRequired());
        assertTrue(opt.isRepeated());

        opt = Option.option("--opt", "O", "Option", ref::set)
                .metaVar("o").hidden().build();
        assertEquals("--opt", opt.getName());
        assertEquals("O", opt.getShortNames());
        assertEquals("[-O o]", opt.getSingleLineUsage());
        assertEquals("--opt (-O) o", opt.getPrefix());
        assertTrue(opt.isHidden());
        assertFalse(opt.isRequired());
        assertFalse(opt.isRepeated());

        opt = Option.optionShort("Oof", "Option", ref::set).metaVar("o").build();
        assertNull(opt.getName());
        assertEquals("Oof", opt.getShortNames());
        assertEquals("[-O o]", opt.getSingleLineUsage());
        assertEquals("-O (-o, -f) o", opt.getPrefix());
        assertFalse(opt.isHidden());
        assertFalse(opt.isRequired());
        assertFalse(opt.isRepeated());
    }

    @Test
    public void testBadConstructor() {
        AtomicReference<String> ref = new AtomicReference<>();
        try {
            Option.option(null, "", "Option", ref::set);
            fail("no exception");
        } catch (NullPointerException e) {
            assertEquals("name == null", e.getMessage());
        }
        try {
            Option.option("--foo", null, "Option", ref::set);
            fail("no exception");
        } catch (NullPointerException e) {
            assertEquals("shortNames == null", e.getMessage());
        }
        try {
            Option.option("--foo", "f", null, ref::set);
            fail("no exception");
        } catch (NullPointerException e) {
            assertEquals("usage == null", e.getMessage());
        }

        try {
            Option.optionShort("", "Test", ref::set);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertEquals("Empty short name character set.", e.getMessage());
        }
        try {
            Option.optionShort("-", "Test", ref::set);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertEquals("Invalid short name set \"-\", can only be letters, numbers, '?' or '!'.", e.getMessage());
        }

        try {
            optionLong("", "Option", ref::set);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertEquals("Empty long name.", e.getMessage());
        }
        try {
            optionLong("-foo", "Option", ref::set);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertEquals("Invalid name \"-foo\", must be an identifier string prefixed with '--', e.g.: \"--name\".", e.getMessage());
        }

        try {
            optionShort("f", "", ref::set);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertEquals("Empty usage string.", e.getMessage());
        }
    }

    @Test
    public void testApplyShort() {
        AtomicReference<String> ref = new AtomicReference<>();
        Option opt = option("--opt", "O", "Option", ref::set).build();

        opt.applyShort("Oval", List.of("-Oval", "not"));
        assertEquals("val", ref.get());

        opt = option("--opt", "O", "Option", ref::set).build();
        opt.applyShort("O", List.of("-O", "other", "not"));
        assertEquals("other", ref.get());

        try {
            opt.applyShort("O", List.of("-O", "other", "not"));
            fail("no exception");
        } catch (ArgException e) {
            assertEquals("Option --opt already applied", e.getMessage());
        }

        opt = option("--opt", "O", "Option", ref::set).build();
        try {
            opt.applyShort("O", List.of("-O"));
            fail("no exception");
        } catch (ArgException e) {
            assertEquals("Missing value after -O", e.getMessage());
        }
    }

    @Test
    public void testApply() {
        AtomicReference<String> ref = new AtomicReference<>();
        Option opt = Option.option("--opt", "O", "Option", ref::set).build();

        opt.apply(List.of("--opt=val", "not"));
        assertEquals("val", ref.get());

        opt = Option.option("--opt", "O", "Option", ref::set)
                .metaVar("o").build();
        opt.apply(List.of("--opt", "other", "not"));
        assertEquals("other", ref.get());

        opt.validate();
    }

    @Test
    public void testApplyFails() {
        AtomicReference<String> ref = new AtomicReference<>();
        Option opt = Option.option("--opt", "O", "Option", ref::set).build();

        opt.apply(List.of("--opt", "other", "not"));
        try {
            opt.apply(List.of("--opt", "other", "not"));
            fail("no exception");
        } catch (ArgException e) {
            assertEquals("Option --opt already applied", e.getMessage());
        }

        opt = Option.option("--opt", "O", "Option", ref::set).build();
        try {
            opt.apply(List.of("--not", "other", "not"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertEquals("Argument not matching option --opt: --not", e.getMessage());
        }

        opt = Option.option("--opt", "O", "Option", ref::set).build();
        try {
            opt.apply(List.of("--opt"));
            fail("no exception");
        } catch (ArgException e) {
            assertEquals("Missing value after --opt", e.getMessage());
        }

        opt = Option.optionShort("O", "Option", ref::set).build();
        try {
            opt.apply(List.of("--opt", "value"));
            fail("no exception");
        } catch (IllegalStateException e) {
            assertEquals("No long option for -[O]", e.getMessage());
        }
    }

    @Test
    public void testApplyRepeated() {
        ArrayList<String> values = new ArrayList<>();
        Option opt = Option.option("--opt", "O", "Option", values::add).repeated().build();

        opt.applyShort("Oval", List.of("-Oval", "not"));
        opt.applyShort("O", List.of("-O", "other", "not"));
        opt.apply(List.of("--opt", "third", "not"));

        assertEquals(listOf("val", "other", "third"), values);

        opt.validate();
    }

    @Test
    public void testApplyRequired() {
        AtomicReference<String> ref = new AtomicReference<>();
        Option opt = Option.option("--opt", "O", "Option", ref::set).required().build();

        try {
            opt.validate();
        } catch (ArgException e) {
            assertEquals("Option --opt is required", e.getMessage());
        }

        opt.apply(List.of("--opt", "val"));
        opt.validate();
    }
}
