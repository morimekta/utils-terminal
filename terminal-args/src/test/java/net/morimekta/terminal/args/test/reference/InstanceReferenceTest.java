package net.morimekta.terminal.args.test.reference;

import net.morimekta.terminal.args.annotations.ArgIsHidden;
import net.morimekta.terminal.args.annotations.ArgIsRequired;
import net.morimekta.terminal.args.reference.AdderInstanceReference;
import net.morimekta.terminal.args.reference.PutterInstanceReference;
import net.morimekta.terminal.args.reference.Reference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InstanceReferenceTest {
    @ArgIsHidden
    private static void annotatedMethod() {}

    private static ArgIsHidden annotation;

    @BeforeAll
    public static void setUpClass() throws NoSuchMethodException {
        annotation = InstanceReferenceTest
                .class
                .getDeclaredMethod("annotatedMethod")
                .getAnnotation(ArgIsHidden.class);
    }

    @Test
    public void testAdderInstanceReference() {
        var annotationRef = mock(Reference.class);
        when(annotationRef.isAnnotationPresent(ArgIsHidden.class)).thenReturn(true);
        when(annotationRef.getAnnotation(ArgIsHidden.class)).thenReturn(annotation);

        var list = new ArrayList<>();
        var adder = new AdderInstanceReference(
                "hiddenList", String.class, list, annotationRef);

        assertThat(adder.getName(), is("hiddenList"));
        assertThat(adder.getUsage(), is("Add value to hiddenList."));
        assertThat(adder.get(), is(sameInstance(list)));
        assertThat(adder.isAnnotationPresent(ArgIsHidden.class), is(true));
        assertThat(adder.isAnnotationPresent(ArgIsRequired.class), is(false));
        assertThat(adder.getAnnotation(ArgIsHidden.class), is(notNullValue()));
        assertThat(adder.getAnnotation(ArgIsRequired.class), is(nullValue()));
        assertThat(adder.getType(), is(ArrayList.class));
        var generic = adder.getGenericType();
        assertThat(generic, is(instanceOf(ParameterizedType.class)));
        var parameterizedType = (ParameterizedType) generic;
        assertThat(parameterizedType.getRawType(), is(ArrayList.class));
        assertThat(parameterizedType.getActualTypeArguments().length, is(1));
        assertThat(parameterizedType.getActualTypeArguments()[0], is(String.class));
    }

    @Test
    public void testPutterInstanceReference() {
        var annotationRef = mock(Reference.class);
        when(annotationRef.isAnnotationPresent(ArgIsHidden.class)).thenReturn(true);
        when(annotationRef.getAnnotation(ArgIsHidden.class)).thenReturn(annotation);

        var map = new HashMap<>();
        var adder = new PutterInstanceReference(
                "hiddenMap", String.class, Integer.class, map, annotationRef) {};

        assertThat(adder.getName(), is("hiddenMap"));
        assertThat(adder.getUsage(), is("Put value in hiddenMap."));
        assertThat(adder.get(), is(sameInstance(map)));
        assertThat(adder.isAnnotationPresent(ArgIsHidden.class), is(true));
        assertThat(adder.isAnnotationPresent(ArgIsRequired.class), is(false));
        assertThat(adder.getAnnotation(ArgIsHidden.class), is(notNullValue()));
        assertThat(adder.getAnnotation(ArgIsRequired.class), is(nullValue()));
        assertThat(adder.getType(), is(HashMap.class));
        var generic = adder.getGenericType();
        assertThat(generic, is(instanceOf(ParameterizedType.class)));
        var parameterizedType = (ParameterizedType) generic;
        assertThat(parameterizedType.getRawType(), is(HashMap.class));
        assertThat(parameterizedType.getActualTypeArguments().length, is(2));
        assertThat(parameterizedType.getActualTypeArguments()[0], is(String.class));
        assertThat(parameterizedType.getActualTypeArguments()[1], is(Integer.class));
    }
}
