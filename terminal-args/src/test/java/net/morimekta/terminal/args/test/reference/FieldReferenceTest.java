package net.morimekta.terminal.args.test.reference;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.annotations.ArgIsHidden;
import net.morimekta.terminal.args.annotations.ArgIsRequired;
import net.morimekta.terminal.args.reference.FieldReference;
import net.morimekta.terminal.args.reference.SettableFieldReference;
import net.morimekta.terminal.args.test.impl.test.ConfigFieldsComplex;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsPrimitives;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class FieldReferenceTest {

    @Test
    public void testFieldReference() throws NoSuchFieldException {
        var fields = new ConfigFieldsComplex();
        fields.hiddenList = new ArrayList<>();

        var field = fields.getClass().getField("hiddenList");
        var reference = new FieldReference(
                fields, field, field);

        assertThat(reference.getName(), is("hiddenList"));
        assertThat(reference.getUsage(), is("Set hiddenList."));
        assertThat(reference.get(), is(sameInstance(fields.hiddenList)));
        assertThat(reference.isAnnotationPresent(ArgIsHidden.class), is(true));
        assertThat(reference.isAnnotationPresent(ArgIsRequired.class), is(false));
        assertThat(reference.getAnnotation(ArgIsHidden.class), is(notNullValue()));
        assertThat(reference.getAnnotation(ArgIsRequired.class), is(nullValue()));
        assertThat(reference.getType(), is(List.class));
        var generic = reference.getGenericType();
        assertThat(generic, is(instanceOf(ParameterizedType.class)));
        var parameterizedType = (ParameterizedType) generic;
        assertThat(parameterizedType.getRawType(), is(List.class));
        assertThat(parameterizedType.getActualTypeArguments().length, is(1));
        assertThat(parameterizedType.getActualTypeArguments()[0], is(String.class));
    }

    @Test
    public void testSettableFieldReference() throws NoSuchFieldException {
        var fields = new ConfigFieldsComplex();
        var field = fields.getClass().getField("hiddenList");
        var reference = new SettableFieldReference(
                fields, field, field);

        assertThat(reference.getName(), is("hiddenList"));
        assertThat(reference.getUsage(), is("Set hiddenList."));
        assertThat(reference.get(), is(nullValue()));
        assertThat(reference.isAnnotationPresent(ArgIsHidden.class), is(true));
        assertThat(reference.isAnnotationPresent(ArgIsRequired.class), is(false));
        assertThat(reference.getAnnotation(ArgIsHidden.class), is(notNullValue()));
        assertThat(reference.getAnnotation(ArgIsRequired.class), is(nullValue()));
        assertThat(reference.getType(), is(List.class));
        var generic = reference.getGenericType();
        assertThat(generic, is(instanceOf(ParameterizedType.class)));
        var parameterizedType = (ParameterizedType) generic;
        assertThat(parameterizedType.getRawType(), is(List.class));
        assertThat(parameterizedType.getActualTypeArguments().length, is(1));
        assertThat(parameterizedType.getActualTypeArguments()[0], is(String.class));

        reference.set(List.of("foo", "bar"));

        assertThat(fields.hiddenList, is(List.of("foo", "bar")));
        assertThat(reference.get(), is(sameInstance(fields.hiddenList)));
    }

    @Test
    public void testSettableFieldReference_IllegalAccess() throws NoSuchFieldException {
        var fields = new ConfigMethodsPrimitives();

        var field = fields.getClass().getDeclaredField("flt");
        var reference = new SettableFieldReference(
                fields, field, field);

        try {
            Assertions.fail("No exception: " + reference.get());
        } catch (ArgException e) {
            assertThat(e.getCause(), is(instanceOf(IllegalAccessException.class)));
            assertThat(e.getMessage(), allOf(
                    containsString(FieldReference.class.getName()),
                    containsString("cannot access a member of class"),
                    containsString(ConfigMethodsPrimitives.class.getName())));
        }

        try {
            reference.set(44.4f);
            Assertions.fail("No exception.");
        } catch (ArgException e) {
            assertThat(e.getCause(), is(instanceOf(IllegalAccessException.class)));
            assertThat(e.getMessage(), allOf(
                    containsString(SettableFieldReference.class.getName()),
                    containsString("cannot access a member of class"),
                    containsString(ConfigMethodsPrimitives.class.getName())));
        }
    }
}
