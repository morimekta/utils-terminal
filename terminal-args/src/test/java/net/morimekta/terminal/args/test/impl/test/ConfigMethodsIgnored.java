package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.annotations.ArgIgnore;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unused")
public class ConfigMethodsIgnored {
    private Boolean bool;

    public Boolean getBool() {
        return bool;
    }

    public void setBool(Boolean bool) {
        this.bool = bool;
    }

    // not following convention, and not declared with annotation.
    private boolean plainText;

    public boolean plainText() {
        return plainText;
    }

    public void plainText(boolean pt) {
        plainText = pt;
    }

    @ArgIgnore
    private String ignoredName;

    public String getIgnoredName() {
        return ignoredName;
    }

    public void setIgnoredName(String ignoredName) {
        this.ignoredName = ignoredName;
    }

    public void addToImpossible(Object foo) {
        LOGGER.log(Level.FINE, "" + foo);
    }

    public void putInImpossible(Object foo, Object bar) {
        LOGGER.log(Level.FINE, foo + ": " + bar);
    }

    @ArgIgnore
    public void setValueToIgnore(String foo) {
        LOGGER.log(Level.FINE, foo);
    }

    private Map<String, String> getValuePrivate() {
        return Map.of();
    }

    private void putValuePrivate(String foo, String bar) {
        LOGGER.log(Level.FINE, foo + ": " + bar);
    }

    public String setValueNoArgs() {
        return toString();
    }

    public String setValueTooManyArgs(String a, String b, String c) {
        return a + "-" + b + "-" + c;
    }

    public String getValueWithArgs(String param) {
        return param;
    }

    public static final Logger LOGGER = Logger.getLogger(ConfigMethodsIgnored.class.getName());
}
