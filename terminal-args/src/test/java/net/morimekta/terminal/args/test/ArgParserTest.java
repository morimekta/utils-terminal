/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.test;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ArgParser;
import net.morimekta.terminal.args.impl.ArgParserImpl;
import net.morimekta.testing.junit5.ConsoleExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.terminal.args.ArgParser.argParser;
import static net.morimekta.terminal.args.Argument.argument;
import static net.morimekta.terminal.args.Flag.flag;
import static net.morimekta.terminal.args.Flag.flagLong;
import static net.morimekta.terminal.args.Option.option;
import static net.morimekta.terminal.args.Property.propertyShort;
import static net.morimekta.terminal.args.ValueParser.i32;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests for the Argument Parser.
 */
@ExtendWith(ConsoleExtension.class)
public class ArgParserTest {
    @TempDir
    public Path tmp;

    private AtomicReference<String> strValue;
    private AtomicInteger           intValue;
    private AtomicBoolean           boolValue;
    private ArrayList<String>       strList;
    private Map<String, String>     properties;

    @BeforeEach
    public void setUp() {
        strValue = new AtomicReference<>();
        intValue = new AtomicInteger();
        boolValue = new AtomicBoolean();
        strList = new ArrayList<>();
        properties = new HashMap<>();
    }

    @Test
    public void testConstructor() {
        var parser = argParser("gt", "0.2.5", "Git Tools")
                .add(flag("--help", "?h", "Help", boolValue::set))
                .build();

        assertThat(parser.getProgram(), is("gt"));
        assertThat(parser.getVersion(), is("0.2.5"));
        assertThat(parser.getDescription(), is("Git Tools"));
        assertThat(parser.toString(), is("ArgParser{gt}"));
        assertThat(parser.getParent(), is(nullValue()));
    }

    @Test
    public void testParse_simple() {
        argParser("gt", "0.2.5", "Git Tools")
                .add(option("--arg1", "a", "Integer value", i32().andApply(intValue::set))
                             .metaVar("I")
                             .defaultValue("55"))
                .add(propertyShort('D', "System property", properties::put))
                .add(flagLong("--arg2", "Another boolean", boolValue::set))
                .add(argument("type", "Some type", strValue::set)
                             .defaultValue("no-type")
                             .when(s -> !s.startsWith("/")))
                .add(argument("file", "Extra files", strList::add)
                             .repeated()
                             .required())
                .parse("--arg1", "4", "-Dsome.key=not.default", "--arg2", "/iggy", "some-type", "pop")
                .validate();

        assertEquals(4, intValue.get());
        assertEquals("not.default", properties.get("some.key"));
        assertTrue(boolValue.get());
        assertEquals("some-type", strValue.get());
        assertEquals(2, strList.size());
        assertEquals("/iggy", strList.get(0));
        assertEquals("pop", strList.get(1));
    }

    @Test
    public void testParse_alt() {
        intValue.set(55);
        boolValue.set(true);

        argParser("gt", "0.2.5", "Git Tools")
                .add(option("--arg1", "a", "Integer value", i32().andApply(intValue::set)).metaVar("I"))
                .add(propertyShort('D', "System property", properties::put).metaKey("key").metaVar("val").build())
                .add(flagLong("--arg2", "Boolean Usage", boolValue::set).negateName("--no_arg2").build())
                .parse("--arg1=4", "--no_arg2")
                .validate();

        assertEquals(4, intValue.get());
        assertNull(properties.get("some.key"));
        assertFalse(boolValue.get());
    }

    @Test
    public void testParse_short() {
        ArgParser.argParser("gt", "0.2.5", "Git Tools")
                 .add(option("--arg1", "a", "Integer value", i32().andApply(intValue::set)).metaVar("I"))
                 .add(propertyShort('D', "System property", properties::put))
                 .add(flag("--arg2", "b", "Boolean Usage", boolValue::set))
                 .parse("-ba4");

        assertEquals(4, intValue.get());
        assertNull(properties.get("some.key"));
        assertTrue(boolValue.get());
    }

    @Test
    public void testParse_shortFlag() {
        AtomicBoolean b1 = new AtomicBoolean(false);
        AtomicBoolean b2 = new AtomicBoolean(false);
        AtomicBoolean b3 = new AtomicBoolean(false);

        ArgParser.argParser("gt", "0.2.5", "Git Tools")
                 .add(flag("--arg1", "a", "Boolean Usage", b1::set))
                 .add(flag("--arg2", "b", "Boolean Usage", b2::set))
                 .add(flag("--arg3", "c", "Boolean Usage", b3::set))
                 .parse("-ac");

        assertTrue(b1.get());
        assertFalse(b2.get());
        assertTrue(b3.get());
    }

    @Test
    public void testParse_args() {
        ArgParser.argParser("gt", "0.2.5", "Git Tools")
                 .add(flag("--bool", "b", "Bool", boolValue::set))
                 .add(argument("arg", "A", strList::add).repeated().required())
                 .parse("first", "--", "--bool");

        assertThat(strList, is(listOf("first", "--bool")));
        assertThat(boolValue.get(), is(false));
    }

    @Test
    public void testParse_fails() {
        var parser = ArgParser.argParser("gt", "0.2.5", "Git Tools")
                              .add(flag("--bool", "b", "Bool", boolValue::set))
                              .add(argument("arg", "A", strList::add)
                                           .when(s -> !s.equals("bcd"))
                                           .repeated()
                                           .required());

        try {
            parser.parse("--boo");
            fail("no exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("No option for --boo"));
        }

        try {
            parser.parse("-bcd");
            fail("no exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("No short opt for -c"));
        }

        try {
            parser.parse("bcd");
            fail("no exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("No option found for bcd"));
        }

        try {
            parser.parse("--", "bcd");
            fail("no exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("No argument found for bcd"));
        }
    }

    @Test
    public void testFlagsFile() throws IOException {
        File flags = tmp.resolve("flags").toFile();
        try (FileOutputStream fos = new FileOutputStream(flags)) {
            fos.write(("--bool\n" +
                       "--opt the-opt\n" +
                       "\n" +
                       "# comment\n" +
                       "\"foo\\tbar\"\n" +
                       "      baz\n").getBytes(UTF_8));
        }

        AtomicBoolean b = new AtomicBoolean(false);
        ArrayList<String> arg = new ArrayList<>();
        AtomicReference<String> opt = new AtomicReference<>();

        ArgParser.Builder parser = ArgParser.argParser("gt", "0.2.5", "Git Tools")
                                            .add(flag("--bool", "b", "Bool", b::set))
                                            .add(option("--opt", "o", "Usage", opt::set).metaVar("OPT"))
                                            .add(argument("arg", "A", arg::add).repeated().required());

        assertThat(parser.parse(List.of("@" + flags.getCanonicalPath())),
                   is(instanceOf(ArgParser.class)));

        assertThat(b.get(), is(true));
        assertThat(opt.get(), is("the-opt"));
        assertThat(arg, is(listOf("foo\tbar", "baz")));

        try (FileOutputStream fos = new FileOutputStream(flags)) {
            fos.write(("--boo\n").getBytes(UTF_8));
        }

        try {
            parser.parse("@" + flags.getCanonicalPath());
            fail("no exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("Argument file flags: No option for --boo"));
        }
    }

    @Test
    public void testValidate() {
        ArgParser parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .add(option("--opt", "o", "Usage", strValue::set).metaVar("OPT").required())
                .add(argument("arg", "A", strList::add).required())
                .build();

        try {
            parser.validate();
            fail("no exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("Option --opt is required"));
        }

        parser.parse("--opt", "O");

        try {
            parser.validate();
            fail("no exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("Argument arg is required"));
        }

        parser.parse("arg");

        parser.validate();
    }

    @Test
    public void testAdd_fails() {
        ArgParserImpl.BuilderImpl parent = new ArgParserImpl.BuilderImpl(null, "gt", "v0.1", "GitTool");
        parent.add(flag("--parent", "p", "Usage", boolValue::set));
        ArgParserImpl.BuilderImpl parser = new ArgParserImpl.BuilderImpl(
                (ArgParserImpl) parent.build(),
                "gt tool",
                "v0.1",
                "GitTool");
        parser.add(flag("--exists", "e", "Usage", boolValue::set));

        try {
            parser.add(flag("--exists", "n", "Usage", boolValue::set));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Option --exists already exists"));
        }

        try {
            parser.add(flag("--parent", "n", "Usage", boolValue::set));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Option --parent already exists in parent"));
        }

        try {
            parser.add(flag("--new", "e", "Usage", boolValue::set));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Short option -e already exists"));
        }

        try {
            parser.add(flag("--new3", "p", "Usage", boolValue::set));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Short option -p already exists in parent"));
        }

        try {
            parser.add(flag("--new2", "2", "Usage", boolValue::set).negateName("--exists"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Flag --exists already exists"));
        }
    }
}
