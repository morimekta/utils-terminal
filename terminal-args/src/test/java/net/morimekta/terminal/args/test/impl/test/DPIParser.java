package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;

public class DPIParser implements ValueParser<DPI> {
    @Override
    public DPI parse(String value) {
        try {
            return new DPI(Double.parseDouble(value));
        } catch (NumberFormatException e) {
            throw new ArgException(e.getMessage(), e);
        }
    }
}
