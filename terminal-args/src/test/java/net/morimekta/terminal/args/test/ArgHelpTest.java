/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.test;

import net.morimekta.io.tty.TTY;
import net.morimekta.terminal.args.ArgHelp;
import net.morimekta.terminal.args.ArgParser;
import net.morimekta.terminal.args.Option;
import net.morimekta.testing.junit5.ConsoleExtension;
import net.morimekta.testing.junit5.ConsoleSize;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static net.morimekta.terminal.args.ArgHelp.argHelp;
import static net.morimekta.terminal.args.Argument.argument;
import static net.morimekta.terminal.args.Flag.flagLong;
import static net.morimekta.terminal.args.Flag.flagShort;
import static net.morimekta.terminal.args.Option.option;
import static net.morimekta.terminal.args.Option.optionLong;
import static net.morimekta.terminal.args.Option.optionShort;
import static net.morimekta.terminal.args.Property.propertyShort;
import static net.morimekta.terminal.args.SubCommand.subCommand;
import static net.morimekta.terminal.args.ValueParser.i32;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Tests for the sub-command handling.
 */
@ExtendWith(ConsoleExtension.class)
@ConsoleSize(cols = 120, rows = 44)
public class ArgHelpTest {
    public interface Sub {
    }

    public static class Command1 implements Sub {
        public Command1(ArgParser.Builder args) {
            args.add(option("--arg_1", "a", "Arg 1", i32(this::setI)));
            args.add(option("--arg_2", "b", "Arg 2", this::setS));
        }

        private int    i = 12;
        private String s = "";

        private void setI(int i) {
            this.i = i;
        }

        private void setS(String s) {
            this.s = s;
        }

        @Override
        public String toString() {
            return "Command1{i=" + i + ",s=" + s + "}";
        }
    }

    public static class Command2 implements Sub {
        public Command2(ArgParser.Builder args) {
            args.add(option("--foo", "f", "Foo foo foo", i32(this::setI)));
            args.add(option("--bar", "b", "Bar bar bar", this::setBar));
        }

        private int    foo = 12;
        private String bar = "";

        private void setI(int foo) {
            this.foo = foo;
        }

        private void setBar(String bar) {
            this.bar = bar;
        }

        @Override
        public String toString() {
            return "Command1{foo=" + foo + ",bar=" + bar + "}";
        }
    }

    private AtomicReference<String> strValue;
    private AtomicInteger           intValue;
    private AtomicBoolean           boolValue;
    private ArrayList<String>       strList;
    private Map<String, String>     properties;
    private AtomicReference<Sub>    command;

    @BeforeEach
    public void setUp() {
        strValue = new AtomicReference<>();
        intValue = new AtomicInteger();
        boolValue = new AtomicBoolean();
        strList = new ArrayList<>();
        command = new AtomicReference<>();
        properties = new HashMap<>();
    }

    @Test
    public void testHelp_BadArgs(TTY tty) {
        var parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .add(flagLong("--arg2", "Another boolean", boolValue::set))
                .build();
        var helpB = argHelp(parser)
                .usageWidth(80)
                .showSubCommands(false);
        try {
            helpB.usingTTYWidth(tty, 0);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid max width 0"));
        }
    }

    @Test
    public void testHelp_SubCommands() {
        var parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .add(option("--arg1", "A", "Integer value", i32().andApply(intValue::set)).metaVar(
                        "I").defaultValue("55"))
                .add(flagLong("--arg2", "Another boolean", boolValue::set))
                .add(argument("type", "Some type", strValue::set).defaultValue("no-type")
                                                                 .when(s -> !s.startsWith("/")))
                .withSubCommands("file", "Extra files", command::set)
                .add(subCommand("sub1", "Sub sub one", Command1::new))
                .add(subCommand("sub2", "Sub sub two", Command2::new).alias("foo"))
                .build();
        var help = argHelp(parser)
                .usageWidth(100)
                .showDefaults(false)
                .subCommandsHeader("Available Subs:")
                .build();

        assertThat(getSingleLineUsage(help),
                   is("[-A I] [--arg2] [type] file [...]"));
        assertThat(getPreamble(help),
                   is("" +
                      "Git Tools - 0.2.5\n" +
                      "Usage: gt [-A I] [--arg2] [type] file [...]\n"));
        assertThat(getHelp(help),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt [-A I] [--arg2] [type] file [...]\n" +
                      "\n" +
                      " --arg1 (-A) I : Integer value\n" +
                      " --arg2        : Another boolean\n" +
                      " type          : Some type\n" +
                      " file          : Extra files\n" +
                      "\n" +
                      "Available Subs:\n" +
                      "\n" +
                      " sub1 : Sub sub one\n" +
                      " sub2 : Sub sub two\n"));

        var subParser = parser.getSubCommandSet().parserForSubCommand("sub1");
        var subHelp = argHelp(subParser)
                .usageWidth(100)
                .build();
        assertThat(getSingleLineUsage(subHelp),
                   is("[-a A] [-b B]"));
        assertThat(getPreamble(subHelp),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt sub1 [-a A] [-b B]\n"));
        assertThat(getHelp(subHelp),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt sub1 [-a A] [-b B]\n" +
                      "\n" +
                      " --arg_1 (-a) A : Arg 1\n" +
                      " --arg_2 (-b) B : Arg 2\n"));
    }

    @Test
    public void testHelp_LongLines(TTY tty) {
        var parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .add(option("--arg1", "a", "Integer value", i32().andApply(intValue::set))
                             .metaVar("I")
                             .defaultValue("55"))
                .add(propertyShort('D', "System property", properties::put))
                .add(flagLong("--arg2", "Another boolean", boolValue::set))
                .add(optionShort("A", "Integer value (#2), " +
                                      "This one has a really long description, that should be " +
                                      "wrapped at some point. This extra text is just to make " +
                                      "sure it actually wraps...", i32().andApply(intValue::set))
                             .metaVar("I")
                             .defaultValue("55"))
                .add(optionLong("--this-is-somewhat-long-long-option",
                                "ANOTHER Integer value (#3), " +
                                "This one has a really long description, that should be " +
                                "wrapped at some point. This extra text is just to make " +
                                "sure it actually wraps... This one should also be side-" +
                                "shifted on the first line.", i32().andApply(intValue::set))
                             .metaVar("V")
                             .defaultValue("55"))
                .add(option("--this-is-a-really-long-long-option", "cdefgh",
                            "ANOTHER Integer value (#2), " +
                            "This one has a really long description, that should be " +
                            "wrapped at some point. This extra text is just to make " +
                            "sure it actually wraps... Though this one should be shifted " +
                            "entirely to the next line.", i32().andApply(intValue::set))
                             .metaVar("true-value")
                             .defaultValue("55"))
                .build();

        ArgHelp help = argHelp(parser)
                .usingTTYWidth(tty, 80)
                .build();

        assertThat(getSingleLineUsage(help),
                   is("[-a I] [-Dkey=val ...] [--arg2] [-A I]" +
                      " [--this-is-somewhat-long-long-option V] [-c true-value]"));
        assertThat(getPreamble(help),
                   is("" +
                      "Git Tools - 0.2.5\n" +
                      "Usage: gt [-a I] [-Dkey=val ...] [--arg2] [-A I]\n" +
                      "          [--this-is-somewhat-long-long-option V] [-c true-value]\n"));
        assertThat(getHelp(help),
                   Is.is("" +
                         "Git Tools - 0.2.5\n" +
                         "Usage: gt [-a I] [-Dkey=val ...] [--arg2] [-A I]\n" +
                         "          [--this-is-somewhat-long-long-option V] [-c true-value]\n" +
                         "\n" +
                         " --arg1 (-a) I          : Integer value (default: 55)\n" +
                         " -Dkey=val              : System property\n" +
                         " --arg2                 : Another boolean\n" +
                         " -A I                   : Integer value (#2), This one has a really long\n" +
                         "                          description, that should be wrapped at some point.\n" +
                         "                          This extra text is just to make sure it actually\n" +
                         "                          wraps... (default: 55)\n" +
                         " --this-is-somewhat-long-long-option V : ANOTHER Integer value (#3), This one\n" +
                         "                          has a really long description, that should be wrapped\n" +
                         "                          at some point. This extra text is just to make sure it\n" +
                         "                          actually wraps... This one should also be side-shifted\n" +
                         "                          on the first line. (default: 55)\n" +
                         " --this-is-a-really-long-long-option (-c, -d, -e, -f, -g, -h) true-value\n" +
                         "                          ANOTHER Integer value (#2), This one has a really long\n" +
                         "                          description, that should be wrapped at some point.\n" +
                         "                          This extra text is just to make sure it actually\n" +
                         "                          wraps... Though this one should be shifted entirely to\n" +
                         "                          the next line. (default: 55)\n"));
        assertThat(getHelp(argHelp(parser)
                                   .usingTTYWidth(tty)
                                   .build()),
                   Is.is("" +
                         "Git Tools - 0.2.5\n" +
                         "Usage: gt [-a I] [-Dkey=val ...] [--arg2] [-A I] [--this-is-somewhat-long-long-option V] [-c true-value]\n" +
                         "\n" +
                         " --arg1 (-a) I                        : Integer value (default: 55)\n" +
                         " -Dkey=val                            : System property\n" +
                         " --arg2                               : Another boolean\n" +
                         " -A I                                 : Integer value (#2), This one has a really long description, that should be\n" +
                         "                                        wrapped at some point. This extra text is just to make sure it actually wraps...\n" +
                         "                                        (default: 55)\n" +
                         " --this-is-somewhat-long-long-option V : ANOTHER Integer value (#3), This one has a really long description, that should\n" +
                         "                                        be wrapped at some point. This extra text is just to make sure it actually\n" +
                         "                                        wraps... This one should also be side-shifted on the first line. (default: 55)\n" +
                         " --this-is-a-really-long-long-option (-c, -d, -e, -f, -g, -h) true-value\n" +
                         "                                        ANOTHER Integer value (#2), This one has a really long description, that should\n" +
                         "                                        be wrapped at some point. This extra text is just to make sure it actually\n" +
                         "                                        wraps... Though this one should be shifted entirely to the next line.\n" +
                         "                                        (default: 55)\n"));
    }

    @Test
    public void testHelp_ShowHidden() {
        var parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .add(flagShort("A", "Shown", boolValue::set))
                .add(flagShort("B", "Hidden", boolValue::set).hidden())
                .add(argument("shown", "Shown", strList::add))
                .add(argument("hidden", "Hidden", strList::add).hidden())
                .withSubCommands("name", "exp", command::set)
                .add(subCommand("cmd1", "Shown", Command1::new))
                .add(subCommand("cmd2", "Hidden", Command2::new).hidden())
                .build();

        ArgHelp help = argHelp(parser)
                .usageWidth(80)
                .build();

        assertThat(getSingleLineUsage(help),
                   is("[-A] [shown] name [...]"));
        assertThat(getPreamble(help),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt [-A] [shown] name [...]\n"));
        assertThat(getHelp(help),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt [-A] [shown] name [...]\n" +
                      "\n" +
                      " -A     : Shown\n" +
                      " shown  : Shown\n" +
                      " name   : exp\n" +
                      "\n" +
                      "Available sub-commands:\n" +
                      "\n" +
                      " cmd1 : Shown\n"));

        help = argHelp(parser)
                .usageWidth(80)
                .showHidden(true)
                .build();

        assertThat(getSingleLineUsage(help),
                   is("[-AB] [shown] [hidden] name [...]"));
        assertThat(getPreamble(help),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt [-AB] [shown] [hidden] name [...]\n"));
        assertThat(getHelp(help),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt [-AB] [shown] [hidden] name [...]\n" +
                      "\n" +
                      " -A     : Shown\n" +
                      " -B     : Hidden\n" +
                      " shown  : Shown\n" +
                      " hidden : Hidden\n" +
                      " name   : exp\n" +
                      "\n" +
                      "Available sub-commands:\n" +
                      "\n" +
                      " cmd1 : Shown\n" +
                      " cmd2 : Hidden\n"));

        var helpB = argHelp(parser)
                .usageWidth(80)
                .showSubCommands(false);

        assertThat(getSingleLineUsage(helpB),
                   is("[-A] [shown] name [...]"));
        assertThat(getPreamble(helpB),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt [-A] [shown] name [...]\n"));
        assertThat(getHelp(helpB),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt [-A] [shown] name [...]\n" +
                      "\n" +
                      " -A     : Shown\n" +
                      " shown  : Shown\n" +
                      " name   : exp\n"));
    }

    @Test
    public void testHelp_SortedOpts() {
        var parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .add(flagShort("A", "Shown", boolValue::set))
                .add(flagShort("B", "Hidden", boolValue::set))
                .add(argument("shown", "Shown", strList::add))
                .add(argument("hidden", "Hidden", strList::add))
                .withSubCommands("name", "exp", command::set)
                .add(subCommand("cmd1", "Shown", Command1::new))
                .add(subCommand("cmd2", "Hidden", Command2::new))
                .build();
        var helpB = argHelp(parser)
                .usageWidth(80)
                .withOptionsComparator(Comparator.comparing(Option::getUsage));
        assertThat(getSingleLineUsage(helpB),
                   is("[-BA] [shown] [hidden] name [...]"));
        assertThat(getPreamble(helpB),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt [-BA] [shown] [hidden] name [...]\n"));
        assertThat(getHelp(helpB),
                   is("Git Tools - 0.2.5\n" +
                      "Usage: gt [-BA] [shown] [hidden] name [...]\n" +
                      "\n" +
                      " -B     : Hidden\n" +  // sorted
                      " -A     : Shown\n" +
                      " shown  : Shown\n" +   // not sorted
                      " hidden : Hidden\n" +
                      " name   : exp\n" +
                      "\n" +
                      "Available sub-commands:\n" +
                      "\n" +
                      " cmd1 : Shown\n" +   // not sorted
                      " cmd2 : Hidden\n"));
    }

    private static String getSingleLineUsage(ArgHelp help) {
        var writer = new ByteArrayOutputStream();
        help.printSingleLineUsage(new PrintStream(writer, false, StandardCharsets.UTF_8));
        return writer.toString(StandardCharsets.UTF_8);
    }

    private static String getPreamble(ArgHelp help) {
        var writer = new ByteArrayOutputStream();
        help.printPreamble(new PrintStream(writer, false, StandardCharsets.UTF_8));
        return writer.toString();
    }

    private static String getHelp(ArgHelp help) {
        var writer = new ByteArrayOutputStream();
        help.printHelp(new PrintStream(writer, false, StandardCharsets.UTF_8));
        return writer.toString();
    }

    private static String getSingleLineUsage(ArgHelp.Builder help) {
        var writer = new ByteArrayOutputStream();
        help.printSingleLineUsage(new PrintStream(writer, false, StandardCharsets.UTF_8));
        return writer.toString(StandardCharsets.UTF_8);
    }

    private static String getPreamble(ArgHelp.Builder help) {
        var writer = new ByteArrayOutputStream();
        help.printPreamble(new PrintStream(writer, false, StandardCharsets.UTF_8));
        return writer.toString();
    }

    private static String getHelp(ArgHelp.Builder help) {
        var writer = new ByteArrayOutputStream();
        help.printHelp(new PrintStream(writer, false, StandardCharsets.UTF_8));
        return writer.toString();
    }
}
