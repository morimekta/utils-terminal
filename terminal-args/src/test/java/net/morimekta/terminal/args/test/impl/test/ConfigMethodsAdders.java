package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.annotations.ArgOptions;
import net.morimekta.terminal.args.annotations.ArgValueParser;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unused")
public class ConfigMethodsAdders {
    private final List<String> optionList = new ArrayList<>();

    public ConfigMethodsAdders() {}

    @ArgOptions(name = "option-list", shortChar = 'O', usage = "Option List...")
    public List<String> getOptionList() {
        return optionList;
    }

    private List<Duration> durations;

    @DummyAnnotation
    public List<Duration> getDurations() {
        return durations;
    }

    @DummyAnnotation
    public void addDurations(Duration duration) {
        if (this.durations == null) {
            this.durations = new ArrayList<>();
        }
        this.durations.add(duration);
    }

    private Collection<Resolution> typeDefined;

    public Collection<Resolution> getTypeDefined() {
        return typeDefined;
    }

    public void setTypeDefined(Collection<Resolution> typeDefined) {
        this.typeDefined = typeDefined;
    }

    @ArgValueParser(DPIParser.class)
    private final Set<DPI> annotationDefined = new HashSet<>();

    public Set<DPI> getAnnotationDefined() {
        return annotationDefined;
    }
}
