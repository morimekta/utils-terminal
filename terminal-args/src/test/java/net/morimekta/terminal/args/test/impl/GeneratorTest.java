package net.morimekta.terminal.args.test.impl;

import net.morimekta.terminal.args.ArgHelp;
import net.morimekta.terminal.args.ArgNameFormat;
import net.morimekta.terminal.args.ArgParser;
import net.morimekta.terminal.args.test.impl.test.ConfigFieldsComplex;
import net.morimekta.terminal.args.test.impl.test.ConfigFieldsComplexInherited;
import net.morimekta.terminal.args.test.impl.test.ConfigFieldsComplexInheritedOverride;
import net.morimekta.terminal.args.test.impl.test.ConfigFieldsIgnored;
import net.morimekta.terminal.args.test.impl.test.ConfigFieldsInherited;
import net.morimekta.terminal.args.test.impl.test.ConfigFieldsPrimitives;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsAdders;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsComplex;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsIgnored;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsInherited;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsPrimitives;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsPutters;
import net.morimekta.terminal.args.test.impl.test.DPI;
import net.morimekta.terminal.args.test.impl.test.MyEnum;
import net.morimekta.terminal.args.test.impl.test.Resolution;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleExtension;
import net.morimekta.testing.junit5.ConsoleSize;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.logging.Logger.getLogger;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(ConsoleExtension.class)
@ConsoleSize(cols = 120, rows = 44)
@SuppressWarnings("unused")
public class GeneratorTest {
    private static final Logger LOGGER = Logger.getLogger(GeneratorTest.class.getName());

    @BeforeAll
    public static void setUpClass() throws InterruptedException {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
        getLogger("").setLevel(Level.FINEST);
        Thread.sleep(100);
    }

    @Test
    public void testArgBuilder_PrimitiveFields() {
        var config = new ConfigFieldsPrimitives();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgs(config)
                            .build();
        args.parse("--b",
                   "--bool",
                   "--bt=0x33",
                   "--i8", "8",
                   "--s", "0737",
                   "--i16", "0x55",
                   "--i", "42",
                   "--i32", "32",
                   "--l", "0x30",
                   "--i64", "042",
                   "--big-int", "123456789012345678901234567890",
                   "--f", "32.0",
                   "--flt", "32.0",
                   "--d", "64.0",
                   "--dbl", "64.0",
                   "--big-decimal", "123.456",
                   "--my-enum", "B",
                   "--dur", "55s",
                   "--name", "name",
                   "--path", "/tmp",
                   "--file", "/tmp");
        args.validate();
        assertThat(config.b, is(true));
        assertThat(config.bool, is(true));
        assertThat(config.bt, is((byte) 0x33));
        assertThat(config.i8, is((byte) 8));
        assertThat(config.s, is((short) 479));
        assertThat(config.i16, is((short) 0x55));
        assertThat(config.i, is(42));
        assertThat(config.i32, is(32));
        assertThat(config.l, is(48L));
        assertThat(config.i64, is(34L));
        assertThat(config.bigInt, is(new BigInteger("123456789012345678901234567890")));
        assertThat(config.f, is(32f));
        assertThat(config.flt, is(32f));
        assertThat(config.d, is(64d));
        assertThat(config.dbl, is(64d));
        assertThat(config.bigDecimal, is(new BigDecimal("123.456")));
        assertThat(config.myEnum, is(MyEnum.B));
        assertThat(config.dur, is(Duration.ofSeconds(55)));
        assertThat(config.name, is("name"));
        assertThat(config.path, is(Path.of("/tmp")));
        assertThat(config.file, is(new File("/tmp")));
    }

    @Test
    public void testArgBuilder_InheritedFields() {
        var config = new ConfigFieldsInherited();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgs(config)
                            .build();
        args.parse("--name", "foo",
                   "--height", "33.66");
        args.validate();
        assertThat(config.name, is("foo"));
        assertThat(config.height, is(33.66));
    }

    @Test
    public void testArgBuilder_FieldsComplex() {
        var config = new ConfigFieldsComplex();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgs(config)
                            .build();
        args.parse("--resolution=1920x1280",
                   "--dpi", "300.5",
                   "--map", "foo=55",
                   "--map", "bar=128",
                   "--hidden_map", "foo=bar",
                   "--durations=42s",
                   "--longs=1",
                   "--longs=2",
                   "--hidden_list", "foo",
                   "--hidden_list", "bar",
                   "--fields_name=FOO",
                   "--other_value=55");

        assertThat(config.resolution, is(notNullValue()));
        assertThat(config.resolution.x, is(1920));
        assertThat(config.resolution.y, is(1280));
        assertThat(config.dpi, is(notNullValue()));
        assertThat(config.dpi.dpi, is(300.5));
        assertThat(config.map, is(Map.of("foo", 55, "bar", 128)));
        assertThat(config.hiddenMap, is(Map.of("foo", "bar")));
        assertThat(config.durations, is(List.of(Duration.ofSeconds(42))));
        assertThat(config.longs, is(Set.of(1L, 2L)));
        assertThat(config.hiddenList, is(List.of("foo", "bar")));

        assertThat(config.simple.name, is("FOO"));
        assertThat(config.other.value, is(55));
    }

    @Test
    public void testArgBuilder_FieldsComplexInherited() {
        var config = new ConfigFieldsComplexInherited();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgs(config)
                            .build();
        args.parse("--fields_name=FOO",
                   "--other_name=55",
                   "--other_simple_name=NOO",
                   "--simple_other_name=YES");

        assertThat(config.simple.name, is("FOO"));
        assertThat(config.other.name, is("55"));
        assertThat(config.otherSimple.name, is("NOO"));
        assertThat(config.simpleOther.name, is("YES"));
    }

    @Test
    public void testArgBuilder_FieldsComplexInheritedOverride() {
        var config = new ConfigFieldsComplexInheritedOverride();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgs(config)
                            .build();

        args.parse("--fieldsName=FOO",
                   "--otherName=55",
                   "--otherSimpleName=NOO",
                   "--simpleOtherName=YES");

        assertThat(config.simple.name, is("FOO"));
        assertThat(config.other.name, is("55"));
        assertThat(config.otherSimple.name, is("NOO"));
        assertThat(config.simpleOther.name, is("YES"));
    }

    @Test
    public void testArgBuilder_FieldsIgnored(Console console) {
        var config = new ConfigFieldsIgnored();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgs(config)
                            .build();
        args.parse("--bool");

        assertThat(config.bool, is(true));

        var help = ArgHelp.argHelp(args).usingTTYWidth(console.tty()).build();

        assertThat(getHelp(help), is(
                "baz - bar\n" +
                "Usage: foo [--bool]\n" +
                "\n" +
                " --bool : Set bool.\n" +
                ""));
    }

    @Test
    public void testArgBuilder_MethodsPrimitives() {
        var config = new ConfigMethodsPrimitives();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgsNameFormat(ArgNameFormat.CAMEL)
                            .generateArgs(config)
                            .build();
        args.parse("--b",
                   "--bool",
                   "--bt=0x33",
                   "--i8", "8",
                   "--s", "0737",
                   "--i16", "0x55",
                   "--i", "42",
                   "--i32", "32",
                   "--l", "0x30",
                   "--i64", "042",
                   "--bigInt", "123456789012345678901234567890",
                   "--f", "32.0",
                   "--flt", "32.0",
                   "--d", "64.0",
                   "--dbl", "64.0",
                   "--myEnum", "B",
                   "--dur", "55s",
                   "--name", "name",
                   "--path", "/tmp",
                   "--file", "/tmp");
        assertThat(config.isB(), is(true));
        assertThat(config.getBool(), is(Boolean.TRUE));
        assertThat(config.getBt(), is((byte) 51));
        assertThat(config.getI8(), is((byte) 8));
        assertThat(config.getS(), is((short) 479));
        assertThat(config.getI16(), is((short) 85));
        assertThat(config.getI(), is(42));
        assertThat(config.getI32(), is(32));
        assertThat(config.getL(), is(48L));
        assertThat(config.getI64(), is(34L));
        assertThat(config.getBigInt(), is(new BigInteger("123456789012345678901234567890")));
        assertThat(config.getF(), is(32f));
        assertThat(config.getFlt(), is(32f));
        assertThat(config.getD(), is(64.d));
        assertThat(config.getDbl(), is(64.d));
        assertThat(config.getMyEnum(), is(MyEnum.B));
        assertThat(config.getDur(), is(Duration.ofSeconds(55)));
        assertThat(config.getPath(), is(Path.of("/tmp")));
        assertThat(config.getFile(), is(new File("/tmp")));
    }

    @Test
    public void testArgBuilder_MethodsInherited() {
        var config = new ConfigMethodsInherited();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgsNameFormat(ArgNameFormat.CAMEL)
                            .generateArgs(config)
                            .build();
        args.parse("--name", "foo",
                   "--height", "33.66");
        args.validate();
        assertThat(config.getName(), is("foo"));
        assertThat(config.getHeight(), is(33.66));
    }

    @Test
    public void testArgBuilder_MethodsComplex() {
        var config = new ConfigMethodsComplex();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgsNameFormat(ArgNameFormat.SNAKE)
                            .generateArgs(config)
                            .build();
        args.parse("--booleans",
                   "-bb",
                   "--tags", "bar",
                   "--tags", "baz",
                   "--plain_text",
                   "--chained_name", "foo");
        args.validate();

        assertThat(config.getBooleans(), is(3));
        assertThat(config.getTags(), is(Set.of("bar", "baz")));
        assertThat(config.plainText(), is(true));
        assertThat(config.chained().getName(), is("foo"));
    }

    @Test
    public void testArgBuilder_MethodsAdders() {
        var config = new ConfigMethodsAdders();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgsNameFormat(ArgNameFormat.SNAKE)
                            .generateArgs(config)
                            .build();
        args.parse("--option-list", "foo",
                   "-Obar",
                   "--durations", "55s",
                   "--type_defined", "1920x1080",
                   "--annotation_defined", "55.7");
        args.validate();

        assertThat(config.getOptionList(), is(List.of("foo", "bar")));
        assertThat(config.getDurations(), is(List.of(Duration.ofSeconds(55))));
        assertThat(config.getTypeDefined(), is(List.of(new Resolution(1920, 1080))));
        assertThat(config.getAnnotationDefined(), is(Set.of(new DPI(55.7))));
    }

    @Test
    public void testArgBuilder_MethodsPutters() {
        var config = new ConfigMethodsPutters();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgsNameFormat(ArgNameFormat.CAMEL)
                            .generateArgs(config)
                            .build();
        args.parse("--option-map", "foo=bar",
                   "-Obar=foo",
                   "--parameterizedMap", "123.4=1280x768",
                   "--parameterizedPutter", "1920x1080=55",
                   "--hiddenMap", "NOPE=yes",
                   "--methodPutter", "A=B");
        args.validate();

        assertThat(config.getOptionMap(), is(mapOf("foo", "bar", "bar", "foo")));
        assertThat(config.getParameterizedMap(), is(mapOf(new DPI(123.4), new Resolution(1280, 768))));
        assertThat(config.getParameterizedPutter(), is(mapOf(new Resolution(1920, 1080), new DPI(55))));
        assertThat(config.getHiddenMap(), is(mapOf("NOPE", "yes")));
        assertThat(config.methodPutter(), is(mapOf("A", "B")));
    }

    @Test
    public void testArgBuilder_MethodsIgnored(Console console) {
        var config = new ConfigMethodsIgnored();
        var args = ArgParser.argParser("foo", "bar", "baz")
                            .generateArgsNameFormat(ArgNameFormat.CAMEL)
                            .generateArgs(config)
                            .build();
        args.parse("--bool");
        assertThat(config.getBool(), is(Boolean.TRUE));

        var help = ArgHelp.argHelp(args).usingTTYWidth(console.tty()).build();

        assertThat(getHelp(help), is(
                "baz - bar\n" +
                "Usage: foo [--bool]\n" +
                "\n" +
                " --bool : Apply setBool().\n" +
                ""));
    }

    private static String getHelp(ArgHelp help) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (PrintStream pw = new PrintStream(out, true, UTF_8)) {
            help.printHelp(pw);
        }
        return out.toString(UTF_8);
    }
}
