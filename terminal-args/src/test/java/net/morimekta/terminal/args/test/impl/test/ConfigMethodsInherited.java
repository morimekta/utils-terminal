package net.morimekta.terminal.args.test.impl.test;

@SuppressWarnings("unused")
public class ConfigMethodsInherited extends ConfigMethodsSimple {
    private double height;

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
