/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.test;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.Flag;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static net.morimekta.terminal.args.Flag.flag;
import static net.morimekta.terminal.args.Flag.flagLong;
import static net.morimekta.terminal.args.Flag.flagShort;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests for flag arguments.
 */
public class FlagTest {
    @Test
    public void testConstructor() {
        AtomicReference<Boolean> ref = new AtomicReference<>();
        Flag flag = flag("--stuff", "S", "Do stuff", ref::set).build();

        assertNull(flag.getSingleLineUsage());
        assertNull(flag.getNegateName());
        assertEquals("Do stuff", flag.getUsage());
        assertNull(flag.getDefaultValue());
        assertFalse(flag.isHidden());
        assertFalse(flag.isRepeated());
        assertFalse(flag.isRequired());

        flag.validate();

        flag = flagLong("--stuff", "Do stuff", ref::set)
                .negateName("--no-stuff")
                .defaultOn()
                .build();
        assertEquals("[--stuff]", flag.getSingleLineUsage());
        assertEquals("--no-stuff", flag.getNegateName());
        assertEquals("on", flag.getDefaultValue());
        assertFalse(flag.isHidden());
        assertFalse(flag.isRepeated());
        assertFalse(flag.isRequired());

        flag.validate();

        flag = flagShort("S", "Do stuff", ref::set)
                .negateShortName('s')
                .build();
        assertEquals("Ss", flag.getShortNames());
        assertNull(flag.getSingleLineUsage());

        try {
            flag(null, null, "Fail", ref::set);
            fail("No exception on invalid constructor");
        } catch (NullPointerException e) {
            assertEquals("name == null", e.getMessage());
        }
    }

    @Test
    public void testApply() {
        AtomicReference<Boolean> ref = new AtomicReference<>();

        Flag flag = flag("--stuff", "S", "Do stuff", ref::set).build();
        flag.applyShort("S", List.of("-S"));
        assertTrue(ref.get());

        flag = flag("--stuff", "S", "Do stuff", ref::set).hidden().negateName("--no_stuff").build();
        flag.apply(List.of("--no_stuff"));
        assertFalse(ref.get());

        flag = flag("--stuff", "S", "Do stuff", ref::set).defaultValue(false).build();
        flag.apply(List.of("--stuff"));
        assertThat(flag.getDefaultValue(), is("false"));
        assertTrue(ref.get());

        flag = flag("--stuff", "S", "Do stuff", ref::set).build();
        flag.apply(List.of("--stuff=FALSE"));
        assertFalse(ref.get());

        flag = flag("--stuff", "S", "Do stuff", ref::set).defaultOff().build();
        flag.apply(List.of("--stuff=TRUE"));
        assertThat(flag.getDefaultValue(), is("off"));
        assertTrue(ref.get());

        flag = flag("--stuff", "S", "Do stuff", ref::set).hidden().negateShortName('s').build();
        flag.applyShort("s", List.of("-s"));
        assertFalse(ref.get());

        // And if used to increment / decrement.
        AtomicInteger i = new AtomicInteger();
        flag = flag("--stuff", "S", "Do stuff", b -> {
            if (b) {
                i.incrementAndGet();
            } else {
                i.decrementAndGet();
            }
        }).repeated().negateShortName('s').build();
        flag.applyShort("SS", List.of("-SS"));
        flag.applyShort("S", List.of("-SS"));
        assertThat(i.get(), is(2));

        flag.applyShort("ss", List.of("-ss"));
        flag.applyShort("s", List.of("-ss"));
        assertThat(i.get(), is(0));
    }

    @Test
    public void testApplyFails() {
        AtomicReference<Boolean> ref = new AtomicReference<>();

        Flag flag = flag("--stuff", "S", "Do stuff", ref::set).build();
        flag.applyShort("SS", List.of("-SS"));
        try {
            flag.applyShort("S", List.of("-SS"));
            fail("No exception on over-applying");
        } catch (ArgException e) {
            assertEquals("--stuff is already applied", e.getMessage());
        }

        flag = flag("--stuff", "S", "Do stuff", ref::set).build();
        flag.apply(List.of("--stuff"));
        try {
            flag.apply(List.of("--no_stuff"));
            fail("No exception on over-applying");
        } catch (ArgException e) {
            assertEquals("--stuff is already applied", e.getMessage());
        }

        flag = flagShort("S", "Do stuff", ref::set).build();
        try {
            flag.apply(List.of("--S"));
            fail("No exception on mis-applying");
        } catch (IllegalStateException e) {
            assertEquals("No long option for -[S]", e.getMessage());
        }

        flag = flag("--stuff", "S", "Do stuff", ref::set).build();
        try {
            flag.apply(List.of("--S"));
            fail("No exception on mis-applying");
        } catch (IllegalArgumentException e) {
            assertEquals("Argument not matching flag --stuff: --S", e.getMessage());
        }
    }
}
