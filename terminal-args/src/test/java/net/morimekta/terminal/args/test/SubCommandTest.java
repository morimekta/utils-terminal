/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.test;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ArgParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static net.morimekta.terminal.args.Argument.argument;
import static net.morimekta.terminal.args.Flag.flagLong;
import static net.morimekta.terminal.args.Option.option;
import static net.morimekta.terminal.args.SubCommand.subCommand;
import static net.morimekta.terminal.args.ValueParser.i32;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests for the sub-command handling.
 */
public class SubCommandTest {
    public interface Sub {
    }

    public static class Command1 implements Sub {
        public Command1(ArgParser.Builder args) {
            args.add(option("--cmd_arg1", "a", "Arg 1", i32(this::setI)));
            args.add(option("--cmd_arg2", "b", "Arg 2", this::setS));
        }

        private int    i = 12;
        private String s = "";

        private void setI(int i) {
            this.i = i;
        }

        private void setS(String s) {
            this.s = s;
        }

        @Override
        public String toString() {
            return "Command1{i=" + i + ",s=" + s + "}";
        }
    }

    public static class Command2 implements Sub {
        public Command2(ArgParser.Builder args) {
            args.add(option("--foo", "f", "Foo foo foo", i32(this::setI)));
            args.add(option("--bar", "b", "Bar bar bar", this::setBar));
        }

        private int    foo = 12;
        private String bar = "";

        private void setI(int foo) {
            this.foo = foo;
        }

        private void setBar(String bar) {
            this.bar = bar;
        }

        @Override
        public String toString() {
            return "Command2{foo=" + foo + ",bar=" + bar + "}";
        }
    }

    private AtomicReference<String> strValue;
    private AtomicInteger           intValue;
    private AtomicBoolean           boolValue;
    private AtomicReference<Sub>    command;

    @BeforeEach
    public void setUp() {
        strValue = new AtomicReference<>();
        intValue = new AtomicInteger();
        boolValue = new AtomicBoolean();
        command = new AtomicReference<>();
    }

    @Test
    public void testSubCommands() {
        ArgParser parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .add(option("--arg1", "A", "Integer value", i32().andApply(intValue::set)))
                .add(flagLong("--arg2", "Another boolean", boolValue::set))
                .add(argument("type", "Some type", strValue::set).defaultValue("no-type")
                                                                 .when(s -> !s.startsWith("/")))
                .withSubCommands("file", "Extra files", command::set)
                .add(subCommand("sub1", "Sub sub one", Command1::new))
                .add(subCommand("sub2", "Sub sub two", Command2::new).alias("foo"))
                .build();

        try {
            // required by default.
            parser.validate();
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("file not chosen"));
        }
        parser.parse("type", "sub2");
        parser.validate();
        assertThat(command.get(), is(instanceOf(Command2.class)));

        var scs = parser.getSubCommandSet();
        assertThat(scs, is(notNullValue()));
        assertThat(scs.getName(), is("file"));
        assertThat(scs.getUsage(), is("Extra files"));
        assertThat(scs.getSubCommands(), hasSize(2));
        assertThat(scs.getSubCommandByName("sub1"), is(notNullValue()));
        assertThat(scs.getSubCommandByName("sub2"), is(notNullValue()));
        assertThat(scs.getSubCommandByName("sub3"), is(nullValue()));
        var sub1 = scs.getSubCommandByName("sub1");
        assertThat(sub1.getName(), is("sub1"));
        assertThat(sub1.getUsage(), is("Sub sub one"));
        assertThat(sub1.getAliases(), is(List.of()));
        var sub2 = scs.getSubCommandByName("sub2");
        assertThat(sub2.getName(), is("sub2"));
        assertThat(sub2.getUsage(), is("Sub sub two"));
        assertThat(sub2.getAliases(), is(List.of("foo")));
        assertThat(sub2.toString(), is("SubCommand{sub2}"));
        assertThat(scs.getSubCommandByName("foo"), is(sameInstance(sub2)));

        assertThat(scs.toString(), is("SubCommandSet{file, [SubCommand{sub1}, SubCommand{sub2}]}"));

        try {
            scs.parserForSubCommand("nope");
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("Unknown sub-command nope."));
        }
    }

    @Test
    public void testBuildEmpty() {
        try {
            ArgParser.argParser("gt", "0.2.5", "Git Tools")
                     .withSubCommands("file", "Extra files", command::set)
                     .build();
            fail("No exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("No sub-commands added on sub-command set."));
        }
    }

    @Test
    public void testSubCommands_DoubleDefault() {
        try {
            ArgParser.argParser("gt", "0.2.5", "Git Tools")
                     .withSubCommands("file", "Extra files", command::set)
                     .add(subCommand("sub1", "Sub sub one", Command1::new))
                     .add(subCommand("sub2", "Sub sub two", Command2::new).alias("foo"))
                     .defaultCommand("sub1")
                     .defaultCommand("sub2")
                     .build();
            fail("No exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Default SubCommand already set for sub2."));
        }
    }

    @Test
    public void testSubCommands_DefaultCommand() {
        ArgParser parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .withSubCommands("file", "Extra files", command::set)
                .add(subCommand("sub1", "Sub sub one", Command1::new))
                .add(subCommand("sub2", "Sub sub two", Command2::new).alias("foo"))
                .defaultCommand("sub2")
                .build();
        var scs = parser.getSubCommandSet();
        assertThat(scs.getSingleLineUsage(), is("[file [...]]"));
        assertThat(scs.getDefaultValue(), is("sub2"));
    }

    @Test
    public void testSubCommands_DefaultUnknown() {
        try {
            ArgParser.argParser("gt", "0.2.5", "Git Tools")
                     .withSubCommands("file", "Extra files", command::set)
                     .add(subCommand("sub1", "Sub sub one", Command1::new))
                     .add(subCommand("sub2", "Sub sub two", Command2::new).alias("foo"))
                     .defaultCommand("sub3")
                     .build();
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("SubCommand with name sub3 does not exist."));
        }
    }

    @Test
    public void testSubCommands_OptionalCommand() {
        ArgParser parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .withSubCommands("file", "Extra files", command::set)
                .add(subCommand("sub1", "Sub sub one", Command1::new))
                .add(subCommand("sub2", "Sub sub two", Command2::new).alias("foo"))
                .optionalCommand()
                .build();
        var scs = parser.getSubCommandSet();
        assertThat(scs.getSingleLineUsage(), is("[file [...]]"));
        assertThat(scs.getDefaultValue(), is(nullValue()));
    }

    @Test
    public void testSubCommands_Select() {
        ArgParser parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .add(option("--arg1", "A", "Integer value", i32().andApply(intValue::set)))
                .add(flagLong("--arg2", "Another boolean", boolValue::set))
                .withSubCommands("file", "Extra files", command::set)
                .add(subCommand("sub1", "Sub sub one", Command1::new))
                .add(subCommand("sub2", "Sub sub two", Command2::new).alias("foo"))
                .parse("foo", "--foo", "33");
        parser.validate();
        assertThat(command.get().toString(), is("Command2{foo=33,bar=}"));
    }

    @Test
    public void testSubCommands_SelectDouble() {
        ArgParser parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .withSubCommands("file", "Extra files", command::set)
                .add(subCommand("sub1", "Sub sub one", Command1::new))
                .add(subCommand("sub2", "Sub sub two", Command2::new).alias("foo"))
                .build();
        var scs = parser.getSubCommandSet();
        scs.apply(List.of("sub1"));
        try {
            scs.apply(List.of("sub2"));
            fail("no exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("file already selected"));
        }
    }

    @Test
    public void testSubCommands_SelectUnknown() {
        ArgParser parser = ArgParser
                .argParser("gt", "0.2.5", "Git Tools")
                .withSubCommands("file", "Extra files", command::set)
                .add(subCommand("sub1", "Sub sub one", Command1::new))
                .add(subCommand("sub2", "Sub sub two", Command2::new).alias("foo"))
                .build();
        var scs = parser.getSubCommandSet();
        try {
            scs.apply(List.of("bar"));
            fail("no exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("No such file: bar"));
        }
    }
}
