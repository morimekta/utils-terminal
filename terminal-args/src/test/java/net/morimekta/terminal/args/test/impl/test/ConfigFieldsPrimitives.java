package net.morimekta.terminal.args.test.impl.test;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.time.Duration;

public class ConfigFieldsPrimitives {
    public boolean    b;
    public Boolean    bool;
    public byte       bt;
    public Byte       i8;
    public short      s;
    public Short      i16;
    public int        i;
    public Integer    i32;
    public long       l;
    public Long       i64;
    public BigInteger bigInt;
    public float      f;
    public Float      flt;
    public double     d;
    public Double     dbl;
    public BigDecimal bigDecimal;
    public MyEnum     myEnum;
    public Duration   dur;
    public String     name;
    public Path       path;
    public File       file;
}
