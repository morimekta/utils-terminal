package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.ArgNameFormat;
import net.morimekta.terminal.args.annotations.ArgNaming;

@ArgNaming(ArgNameFormat.CAMEL)
public class ConfigFieldsComplexInheritedOverride extends ConfigFieldsComplex {
    public final ConfigFieldsSimple simpleOther = new ConfigFieldsSimple();
    public       ConfigFieldsSimple otherSimple = new ConfigFieldsSimple();
}
