/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.test.parser;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;
import net.morimekta.terminal.args.parser.IntegerParser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import static java.nio.file.StandardOpenOption.CREATE;
import static net.morimekta.terminal.args.ValueParser.bigDecimal;
import static net.morimekta.terminal.args.ValueParser.bigInt;
import static net.morimekta.terminal.args.ValueParser.dbl;
import static net.morimekta.terminal.args.ValueParser.dir;
import static net.morimekta.terminal.args.ValueParser.file;
import static net.morimekta.terminal.args.ValueParser.flt;
import static net.morimekta.terminal.args.ValueParser.i16;
import static net.morimekta.terminal.args.ValueParser.i32;
import static net.morimekta.terminal.args.ValueParser.i64;
import static net.morimekta.terminal.args.ValueParser.i8;
import static net.morimekta.terminal.args.ValueParser.oneOf;
import static net.morimekta.terminal.args.ValueParser.outputDir;
import static net.morimekta.terminal.args.ValueParser.outputFile;
import static net.morimekta.terminal.args.ValueParser.path;
import static net.morimekta.terminal.args.ValueParser.ui32;
import static net.morimekta.terminal.args.ValueParser.ui64;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * TODO(morimekta): Make a real class description.
 */
public class ValueParserTest {
    @TempDir
    public Path tmp;

    @Test
    public void testAndThen() {
        AtomicInteger integer = new AtomicInteger();
        Map<String, Object> config = new TreeMap<>();

        IntegerParser parser = new IntegerParser();

        parser.andApply(integer::set).accept("4");
        assertEquals(4, integer.get());

        parser.andPut(config::put).put("first", "4");
        assertEquals(4, config.get("first"));

        parser.andPutAs(config::put, "second").accept("6");
        assertEquals(6, config.get("second"));
    }

    @Test
    public void testPutInto() {
        Map<String, Object> config = new TreeMap<>();

        ValueParser.putAs(config::put, "test").accept("value");
        assertEquals("value", config.get("test"));
    }

    @Test
    public void testI8() {
        AtomicReference<Byte> integer = new AtomicReference<>();

        i8(integer::set).accept("040");
        assertEquals((byte) 32, integer.get());

        i8(integer::set).accept("40");
        assertEquals((byte) 40, integer.get());

        i8(integer::set).accept("0x40");
        assertEquals((byte) 64, integer.get());

        try {
            i8(integer::set).accept("b");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid integer value b", e.getMessage());
        }
    }

    @Test
    public void testI16() {
        AtomicReference<Short> integer = new AtomicReference<>();

        i16(integer::set).accept("040");
        assertEquals((short) 32, integer.get());

        i16(integer::set).accept("40");
        assertEquals((short) 40, integer.get());

        i16(integer::set).accept("0x40");
        assertEquals((short) 64, integer.get());

        try {
            i16(integer::set).accept("b");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid integer value b", e.getMessage());
        }
    }

    @Test
    public void testI32() {
        AtomicInteger integer = new AtomicInteger();

        i32(integer::set).accept("040");
        assertEquals(32, integer.get());

        i32(integer::set).accept("40");
        assertEquals(40, integer.get());

        i32(integer::set).accept("0x40");
        assertEquals(64, integer.get());

        try {
            i32(integer::set).accept("b");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid integer value b", e.getMessage());
        }
    }

    @Test
    public void testUI32() {
        AtomicInteger integer = new AtomicInteger();

        ui32(integer::set).accept("040");
        assertEquals(32, integer.get());

        ui32(integer::set).accept("40");
        assertEquals(40, integer.get());

        ui32(integer::set).accept("0x40");
        assertEquals(64, integer.get());

        try {
            ui32(integer::set).accept("-5");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid unsigned integer value -5", e.getMessage());
        }
    }

    @Test
    public void testI64() {
        AtomicLong integer = new AtomicLong();

        i64(integer::set).accept("40");
        assertEquals(40L, integer.get());

        i64(integer::set).accept("040");
        assertEquals(32L, integer.get());

        i64(integer::set).accept("0x40");
        assertEquals(64L, integer.get());

        try {
            i64(integer::set).accept("b");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid long value b", e.getMessage());
        }
    }

    @Test
    public void testUI64() {
        AtomicLong integer = new AtomicLong();

        ui64(integer::set).accept("40");
        assertEquals(40L, integer.get());

        ui64(integer::set).accept("040");
        assertEquals(32L, integer.get());

        ui64(integer::set).accept("0x40");
        assertEquals(64L, integer.get());

        try {
            ui64(integer::set).accept("-5");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid unsigned long value -5", e.getMessage());
        }
    }

    @Test
    public void testBigInt() {
        AtomicReference<BigInteger> integer = new AtomicReference<>();

        bigInt(integer::set).accept("40");
        assertEquals(new BigInteger(new byte[]{40}), integer.get());

        bigInt(integer::set).accept("0x0040");
        assertEquals(new BigInteger(new byte[]{64}), integer.get());

        bigInt(integer::set).accept("0x00");
        assertEquals(new BigInteger(new byte[]{0}), integer.get());

        try {
            bigInt(integer::set).accept("b");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid big integer value b", e.getMessage());
        }
    }

    @Test
    public void testFLT() {
        AtomicReference<Float> integer = new AtomicReference<>();

        flt(integer::set).accept("4.7");
        assertEquals(4.7, integer.get(), 0.001);

        try {
            flt(integer::set).accept("b");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid float value b", e.getMessage());
        }
    }

    @Test
    public void testDBL() {
        AtomicReference<Double> integer = new AtomicReference<>();

        dbl(integer::set).accept("4.7");
        assertEquals(4.7, integer.get(), 0.001);

        try {
            dbl(integer::set).accept("b");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid double value b", e.getMessage());
        }
    }

    @Test
    public void testBigDecimal() {
        AtomicReference<BigDecimal> reference = new AtomicReference<>();

        bigDecimal(reference::set).accept("4.7");
        assertEquals(new BigDecimal("4.7"), reference.get());

        try {
            bigDecimal(reference::set).accept("b");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid big decimal value b", e.getMessage());
        }
    }

    private enum E {
        A,
        B,
    }

    @Test
    public void testOneOf() {
        AtomicReference<E> value = new AtomicReference<>(E.A);

        oneOf(E.class, value::set).accept("B");

        assertEquals(E.B, value.get());

        try {
            oneOf(E.class).andApply(value::set).accept("not");
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertEquals("Invalid E value not", e.getMessage());
        }
    }

    @Test
    public void testFile() throws IOException {
        Path tempFile = tmp.resolve("exists");
        Files.writeString(tempFile, "foo", CREATE);
        AtomicReference<Path> f = new AtomicReference<>();

        file(f::set).accept(tempFile.toString());

        assertEquals(tempFile, f.get());

        try {
            file(f::set).accept(tmp.resolve("exists.not").toString());
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertThat(e.getMessage(), startsWith("No such file "));
        }
    }

    @Test
    public void testDir() throws IOException {
        Path tempDir = tmp.resolve("exists");
        Files.createDirectories(tempDir);
        AtomicReference<Path> f = new AtomicReference<>();

        dir(f::set).accept(tempDir.toAbsolutePath().toString());

        assertEquals(tempDir, f.get());

        try {
            Path not = tmp.resolve("exists.not");

            dir(f::set).accept(not.toString());
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertThat(e.getMessage(), startsWith("No such directory "));
        }
    }

    @Test
    public void testOutputFile() throws IOException {
        Path tempFile = tmp.resolve("exists");
        Files.writeString(tempFile, "foo");
        AtomicReference<Path> f = new AtomicReference<>();

        outputFile(f::set).accept(tempFile.toString());
        assertEquals(tempFile, f.get());

        Path not = tmp.resolve("exists.not");
        f.set(null);

        outputFile(f::set).accept(not.toString());
        assertEquals(not, f.get());

        try {
            Path tempDir = tmp.resolve("exists.dir");
            Files.createDirectories(tempDir);

            outputFile(f::set).accept(tempDir.toString());
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertThat(e.getMessage(), containsString(" exists and is not a file"));
        }
    }

    @Test
    public void testOutputDir() throws IOException {
        Path tempDir = tmp.resolve("dir");
        Files.createDirectories(tempDir);
        AtomicReference<Path> f = new AtomicReference<>();

        outputDir(f::set).accept(tempDir.toString());
        assertEquals(tempDir, f.get());

        Files.delete(tempDir);
        f.set(null);

        outputDir(f::set).accept(tempDir.toString());
        assertEquals(tempDir, f.get());

        try {
            Path tempFile = tmp.resolve("exists");
            Files.writeString(tempFile, "foo", CREATE);
            outputDir(f::set).accept(tempFile.toString());
            fail("No exception on invalid value.");
        } catch (ArgException e) {
            assertThat(e.getMessage(), containsString(" exists and is not a directory"));
        }
    }

    @Test
    public void testPath() {
        AtomicReference<Path> p = new AtomicReference<>();

        path(p::set).accept(tmp.toString());
        assertEquals(tmp, p.get());
    }

    @Test
    public void testDuration() {
        var duration = new AtomicReference<Duration>();
        var parser = ValueParser.duration(duration::set);
        parser.accept("42w");
        assertThat(duration.get(), is(Duration.ofDays(42 * 7)));
        parser.accept("42d");
        assertThat(duration.get(), is(Duration.ofDays(42)));
        parser.accept("42h");
        assertThat(duration.get(), is(Duration.ofHours(42)));
        parser.accept("42m");
        assertThat(duration.get(), is(Duration.ofMinutes(42)));
        parser.accept("42s");
        assertThat(duration.get(), is(Duration.ofSeconds(42)));
        parser.accept("-42s");
        assertThat(duration.get(), is(Duration.ofSeconds(-42)));
        parser.accept("42.42s");
        assertThat(duration.get(), is(Duration.ofSeconds(42).plusMillis(420)));
        parser.accept("42ms");
        assertThat(duration.get(), is(Duration.ofMillis(42)));
        parser.accept("42ns");
        assertThat(duration.get(), is(Duration.ofNanos(42)));

        try {
            parser.accept("42");
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("Expected duration string, but found: '42'"));
        }
        try {
            parser.accept("12345678901234567890s");
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("Invalid duration: For input string: \"12345678901234567890\""));
        }
    }
}
