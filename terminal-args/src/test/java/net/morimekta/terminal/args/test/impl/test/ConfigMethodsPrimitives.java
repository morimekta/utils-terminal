package net.morimekta.terminal.args.test.impl.test;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.time.Duration;

@SuppressWarnings("unused")
public class ConfigMethodsPrimitives {
    private boolean    b;
    private Boolean    bool;
    private byte       bt;
    private Byte       i8;
    private short      s;
    private Short      i16;
    private int        i;
    private Integer    i32;
    private long       l;
    private Long       i64;
    private BigInteger bigInt;
    private float      f;
    private Float      flt;
    private double     d;
    private Double     dbl;
    private BigDecimal bigDecimal;
    private MyEnum     myEnum;
    private Duration   dur;
    // public field matching method does not fail. Methods are still used.
    public  String     name;
    private Path       path;
    private File       file;

    public boolean isB() {
        return b;
    }

    public void setB(boolean b) {
        this.b = b;
    }

    public Boolean getBool() {
        return bool;
    }

    public void setBool(Boolean bool) {
        this.bool = bool;
    }

    public byte getBt() {
        return bt;
    }

    public void setBt(byte bt) {
        this.bt = bt;
    }

    public Byte getI8() {
        return i8;
    }

    public void setI8(Byte i8) {
        this.i8 = i8;
    }

    public short getS() {
        return s;
    }

    public void setS(short s) {
        this.s = s;
    }

    public Short getI16() {
        return i16;
    }

    public void setI16(Short i16) {
        this.i16 = i16;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public Integer getI32() {
        return i32;
    }

    public void setI32(Integer i32) {
        this.i32 = i32;
    }

    public long getL() {
        return l;
    }

    public void setL(long l) {
        this.l = l;
    }

    public Long getI64() {
        return i64;
    }

    public void setI64(Long i64) {
        this.i64 = i64;
    }

    public BigInteger getBigInt() {
        return bigInt;
    }

    public void setBigInt(BigInteger bigInt) {
        this.bigInt = bigInt;
    }

    public float getF() {
        return f;
    }

    public void setF(float f) {
        this.f = f;
    }

    public Float getFlt() {
        return flt;
    }

    public void setFlt(Float flt) {
        this.flt = flt;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public Double getDbl() {
        return dbl;
    }

    public void setDbl(Double dbl) {
        this.dbl = dbl;
    }

    public BigDecimal getBigDecimal() {
        return bigDecimal;
    }

    public void setBigDecimal(BigDecimal bigDecimal) {
        this.bigDecimal = bigDecimal;
    }

    public MyEnum getMyEnum() {
        return myEnum;
    }

    public void setMyEnum(MyEnum myEnum) {
        this.myEnum = myEnum;
    }

    public Duration getDur() {
        return dur;
    }

    public void setDur(Duration dur) {
        this.dur = dur;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
