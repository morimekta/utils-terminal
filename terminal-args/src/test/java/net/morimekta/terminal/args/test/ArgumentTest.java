/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.test;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.Argument;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests for Argument.
 */
public class ArgumentTest {
    private final AtomicReference<String> ref = new AtomicReference<>();

    @Test
    public void testConstructor_Required() {
        Argument arg = Argument.argument("name", "Set name", ref::set).required().build();

        assertEquals("name", arg.getName());
        assertEquals("name", arg.getPrefix());
        assertEquals("name", arg.getSingleLineUsage());
        assertEquals("Set name", arg.getUsage());
        assertThat(arg.getDefaultValue(), is(nullValue()));

        try {
            arg.validate();
            fail("no exception");
        } catch (ArgException e) {
            assertEquals("Argument name is required", e.getMessage());
        }

        assertThat(arg.apply(List.of("foo", "bar")), is(1));
        assertThat(ref.get(), is("foo"));

        arg.validate();
    }

    @Test
    public void testArgumentConstructor_Defaults() {
        var arg = Argument.argument("name", "Set name", ref::set).defaultValue("foo").build();
        assertThat(arg.getName(), is("name"));
        assertThat(arg.getPrefix(), is("name"));
        assertThat(arg.getSingleLineUsage(), is("[name]"));
        assertThat(arg.getUsage(), is("Set name"));
        assertThat(arg.getDefaultValue(), is("foo"));

        arg.validate();
    }

    @Test
    public void testArgumentConstructor() {
        var arg = Argument.argument("name", "Set name", ref::set).build();
        assertThat(arg.getPrefix(), is("name"));
        assertEquals("[name]", arg.getSingleLineUsage());
    }

    @Test
    public void testArgumentConstructor_Repeated() {
        var arg = Argument.argument("name", "Set name", ref::set).repeated().build();
        assertThat(arg.getPrefix(), is("name"));
        assertThat(arg.getSingleLineUsage(), is("[name ...]"));

        assertThat(arg.apply(List.of("foo", "bar")), is(1));
        assertThat(arg.apply(List.of("bar")), is(1));
        assertThat(ref.get(), is("bar"));
    }

    @Test
    public void testArgumentConstructor_RepeatedAndRequired() {
        var arg = Argument.argument("name", "Set name", ref::set).repeated().required().build();
        assertThat(arg.getPrefix(), is("name"));
        assertThat(arg.getSingleLineUsage(), is("(name ...)"));
    }

    @Test
    public void testArgumentConstructor_Hidden() {
        var arg = Argument.argument("name", "Set name", ref::set).hidden().build();
        assertThat(arg.getPrefix(), is("name"));
        assertThat(arg.getSingleLineUsage(), is("[name]"));
        assertThat(arg.isHidden(), is(true));
    }

    @Test
    public void testArgumentConstructor_WhenPattern() {
        var arg = Argument.argument("name", "Set name", ref::set)
                          .when(Pattern.compile("foo|bar"))
                          .build();

        assertThat(arg.apply(List.of("bar")), is(1));
        assertThat(arg.apply(List.of("baz")), is(0));
        assertThat(ref.get(), is("bar"));
    }

    @Test
    public void testArgumentConstructor_WhenNotPattern() {
        var arg = Argument.argument("name", "Set name", ref::set)
                          .whenNot(Pattern.compile("foo|bar"))
                          .build();

        assertThat(arg.apply(List.of("bar")), is(0));
        assertThat(arg.apply(List.of("baz")), is(1));
        assertThat(ref.get(), is("baz"));
    }

    @Test
    public void testArgumentConstructor_WhenTooMany() {
        try {
            Argument.argument("name", "Set name", ref::set)
                    .when(Pattern.compile("foo"))
                    .whenNot(Pattern.compile("foo|bar"))
                    .build();
            fail("No exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Predicate already set for argument 'name'"));
        }

        try {
            Argument.argument("name", "Set name", ref::set)
                    .whenNot(Pattern.compile("foo|bar"))
                    .when(Pattern.compile("foo"))
                    .build();
            fail("No exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Predicate already set for argument 'name'"));
        }

        try {
            Argument.argument("name", "Set name", ref::set)
                    .when(Pattern.compile("foo"))
                    .when(s -> s.equalsIgnoreCase("foo"))
                    .build();
            fail("No exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Predicate already set for argument 'name'"));
        }
    }

    @Test
    public void testArgumentConstructor_EmptyName() {
        try {
            Argument.argument("", "Set name", ref::set);
            fail("no exception: ");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty argument name."));
        }
    }

    @Test
    public void testArgumentConstructor_InvalidName() {
        try {
            Argument.argument("--foo", "Set name", ref::set);
            fail("no exception: ");
        } catch (IllegalArgumentException e) {
            assertThat(
                    e.getMessage(),
                    is("Invalid argument name \"--foo\", must be an identifier string, e.g.: \"name\"."));
        }
    }

    @Test
    public void testArgumentConstructor_EmptyUsage() {
        try {
            Argument.argument("foo", "", ref::set);
            fail("no exception: ");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty usage string."));
        }
    }
}
