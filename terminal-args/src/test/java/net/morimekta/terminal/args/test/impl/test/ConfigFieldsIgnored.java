package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.annotations.ArgIgnore;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused,rawtypes")
public class ConfigFieldsIgnored {
    public boolean bool;

    public       Map                noGenericOnMap;
    public       List               noGenericOnList;
    public final Map                noGenericOnFinalMap      = new HashMap();
    public final Map<String, ?>     noGenericOnFinalMapHalf  = new HashMap<>();
    public final Map<?, ?>          noGenericOnFinalMapCatch = new HashMap<>();
    public final List               noGenericOnFinalList     = new LinkedList();
    public final List<?>            noGenericOnFinalCatch    = new LinkedList<>();
    public       ConfigFieldsSimple chainedNoValue;
    @ArgIgnore
    public       String             withIgnoredAnnotation;
    protected    String             protectedValue;
    private      String             privateValue;
    public final String             finalValue               = "foo";
    public       String             __;  // Invalid as name.
}
