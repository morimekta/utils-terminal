package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.annotations.ArgIsRequired;

public class ConfigFieldsSimple {
    @ArgIsRequired
    public String name;
    public int    value;
}
