package net.morimekta.terminal.args.test.impl;

import net.morimekta.collect.UnmodifiableList;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.terminal.args.ArgException;
import org.junit.jupiter.api.Test;

import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.Supplier;

import static net.morimekta.terminal.args.impl.GeneratorUtil.createCollectionSupplier;
import static net.morimekta.terminal.args.impl.GeneratorUtil.createMapSupplier;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class GeneratorUtilTest {
    @Test
    public void testCreateMapSupplier() {
        Supplier<Map<Object, Object>> s;

        s = createMapSupplier(HashMap.class);
        assertThat(s.get(), is(instanceOf(HashMap.class)));

        s = createMapSupplier(FailingMap.class);
        try {
            fail("No exception: " + s.get());
        } catch (ArgException e) {
            assertThat(e.getMessage(),
                       is("Unable to instantiate net.morimekta.terminal.args.test.impl.GeneratorUtilTest$FailingMap"));
        }

        try {
            createMapSupplier(UnmodifiableMap.class);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(),
                       is("Unable to find default constructor for net.morimekta.collect.UnmodifiableMap"));
        }
        try {
            createMapSupplier(AbstractMap.class);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unable to determine constructor for java.util.AbstractMap"));
        }
        try {
            createMapSupplier(PrivateMap.class);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(
                    e.getMessage(),
                    is("Unable to access default constructor for net.morimekta.terminal.args.test.impl.GeneratorUtilTest$PrivateMap"));
        }
    }

    @Test
    public void testCreateCollectionSupplier() {
        Supplier<Collection<Object>> s;

        s = createCollectionSupplier(LinkedList.class);
        assertThat(s.get(), is(instanceOf(LinkedList.class)));

        s = createCollectionSupplier(FailingList.class);
        try {
            fail("No exception: " + s.get());
        } catch (ArgException e) {
            assertThat(e.getMessage(),
                       is("Unable to instantiate net.morimekta.terminal.args.test.impl.GeneratorUtilTest$FailingList"));
        }

        try {
            createCollectionSupplier(UnmodifiableList.class);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(),
                       is("Unable to find default constructor for net.morimekta.collect.UnmodifiableList"));
        }
        try {
            createCollectionSupplier(AbstractList.class);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unable to determine constructor for java.util.AbstractList"));
        }
        try {
            createCollectionSupplier(PrivateList.class);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(
                    e.getMessage(),
                    is("Unable to access default constructor for net.morimekta.terminal.args.test.impl.GeneratorUtilTest$PrivateList"));
        }
    }

    public static class FailingMap extends HashMap<String, String> {
        private static final long serialVersionUID = 1L;

        public FailingMap() {
            throw new IllegalArgumentException("foo");
        }
    }

    private static class PrivateMap extends HashMap<String, String> {
        private static final long serialVersionUID = 1L;

        public PrivateMap() {}
    }

    public static class FailingList extends ArrayList<String> {
        private static final long serialVersionUID = 1L;

        public FailingList() {
            throw new IllegalArgumentException("foo");
        }
    }

    private static class PrivateList extends ArrayList<String> {
        private static final long serialVersionUID = 1L;

        public PrivateList() {}
    }
}
