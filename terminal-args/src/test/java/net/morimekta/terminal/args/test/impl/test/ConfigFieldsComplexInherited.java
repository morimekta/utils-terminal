package net.morimekta.terminal.args.test.impl.test;

public class ConfigFieldsComplexInherited extends ConfigFieldsComplex {
    public final ConfigFieldsSimple simpleOther = new ConfigFieldsSimple();
    public       ConfigFieldsSimple otherSimple = new ConfigFieldsSimple();
}
