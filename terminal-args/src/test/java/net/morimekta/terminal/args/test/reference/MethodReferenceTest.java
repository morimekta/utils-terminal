package net.morimekta.terminal.args.test.reference;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.annotations.ArgIsRequired;
import net.morimekta.terminal.args.annotations.ArgKeyParser;
import net.morimekta.terminal.args.annotations.ArgValueParser;
import net.morimekta.terminal.args.reference.AdderMethodReference;
import net.morimekta.terminal.args.reference.MethodReference;
import net.morimekta.terminal.args.reference.PutterMethodReference;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsAdders;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsIgnored;
import net.morimekta.terminal.args.test.impl.test.ConfigMethodsPutters;
import net.morimekta.terminal.args.test.impl.test.DPI;
import net.morimekta.terminal.args.test.impl.test.DummyAnnotation;
import net.morimekta.terminal.args.test.impl.test.Resolution;
import org.junit.jupiter.api.Test;

import java.lang.reflect.ParameterizedType;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class MethodReferenceTest {
    public ConfigMethodsAdders  adders  = new ConfigMethodsAdders();
    public ConfigMethodsPutters putters = new ConfigMethodsPutters();

    @Test
    public void testAdderMethodReference() throws NoSuchMethodException {
        var adder = adders.getClass().getMethod("addDurations", Duration.class);
        var getter = adders.getClass().getMethod("getDurations");
        var reference = new AdderMethodReference(adders, getter, adder, adder);

        assertThat(reference.getName(), is("addDurations"));
        assertThat(reference.getUsage(), is("Apply addDurations()."));
        assertThat(reference.get(), is(sameInstance(adders.getDurations())));
        assertThat(reference.isAnnotationPresent(DummyAnnotation.class), is(true));
        assertThat(reference.isAnnotationPresent(ArgValueParser.class), is(false));
        assertThat(reference.getAnnotation(DummyAnnotation.class), is(notNullValue()));
        assertThat(reference.getAnnotation(ArgValueParser.class), is(nullValue()));
        assertThat(reference.getItemType(), is(Duration.class));
        assertThat(reference.getType(), is(List.class));
        var generic = reference.getGenericType();
        assertThat(generic, is(instanceOf(ParameterizedType.class)));
        var parameterizedType = (ParameterizedType) generic;
        assertThat(parameterizedType.getRawType(), is(List.class));
        assertThat(parameterizedType.getActualTypeArguments().length, is(1));
        assertThat(parameterizedType.getActualTypeArguments()[0], is(Duration.class));

        reference = new AdderMethodReference(putters, getter, adder, adder);
        try {
            reference.get();
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("object is not an instance of declaring class"));
        }
    }

    @Test
    public void testAdderMethodReference_NoGetter() throws NoSuchMethodException {
        var adder = adders.getClass().getMethod("addDurations", Duration.class);
        var reference = new AdderMethodReference(adders, null, adder, adder);

        assertThat(reference.getName(), is("addDurations"));
        assertThat(reference.getUsage(), is("Apply addDurations()."));
        assertThat(reference.get(), is(nullValue()));
        assertThat(reference.isAnnotationPresent(DummyAnnotation.class), is(true));
        assertThat(reference.isAnnotationPresent(ArgValueParser.class), is(false));
        assertThat(reference.getAnnotation(DummyAnnotation.class), is(notNullValue()));
        assertThat(reference.getAnnotation(ArgValueParser.class), is(nullValue()));
        assertThat(reference.getItemType(), is(Duration.class));
        assertThat(reference.getType(), is(Collection.class));
        var generic = reference.getGenericType();
        assertThat(generic, is(instanceOf(ParameterizedType.class)));
        var parameterizedType = (ParameterizedType) generic;
        assertThat(parameterizedType.getOwnerType(), is(nullValue()));
        assertThat(parameterizedType.getRawType(), is(Collection.class));
        assertThat(parameterizedType.getActualTypeArguments().length, is(1));
        assertThat(parameterizedType.getActualTypeArguments()[0], is(Duration.class));
    }

    @Test
    public void testMethodReference() throws NoSuchMethodException {
        var getter = adders.getClass().getMethod("getDurations");
        var reference = new MethodReference(adders, getter, getter);

        assertThat(reference.isAnnotationPresent(DummyAnnotation.class), is(true));
        assertThat(reference.getAnnotation(DummyAnnotation.class), is(notNullValue()));
        assertThat(reference.getType(), is(List.class));
        var generic = reference.getGenericType();
        assertThat(generic, is(instanceOf(ParameterizedType.class)));
        var parameterizedType = (ParameterizedType) generic;
        assertThat(parameterizedType.getOwnerType(), is(nullValue()));
        assertThat(parameterizedType.getRawType(), is(List.class));
        assertThat(parameterizedType.getActualTypeArguments().length, is(1));
        assertThat(parameterizedType.getActualTypeArguments()[0], is(Duration.class));
    }

    @Test
    public void testPutterMethodReference() throws NoSuchMethodException {
        var putter = putters.getClass().getMethod("putInParameterizedPutter", Resolution.class, DPI.class);
        var getter = putters.getClass().getMethod("getParameterizedPutter");
        var reference = new PutterMethodReference(putters, getter, putter, putter);

        assertThat(putters.getParameterizedPutter(), is(mapOf()));

        reference.put(new Resolution(1024, 768), new DPI(100.7));

        assertThat(putters.getParameterizedPutter(), is(mapOf(new Resolution(1024, 768), new DPI(100.7))));

        assertThat(reference.getName(), is("putInParameterizedPutter"));
        assertThat(reference.getUsage(), is("Apply putInParameterizedPutter()."));
        assertThat(reference.get(), is(sameInstance(putters.getParameterizedPutter())));
        assertThat(reference.isAnnotationPresent(ArgKeyParser.class), is(true));
        assertThat(reference.isAnnotationPresent(ArgIsRequired.class), is(false));
        assertThat(reference.getAnnotation(ArgKeyParser.class), is(notNullValue()));
        assertThat(reference.getAnnotation(ArgIsRequired.class), is(nullValue()));
        assertThat(reference.getType(), is(Map.class));
        var generic = reference.getGenericType();
        assertThat(generic, is(instanceOf(ParameterizedType.class)));
        var parameterizedType = (ParameterizedType) generic;
        assertThat(parameterizedType.getRawType(), is(Map.class));
        assertThat(parameterizedType.getActualTypeArguments().length, is(2));
        assertThat(parameterizedType.getActualTypeArguments()[0], is(Resolution.class));
        assertThat(parameterizedType.getActualTypeArguments()[1], is(DPI.class));
    }

    @Test
    public void testPutterMethodReference_Errors() throws NoSuchMethodException {
        var putter = putters.getClass().getMethod("putInParameterizedPutter", Resolution.class, DPI.class);
        var getter = putters.getClass().getMethod("getParameterizedPutter");

        var reference = new PutterMethodReference(adders, getter, putter, putter);
        try {
            reference.put(null, null);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("object is not an instance of declaring class"));
        }

        reference = new PutterMethodReference(putters, getter, putter, putter);
        try {
            putters.parameterizedPutter = null;
            reference.put(null, null);
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getCause(), is(instanceOf(NullPointerException.class)));
        }

        var ignored = new ConfigMethodsIgnored();
        var privateGetter = ignored.getClass().getDeclaredMethod("getValuePrivate");
        var privateSetter = ignored.getClass().getDeclaredMethod("putValuePrivate", String.class, String.class);
        reference = new PutterMethodReference(ignored, privateGetter, privateSetter, privateSetter);
        try {
            reference.put(null, null);
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), allOf(
                    containsString(MethodReference.class.getName()),
                    containsString("cannot access"),
                    containsString(ConfigMethodsIgnored.class.getName())));
        }
        try {
            reference.get();
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), allOf(
                    containsString(MethodReference.class.getName()),
                    containsString("cannot access"),
                    containsString(ConfigMethodsIgnored.class.getName())));
        }
    }

    @Test
    public void testPutterMethodReference_NoGetter() throws NoSuchMethodException {
        var putter = putters.getClass().getMethod("putInParameterizedPutter", Resolution.class, DPI.class);
        var reference = new PutterMethodReference(putters, null, putter, putter);

        assertThat(putters.getParameterizedPutter(), is(mapOf()));

        reference.put(new Resolution(1024, 768), new DPI(100.7));

        assertThat(putters.getParameterizedPutter(), is(mapOf(new Resolution(1024, 768), new DPI(100.7))));

        assertThat(reference.getName(), is("putInParameterizedPutter"));
        assertThat(reference.getUsage(), is("Apply putInParameterizedPutter()."));
        assertThat(reference.get(), is(nullValue()));
        assertThat(reference.isAnnotationPresent(ArgKeyParser.class), is(true));
        assertThat(reference.isAnnotationPresent(ArgIsRequired.class), is(false));
        assertThat(reference.getAnnotation(ArgKeyParser.class), is(notNullValue()));
        assertThat(reference.getAnnotation(ArgIsRequired.class), is(nullValue()));
        assertThat(reference.getType(), is(Map.class));
        var generic = reference.getGenericType();
        assertThat(generic, is(instanceOf(ParameterizedType.class)));
        var parameterizedType = (ParameterizedType) generic;
        assertThat(parameterizedType.getOwnerType(), is(nullValue()));
        assertThat(parameterizedType.getRawType(), is(Map.class));
        assertThat(parameterizedType.getActualTypeArguments().length, is(2));
        assertThat(parameterizedType.getActualTypeArguments()[0], is(Resolution.class));
        assertThat(parameterizedType.getActualTypeArguments()[1], is(DPI.class));
    }
}
