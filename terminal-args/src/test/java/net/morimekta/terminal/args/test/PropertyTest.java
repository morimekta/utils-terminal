/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.args.test;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.Option;
import net.morimekta.terminal.args.Property;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static net.morimekta.terminal.args.Option.optionLong;
import static net.morimekta.terminal.args.Property.property;
import static net.morimekta.terminal.args.Property.propertyLong;
import static net.morimekta.terminal.args.Property.propertyShort;
import static net.morimekta.terminal.args.ValueParser.i32;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class PropertyTest {
    @Test
    public void testConstructor_Short() {
        Map<String, String> map = new TreeMap<>();
        Property p = propertyShort('D', "Help", map::put).build();

        assertEquals("-Dkey=val", p.getPrefix());
        assertEquals("Help", p.getUsage());
        assertEquals("[-Dkey=val ...]", p.getSingleLineUsage());
        assertEquals("key", p.getMetaKey());

        p.validate();

        assertThat(propertyShort('A', "Help", map::put).build().isHidden(), is(false));
        assertThat(propertyShort('A', "Help", map::put).hidden().build().isHidden(), is(true));
    }

    @Test
    public void testConstructor_Long() {
        Map<String, String> map = new TreeMap<>();
        Property p = propertyLong("--prop", "Help", map::put).build();

        assertEquals("--prop key=val", p.getPrefix());
        assertEquals("[--prop key=val ...]", p.getSingleLineUsage());
    }

    @Test
    public void testConstructor() {
        Map<String, String> map = new TreeMap<>();
        Property p = property("--prop", 'P', "Help", map::put).build();

        assertEquals("--prop (-P) key=val", p.getPrefix());
        assertEquals("[-Pkey=val ...]", p.getSingleLineUsage());
    }

    @Test
    public void testApplyShort() {
        Map<String, String> map = new TreeMap<>();
        Property p = propertyShort('D', "Help", map::put).build();

        p.applyShort("Dkey=val", List.of("-Dkey=val"));

        assertEquals("val", map.get("key"));

        p.applyShort("D", List.of("-D", "other=other"));
        assertThat(map.get("other"), is("other"));

        try {
            p.applyShort("Dkey", List.of("-Dkey", "val"));
            fail("No exception on invalid applyShort");
        } catch (ArgException e) {
            assertEquals("No key value sep for properties on -D: \"-Dkey\"", e.getMessage());
        }

        try {
            p.applyShort("D=val", List.of("-D=val"));
            fail("No exception on invalid applyShort");
        } catch (ArgException e) {
            assertEquals("Empty property key on -D: \"-D=val\"", e.getMessage());
        }
    }

    @Test
    public void testApply() {
        Map<String, String> map = new TreeMap<>();
        Property p = propertyLong("--def", "Help", map::put).build();
        assertEquals(2, p.apply(List.of("--def", "key=val")));
        assertEquals("val", map.get("key"));

        try {
            p.apply(List.of("--def"));
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("No value for --def"));
        }

        try {
            p.apply(List.of("--def", "key"));
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("No key value sep for properties on --def: \"key\""));
        }

        try {
            p.apply(List.of("--def", "=value"));
            fail("No exception");
        } catch (ArgException e) {
            assertThat(e.getMessage(), is("Empty property key on --def: \"=value\""));
        }
    }

    @Test
    public void testApplyParser() {
        Map<String, Object> config = new TreeMap<>();
        Property straight = property("--def", 'D', "Help", config::put).build();
        Property typed = propertyShort('I', "Int", i32().andPut(config::put)).build();
        Option typedInto = optionLong("--i32", "I2", i32().andPutAs(config::put, "i2")).metaVar("num").build();

        assertEquals(2, straight.apply(List.of("--def", "key=val")));
        assertEquals("val", config.get("key"));

        typed.applyShort("Ik=32", List.of("-I32"));
        assertEquals(32, config.get("k"));

        typedInto.apply(List.of("--i32", "1234"));
        assertEquals(1234, config.get("i2"));
    }
}
