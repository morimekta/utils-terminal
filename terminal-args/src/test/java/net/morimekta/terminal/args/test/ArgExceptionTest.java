package net.morimekta.terminal.args.test;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ArgParser;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class ArgExceptionTest {
    @Test
    @SuppressWarnings("removal")
    public void testDeprecatedConstructors() {
        Throwable cause = new Throwable();
        Throwable fake = new Throwable();
        ArgException exception = new ArgException(cause, "Foo: %s", "nope", fake);
        assertThat(exception.getMessage(), is("Foo: nope"));
        assertThat(exception.getCause(), is(cause));

        ArgParser parser = ArgParser.argParser("foo", "bar", "baz").build();
        ArgException e2 = new ArgException(parser, cause, "Foo: %s", "nope", fake);
        assertThat(e2.getMessage(), is("Foo: nope"));
        assertThat(e2.getCause(), is(cause));
        assertThat(e2.getParser(), is(sameInstance(parser)));
    }
}
