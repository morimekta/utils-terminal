package net.morimekta.terminal.args.test.reference;

import net.morimekta.terminal.args.reference.ChainedAdderReference;
import net.morimekta.terminal.args.reference.ChainedPutterReference;
import net.morimekta.terminal.args.reference.SettableReference;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChainedReferenceTest {

    @Test
    @SuppressWarnings("unchecked,rawtypes")
    public void testChainedAdderReference() {
        var list = new ArrayList<>();
        var chained = mock(SettableReference.class);
        when(chained.get()).thenReturn(list);
        when(chained.getType()).thenReturn((Class) List.class);
        when(chained.getGenericType()).thenReturn(Collection.class);

        var reference = new ChainedAdderReference(chained, Duration.class, ArrayList::new);

        assertThat(reference.get(), is(sameInstance(list)));
        assertThat(reference.getType(), is(List.class));
        assertThat(reference.getGenericType(), is(Collection.class));
    }

    @Test
    @SuppressWarnings("unchecked,rawtypes")
    public void testChainedPutterReference() {
        var map = new LinkedHashMap<>();
        var chained = mock(SettableReference.class);
        when(chained.get()).thenReturn(map);
        when(chained.getType()).thenReturn((Class) HashMap.class);
        when(chained.getGenericType()).thenReturn(TreeMap.class);

        var reference = new ChainedPutterReference(chained, String.class, Duration.class, HashMap::new);

        assertThat(reference.get(), is(sameInstance(map)));
        assertThat(reference.getType(), is(HashMap.class));
        assertThat(reference.getGenericType(), is(TreeMap.class));
    }
}
