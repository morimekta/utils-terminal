package net.morimekta.terminal.args.test.impl.test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface DummyAnnotation {
}
