package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.ArgException;
import net.morimekta.terminal.args.ValueParser;
import net.morimekta.terminal.args.annotations.ArgValueParser;

import java.util.Objects;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

@ArgValueParser(Resolution.Parser.class)
public class Resolution {
    public final int x;
    public final int y;

    public Resolution(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return x + "x" + y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Resolution)) {
            return false;
        }
        Resolution that = (Resolution) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public static class Parser implements ValueParser<Resolution> {
        @Override
        public Resolution parse(String value) {
            var matcher = RES.matcher(value);
            if (!matcher.matches()) {
                throw new ArgException("Invalid resolution: %s", value);
            }
            return new Resolution(parseInt(matcher.group("x")), parseInt(matcher.group("y")));
        }

        private static final Pattern RES = Pattern.compile("^(?<x>\\d+)x(?<y>\\d+)$");
    }
}
