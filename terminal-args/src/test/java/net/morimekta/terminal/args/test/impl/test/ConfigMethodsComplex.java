package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.annotations.ArgIsHidden;
import net.morimekta.terminal.args.annotations.ArgIsRepeated;
import net.morimekta.terminal.args.annotations.ArgIsRequired;
import net.morimekta.terminal.args.annotations.ArgOptions;

import java.util.Set;
import java.util.TreeSet;

@SuppressWarnings("unused")
public class ConfigMethodsComplex {
    private int booleans = 0;

    public int getBooleans() {
        return booleans;
    }

    @ArgIsRepeated
    @ArgOptions(shortChar = 'b')
    public void setBooleans(boolean increment) {
        this.booleans += increment ? 1 : -1;
    }

    private final Set<String> tags = new TreeSet<>();

    public Set<String> getTags() {
        return tags;
    }

    @ArgIsRepeated
    @ArgIsRequired
    public void setTags(String tag) {
        this.tags.add(tag);
    }

    private boolean plainText;

    public boolean plainText() {
        return plainText;
    }

    @ArgIsHidden
    @ArgOptions(usage = "Use plain text.")
    public void plainText(boolean pt) {
        plainText = pt;
    }

    private final ConfigMethodsSimple chained = new ConfigMethodsSimple();

    @ArgOptions(name = "chained")
    public ConfigMethodsSimple chained() {
        return chained;
    }
}
