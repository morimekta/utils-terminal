package net.morimekta.terminal.args.test.impl.test;

import java.util.Objects;

public class DPI {
    public final double dpi;

    public DPI(double dpi) {
        this.dpi = dpi;
    }

    @Override
    public String toString() {
        return "DPI{" + dpi + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DPI)) {
            return false;
        }
        DPI dpi1 = (DPI) o;
        return Double.compare(dpi1.dpi, dpi) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dpi);
    }
}
