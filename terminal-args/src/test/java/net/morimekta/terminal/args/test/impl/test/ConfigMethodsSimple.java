package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.annotations.ArgIsRequired;

public class ConfigMethodsSimple {
    private String name;
    private int    value;

    @ArgIsRequired
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
