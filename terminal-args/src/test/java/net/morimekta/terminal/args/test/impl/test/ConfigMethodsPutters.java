package net.morimekta.terminal.args.test.impl.test;

import net.morimekta.terminal.args.annotations.ArgIsHidden;
import net.morimekta.terminal.args.annotations.ArgKeyParser;
import net.morimekta.terminal.args.annotations.ArgOptions;
import net.morimekta.terminal.args.annotations.ArgValueParser;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class ConfigMethodsPutters {
    private final Map<String, String> optionMap = new HashMap<>();

    @ArgOptions(name = "option-map", shortChar = 'O', usage = "A simple map.")
    public Map<String, String> getOptionMap() {
        return optionMap;
    }

    private Map<DPI, Resolution> parameterizedMap = new HashMap<>();

    public Map<DPI, Resolution> getParameterizedMap() {
        return parameterizedMap;
    }

    @ArgKeyParser(DPIParser.class)
    @ArgValueParser(Resolution.Parser.class)
    public void setParameterizedMap(Map<DPI, Resolution> parameterizedMap) {
        this.parameterizedMap = parameterizedMap;
    }

    public Map<Resolution, DPI> parameterizedPutter = new HashMap<>();

    public Map<Resolution, DPI> getParameterizedPutter() {
        return parameterizedPutter;
    }

    @ArgKeyParser(Resolution.Parser.class)
    @ArgValueParser(DPIParser.class)
    public void putInParameterizedPutter(Resolution res, DPI dpi) {
        this.parameterizedPutter.put(res, dpi);
    }

    @ArgIsHidden
    private Map<String, String> hiddenMap;

    public Map<String, String> getHiddenMap() {
        return hiddenMap;
    }

    public void setHiddenMap(Map<String, String> hiddenMap) {
        this.hiddenMap = hiddenMap;
    }

    // yes do the weird 2 public fields here.
    private final Map<String, String> methodPutter  = new HashMap<>();
    public final  Map<String, String> method_putter = new HashMap<>();

    public Map<String, String> methodPutter() {
        return methodPutter;
    }

    @ArgOptions
    public void methodPutter(String key, String value) {
        methodPutter.put(key, value);
    }
}
