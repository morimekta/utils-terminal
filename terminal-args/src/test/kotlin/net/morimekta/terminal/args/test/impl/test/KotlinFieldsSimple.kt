package net.morimekta.terminal.args.test.impl.test

import net.morimekta.terminal.args.annotations.ArgIsRequired

class KotlinFieldsSimple {
    @ArgIsRequired
    var name: String? = null
    var value = 0
}