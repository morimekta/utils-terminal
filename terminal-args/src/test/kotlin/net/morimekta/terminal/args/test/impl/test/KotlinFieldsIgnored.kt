package net.morimekta.terminal.args.test.impl.test

import net.morimekta.terminal.args.annotations.ArgIgnore

@Suppress("unused")
class KotlinFieldsIgnored {
    // one visible to test for.
    var bool: Boolean? = null

    var noGenericOnMap: Map<*, *>? = null
    var noGenericOnMapHalf: Map<String, *>? = null
    var noGenericOnList: List<*>? = null

    @ArgIgnore
    var withIgnoredAnnotation: String? = null

    @set:ArgIgnore
    var withIgnoredAnnotationOnSetter: String? = null

    @get:ArgIgnore
    val withIgnoredAnnotationOnGetter = KotlinFieldsSimple()

    var chainedNoValue: KotlinFieldsSimple? = null

    val finalValue = "FOO";

    private var privateValue: Boolean = true
}