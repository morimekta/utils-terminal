package net.morimekta.terminal.args.test.impl.test

import java.io.File
import java.math.BigDecimal
import java.math.BigInteger
import java.nio.file.Path
import java.time.Duration

class KotlinFieldsPrimitives {
    var bool = false
    var i8 = 0.toByte()
    var i16 = 0.toShort()
    var i32 = 0
    var i64 = 0L
    var bigInt: BigInteger? = null
    var flt = 0.0f
    var dbl = 0.0
    var bigDecimal: BigDecimal? = null
    var myEnum: MyEnum? = null
    var dur: Duration? = null
    var name: String? = null
    var path: Path? = null
    var file: File? = null
}