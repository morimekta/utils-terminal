package net.morimekta.terminal.args.test.impl.test

import net.morimekta.terminal.args.annotations.ArgIsHidden
import net.morimekta.terminal.args.annotations.ArgIsRequired
import net.morimekta.terminal.args.annotations.ArgOptions
import java.time.Duration

class KotlinFieldsComplex {
    @ArgOptions(shortChar = 'M')
    var map: Map<String, Int>? = null

    @ArgOptions(usage = "Truly put into concurrent")
    @Suppress("unused")
    var hashMap: HashMap<String, String>? = null

    @ArgIsHidden
    @Suppress("unused")
    val hiddenMap: Map<String, String> = mutableMapOf()

    @ArgOptions(shortChar = 'D', usage = "Add duration to list")
    var durations: Collection<Duration>? = null

    @ArgIsRequired
    var longs: LinkedHashSet<Long>? = null

    @ArgIsHidden
    @Suppress("unused")
    val hiddenList: List<String> = mutableListOf()

    @ArgOptions(name = "fields")
    val simple = KotlinFieldsSimple()
    val other = KotlinFieldsSimple()
}