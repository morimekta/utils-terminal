package net.morimekta.terminal.args.test.impl

import net.morimekta.terminal.args.ArgHelp
import net.morimekta.terminal.args.ArgNameFormat
import net.morimekta.terminal.args.ArgParser
import net.morimekta.terminal.args.test.impl.test.KotlinFieldsComplex
import net.morimekta.terminal.args.test.impl.test.KotlinFieldsIgnored
import net.morimekta.terminal.args.test.impl.test.KotlinFieldsPrimitives
import net.morimekta.terminal.args.test.impl.test.MyEnum
import net.morimekta.testing.console.Console
import net.morimekta.testing.junit5.ConsoleExtension
import net.morimekta.testing.junit5.ConsoleSize
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.slf4j.bridge.SLF4JBridgeHandler
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintStream
import java.math.BigDecimal
import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.nio.file.Path
import java.time.Duration
import java.util.logging.Level
import java.util.logging.Logger.getLogger

@ExtendWith(ConsoleExtension::class)
@ConsoleSize(cols = 120, rows = 44)
class GeneratorKtTest {
    companion object {
        @BeforeAll
        @JvmStatic
        fun setUpClass() {
            SLF4JBridgeHandler.removeHandlersForRootLogger()
            SLF4JBridgeHandler.install()
            getLogger("").level = Level.FINEST;
        }

        private fun getHelp(help: ArgHelp): String {
            val out = ByteArrayOutputStream()
            PrintStream(out, true, StandardCharsets.UTF_8).use { pw -> help.printHelp(pw) }
            return out.toString(StandardCharsets.UTF_8)
        }
    }

    @Test
    fun testGenerateArgs_Primitives(console: Console) {
        val config = KotlinFieldsPrimitives()
        val args = ArgParser.argParser("foo", "bar", "baz")
            .generateArgsNameFormat(ArgNameFormat.SNAKE)
            .generateArgs(config)
            .build()
        args.parse(
            "--bool",
            "--i8", "8",
            "--i16", "0x10",
            "--i32", "32",
            "--i64", "0x40",
            "--big_int", "128",
            "--flt", "32.0",
            "--dbl", "64.0",
            "--big_decimal", "123.456",
            "--my_enum", "B",
            "--dur", "55s",
            "--name", "name",
            "--path", "/tmp",
            "--file", "/tmp",
        )
        assertThat(config.bool, `is`(true))
        assertThat(config.i8, `is`(8.toByte()))
        assertThat(config.i16, `is`(16.toShort()))
        assertThat(config.i32, `is`(32))
        assertThat(config.i64, `is`(64L))
        assertThat(config.bigInt, `is`(BigInteger("128")))
        assertThat(config.flt, `is`(32.0f))
        assertThat(config.dbl, `is`(64.0))
        assertThat(config.bigDecimal, `is`(BigDecimal("123.456")))
        assertThat(config.myEnum, `is`(MyEnum.B))
        assertThat(config.dur, `is`(Duration.ofSeconds(55)))
        assertThat(config.name, `is`("name"))
        assertThat(config.path, `is`(Path.of("/tmp")))
        assertThat(config.file, `is`(File("/tmp")))
    }

    @Test
    fun testGenerateArgs_Complex(console: Console) {
        val config = KotlinFieldsComplex()
        val args = ArgParser.argParser("foo", "bar", "baz")
            .generateArgsNameFormat(ArgNameFormat.SNAKE)
            .generateArgs(config)
            .build()
        args.parse(
            "--map", "foo=55",
            "--map", "bar=128",
            "--durations=42s",
            "--longs=1",
            "--longs=2",
            "--fields_name=FOO",
            "--other_value=55",
            "--hidden_list=foo",
            "--hidden_list=bar"
        )
        assertThat(config.map, `is`(mapOf("foo" to 55, "bar" to 128)))
        assertThat(config.durations, `is`(listOf(Duration.ofSeconds(42))))
        assertThat(config.longs, `is`(setOf(1L, 2L)))

        assertThat(config.hiddenList, `is`(listOf("foo", "bar")))

        assertThat(config.simple.name, `is`("FOO"))
        assertThat(config.other.value, `is`(55))
    }

    @Test
    fun testGenerateArgs_Ignored(console: Console) {
        val config = KotlinFieldsIgnored()
        val args = ArgParser.argParser("foo", "bar", "baz")
            .generateArgsNameFormat(ArgNameFormat.SNAKE)
            .generateArgs(config)
            .build()
        args.parse("--bool")
        args.validate()
        assertThat(config.bool, `is`(true))

        val help = ArgHelp.argHelp(args).usingTTYWidth(console.tty()).build()

        assertThat(
            getHelp(help).trimIndent(), `is`(
                """
            baz - bar
            Usage: foo [--bool]

             --bool : Apply setBool().
            """.trimIndent()
            )
        )
    }

}