/*
 * Copyright 2024 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.selection;

/**
 * Command reaction enum.
 */
public enum SelectionReaction {
    /**
     * Select the entry.
     */
    SELECT,

    /**
     * Exit selection with no value (null).
     */
    EXIT,

    /**
     * Stay in the selection.
     */
    STAY,

    /**
     * Stay in the selection and update entries (clear draw cache and redraw
     * all visible entries). Keeps the same selected item regardless of
     * position.
     */
    UPDATE_KEEP_ITEM,

    /**
     * Stay in the selection and update entries (clear draw cache and redraw
     * all visible entries). Keeps the same selected position regardless of
     * the underlying item.
     */
    UPDATE_KEEP_POSITION,
}
