/*
 * Copyright 2024 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.selection;

import net.morimekta.strings.chr.Color;

/**
 * Interface for the entry printer.
 *
 * @param <E> The entry type.
 */
@FunctionalInterface
public interface EntryPrinter<E> {
    /**
     * Print the entry line.
     *
     * @param entry   The entry to be printed.
     * @param bgColor The background color.
     * @return The entry line.throws IOException
     */
    String print(E entry, Color bgColor);

    /**
     * Print the entry line with default background.
     *
     * @param entry The entry to be printed.
     * @return The entry line.
     */
    default String print(E entry) {
        return print(entry, Color.BG_DEFAULT);
    }
}
