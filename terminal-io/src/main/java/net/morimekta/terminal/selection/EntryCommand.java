/*
 * Copyright 2024 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.selection;

import net.morimekta.strings.chr.Char;
import net.morimekta.strings.chr.Unicode;

import static java.util.Objects.requireNonNull;

/**
 * Command. The command works on an entry.
 *
 * @param <E> Value type.
 */
public class EntryCommand<E> {
    /**
     * Make an entry command.
     *
     * @param key    The key to make the action on.
     * @param name   Name of action.
     * @param action Action to make.
     */
    public EntryCommand(char key, String name, EntryAction<E> action) {
        this(new Unicode(key), name, action, false);
    }

    /**
     * Make a hidden entry command.
     *
     * @param key    The key to make the action on.
     * @param action Action to make.
     */
    public EntryCommand(char key, EntryAction<E> action) {
        this(new Unicode(key), "", action, true);
    }

    /**
     * Make an entry command.
     *
     * @param key    The key to make the action on.
     * @param name   Name of action.
     * @param action Action to make.
     */
    public EntryCommand(Char key, String name, EntryAction<E> action) {
        this(key, name, action, false);
    }

    /**
     * Make a hidden entry command.
     *
     * @param key    The key to make the action on.
     * @param action Action to make.
     */
    public EntryCommand(Char key, EntryAction<E> action) {
        this(key, "", action, true);
    }

    private EntryCommand(Char key, String name, EntryAction<E> action, boolean hidden) {
        // Since '\r' and 'n' are basically interchangeable.
        this.key = key.codepoint() == '\r' ? new Unicode('\n') : key;
        this.name = name;
        this.action = requireNonNull(action, "action == null");
        this.hidden = hidden;
    }

    /**
     * Key to react to.
     */
    public final Char           key;
    /**
     * Name of action or empty string.
     */
    public final String         name;
    /**
     * Action callback.
     */
    public final EntryAction<E> action;
    /**
     * If the entry action is hidden.
     */
    public final boolean        hidden;
}
