/*
 * Copyright 2024 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.selection;

import java.util.List;

/**
 * Represents a source of entries that can be selected.
 *
 * @param <E> Type of entries.
 */
public interface EntrySource<E> {
    /**
     * @return Number of entries available for selection.
     */
    int size();

    /**
     * @param i Index to get entry of.
     * @return Entry at absolute index.
     */
    E get(int i);

    /**
     * @param e Entry to find index of.
     * @return Index of entry, or -1 if no such entry found.
     */
    int indexOf(E e);

    /**
     * Load an ordered list of entries from the source.
     *
     * @param offset     Offset of start of loaded section.
     * @param maxEntries Maximum number of entries to be fetched.
     * @return List of loaded entries.
     */
    List<E> load(int offset, int maxEntries);
}
