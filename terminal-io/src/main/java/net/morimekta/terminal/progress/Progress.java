/*
 * Copyright 2021 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.progress;

/**
 * A progress state data class.
 */
public class Progress {
    private final long progress;
    private final long total;

    /**
     * Create progress instance.
     * @param progress Progress value.
     * @param total Progress expected total.
     */
    public Progress(long progress, long total) {
        if (total <= 0) {
            throw new IllegalArgumentException("total <= 0: " + total);
        }
        if (progress < 0) {
            throw new IllegalArgumentException("progress < 0" + progress);
        }
        if (progress > total) {
            throw new IllegalArgumentException(progress + " < " + total);
        }
        this.progress = progress;
        this.total = total;
    }

    /**
     * @return The current progress of total.
     */
    public long getProgress() {
        return progress;
    }

    /**
     * @return The total of which the progress is part of.
     */
    public long getTotal() {
        return total;
    }

    /**
     * @return The ration of progress of the total.
     */
    public double getRatio() {
        return ((double) progress) / total;
    }
}
