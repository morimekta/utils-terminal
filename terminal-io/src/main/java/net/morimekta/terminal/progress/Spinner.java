/*
 * Copyright 2021 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.progress;

import java.time.Duration;

/**
 * Which spinner to show. Some may require extended unicode font to be used in the console without just showing '?'.
 */
public interface Spinner {
    /**
     * Generate a progress line that can contain a progress bar, percentage
     * progress and a spinner as printed during processing, including not
     * started yet.
     * <p>
     * E.g.: <code>"[###   ]  50% / + 13.7  s"</code>
     *
     * @param pct        Percent (0..100) progress.
     * @param spinnerPos An incremental number used to spin the "spinner".
     * @param remaining  Optional remaining duration.
     * @param width      The printable with of the output progress string.
     * @return The formatted output for the spinner. The output must be limited
     *         to <code>width</code> printable width.
     */
    String atProgress(double pct, int spinnerPos, Duration remaining, int width);

    /**
     * Generate a progress line that can contain a progress bar, percentage
     * progress and a spinner for a completed progress.
     * <p>
     * E.g.: <code>"[######] 100% v @ 13.7  s"</code>
     *
     * @param spent Optional duration spent.
     * @param width The printable with of the output progress string.
     * @return The formatted output for the spinner.
     */
    String atComplete(Duration spent, int width);

    /**
     * Generate a progress line that can contain a progress bar, percentage
     * progress and a spinner for stopped progress with a message.
     * <p>
     * E.g.: <code>"[###   ]  50% Aborted"</code>
     *
     * @param pct     Percent (0..100) progress.
     * @param message Message to display in place of the remaining duration.
     * @param width   The printable with of the output progress string.
     * @return The formatted output for the spinner.
     */
    String atStopped(double pct, String message, int width);
}
