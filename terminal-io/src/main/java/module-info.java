/**
 * Package containing utilities for handling advanced terminal I/O.
 */
module net.morimekta.terminal {
    exports net.morimekta.terminal.input;
    exports net.morimekta.terminal.progress;
    exports net.morimekta.terminal.selection;
    exports net.morimekta.terminal;

    requires transitive net.morimekta.strings;
    requires transitive net.morimekta.io;
    requires net.morimekta.collect;
}