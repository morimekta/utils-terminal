/*
 * Copyright 2021 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test;

import net.morimekta.terminal.LinePrinter;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static net.morimekta.strings.StringUtil.stripNonPrintable;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LinePrinterTest {
    @Test
    public void testSimpleLines() {
        List<String> lines = new ArrayList<>();
        LinePrinter printer = l -> lines.add(stripNonPrintable(l));

        printer.println("simple");
        printer.trace("the less you know");
        printer.debug("the more you see");
        printer.info("the info 45%");
        printer.warn("the warn");
        printer.error("the err");
        printer.fatal("the fat");

        assertThat(lines, is(List.of(
                "simple",
                "[trace] the less you know",
                "[debug] the more you see",
                "[info] the info 45%",
                "[warn] the warn",
                "[error] the err",
                "[FATAL] the fat")));
    }

    @Test
    public void testFormattedLines() {
        ArrayList<String> lines = new ArrayList<>();
        LinePrinter printer = l -> lines.add(stripNonPrintable(l));

        printer.trace("the %d you know", -1);
        printer.debug("the %d you see", 1);
        printer.info("the %d info", 1);
        printer.warn("the %d warn", 2);
        printer.error("the %d err", 3);
        printer.fatal("the %d fat", 4);
        printer.formatln("the %d format", 5);

        assertThat(lines, is(List.of(
                "[trace] the -1 you know",
                "[debug] the 1 you see",
                "[info] the 1 info",
                "[warn] the 2 warn",
                "[error] the 3 err",
                "[FATAL] the 4 fat",
                "the 5 format")));
    }
}
