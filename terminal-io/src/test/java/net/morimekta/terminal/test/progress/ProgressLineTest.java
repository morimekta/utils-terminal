/*
 * Copyright 2021 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test.progress;

import net.morimekta.terminal.progress.DefaultSpinners;
import net.morimekta.terminal.progress.Progress;
import net.morimekta.terminal.progress.ProgressLine;
import net.morimekta.terminal.progress.Spinner;
import net.morimekta.testing.concurrent.FakeClock;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import net.morimekta.testing.junit5.ConsoleExtension;
import net.morimekta.testing.junit5.ConsoleSize;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.io.PrintStream;
import java.time.Clock;
import java.util.function.IntSupplier;

import static net.morimekta.strings.StringUtil.stripNonPrintable;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(ConsoleExtension.class)
@ConsoleSize(cols = 80)
@ConsoleDumpOutputOnFailure
@ConsoleDumpErrorOnFailure
@SuppressWarnings("PreferJavaTimeOverload")
public class ProgressLineTest {
    private static class TestProgressLine extends ProgressLine {
        protected TestProgressLine(PrintStream updater,
                                   IntSupplier widthSupplier,
                                   Clock clock,
                                   Spinner spinner,
                                   String title) {
            super(updater, widthSupplier, clock, spinner, title);
        }
    }

    @Test
    public void testProgress(Console console) throws InterruptedException {
        FakeClock clock = new FakeClock();

        try (ProgressLine p = new TestProgressLine(
                System.out,
                () -> 80,
                clock,
                DefaultSpinners.ASCII,
                "Title")) {
            clock.tick(200);
            p.onNext(new Progress(50, 128));
            clock.tick(300);
            // p.onNext(new Progress(126, 128));
            clock.tick(400);
            p.onNext(new Progress(127, 128));
            p.onComplete();
        }

        Thread.sleep(10L);

        assertThat(stripNonPrintable(console.output()),
                   is("\r"
                      + "Title: [----------------------------------------------------]   0% |            \r"
                      + "Title: [####################--------------------------------]  39% /            \r"
                      + "Title: [####################################################]  99% -            \r"
                      + "Title: [####################################################] 100% v @     0.9 s\r\n"));
    }

    @Test
    public void testAbortedOnClose(Console console) {
        try (ProgressLine p = new ProgressLine(
                console.tty(),
                DefaultSpinners.ASCII,
                "Foo")) {
            p.onNext(new Progress(37, 100));
        }

        assertThat(stripNonPrintable(console.output()),
                   is("\r"
                      + "Foo: [------------------------------------------------------]   0% |            \r"
                      + "Foo: [####################----------------------------------]  37% |            \r"
                      + "Foo: [####################----------------------------------]  37% Aborted      \r\n"));
    }

    @Test
    public void testAbortedOnException(Console console) {
        try (ProgressLine p = new ProgressLine(
                console.tty(),
                DefaultSpinners.ASCII,
                "Foo")) {
            p.onNext(new Progress(37, 100));
            p.onError(new IOException("Foo"));
        }

        assertThat(stripNonPrintable(console.output()),
                   is("\r"
                      + "Foo: [------------------------------------------------------]   0% |            \r"
                      + "Foo: [####################----------------------------------]  37% |            \r"
                      + "Foo: [####################----------------------------------]  37% Foo          \r\n"));
    }

    @Test
    public void testTerminal(Console console) {
        try (ProgressLine ignore = new ProgressLine(console.tty(), DefaultSpinners.ASCII, "Foo")) {
            assertThat(console.output(), is(
                    "\r"
                    + "Foo: ["
                    + "\033[32m\033[33m"
                    + "------------------------------------------------------"
                    + "\033[0m]"
                    + "   0% "
                    + "\033[1;33m"
                    + "|"
                    + "\033[0m"
                    + "            "));
            console.reset();
        }
        assertThat(console.output(), is(
                "\r"
                + "Foo: ["
                + "\033[32m\033[33m"
                + "------------------------------------------------------"
                + "\033[0m]"
                + "   0% "
                + "\033[1;31m"
                + "Aborted      "
                + "\033[0m"
                + "\r\n"
                ));
    }

    @Test
    public void testRemainingDuration(Console console) {
        FakeClock clock = new FakeClock();

        try (ProgressLine p = new TestProgressLine(
                System.out,
                () -> 78,
                clock,
                DefaultSpinners.BLOCKS,
                "Title")) {
            clock.tick(2000);
            p.onNext(new Progress(50, 127));
            clock.tick(1500);
            p.onNext(new Progress(100, 127));
            clock.tick(1321);
            p.onComplete();
        }
        assertThat(stripNonPrintable(console.output()),
                   is("\r"
                      + "Title: [⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]   0% ▁            \r"
                      + "Title: [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  39% ▂            \r"
                      + "Title: [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  79% ▃ +     0.9 s\r"
                      + "Title: [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100% ✓ @     4.8 s\r\n"));

    }
}
