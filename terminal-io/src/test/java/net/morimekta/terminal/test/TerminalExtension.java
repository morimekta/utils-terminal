/*
 * Copyright 2024 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test;

import net.morimekta.terminal.Terminal;
import net.morimekta.testing.junit5.ConsoleExtension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Extension for adding a fully virtual TTY and I/O for testing. This will forcefully replace standard in, out and err
 * while the test is running, falling back to default (system streams) when completed. This means any test that uses
 * normal system I/O to print ongoing status will not work with this extension.
 *
 * <pre>{@code
 * {@literal@}ExtendWith(ConsoleExtension.class)
 * public class MyTest {
 *     {@literal@}Test
 *     public void testMyThing(Console console) {
 *         // use the console I/O or TTY or both.
 *     }
 * }
 * }</pre>
 */
public class TerminalExtension extends ConsoleExtension {
    private final AtomicReference<Terminal> terminal = new AtomicReference<>();

    public TerminalExtension() {}

    @Override
    public void afterEach(ExtensionContext context) {
        var old = terminal.getAndSet(null);
        if (old != null) {
            old.finish();
            old.close();
        }

        super.afterEach(context);
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        Class<?> type = parameterContext.getParameter().getType();
        if (type.equals(Terminal.class)) return true;
        return super.supportsParameter(parameterContext, extensionContext);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        Class<?> type = parameterContext.getParameter().getType();
        if (type.equals(Terminal.class)) {
            return terminal.updateAndGet(old -> {
                if (old != null) {
                    return old;
                }
                var console = getConsole();
                return new Terminal(
                        console.tty(),
                        console.getConsoleIn(),
                        console.getConsoleOut(),
                        console.tty().getCurrentMode(),
                        null);
            });
        }
        return super.resolveParameter(parameterContext, extensionContext);
    }
}