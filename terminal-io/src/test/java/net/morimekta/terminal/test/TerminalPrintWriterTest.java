/*
 * Copyright 2024 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test;

import net.morimekta.io.tty.TTYMode;
import net.morimekta.terminal.Terminal;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import net.morimekta.testing.junit5.ConsoleExtension;
import net.morimekta.testing.junit5.ConsoleMode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Locale;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(ConsoleExtension.class)
@ConsoleDumpOutputOnFailure
@ConsoleDumpErrorOnFailure
public class TerminalPrintWriterTest {
    @Test
    @ConsoleMode(TTYMode.COOKED)
    public void testFluentCooked(Console console) {
        try (var terminal = new Terminal(console.tty())) {
            var writer = terminal.printWriter();
            writer.format("foo, %s", "bar\n")
                  .printf("foo, %s\n", "baz")
                  .printf(Locale.GERMAN, "num: %2.5f\n", 1.234)
                  .append(null)
                  .append(' ')
                  .append(null, 2, 4)
                  .append("X, fizzY", 1, 7);
        }
        assertThat(console.output(),
                   is("foo, bar\n"
                      + "foo, baz\n"
                      + "num: 1,23400\n"
                      + "null ll, fizz\n"));
    }

    @Test
    @ConsoleMode(TTYMode.RAW)
    public void testFluentRaw(Console console) {
        try (var terminal = new Terminal(console.tty())) {
            var writer = terminal.printWriter();
            writer.format("foo, %s", "bar\n")
                  .printf("foo, %s\n", "baz")
                  .printf(Locale.GERMAN, "num: %2.5f\n", 1.234)
                  .append(null)
                  .append(' ')
                  .append(null, 2, 4)
                  .append("X, fizzY", 1, 7);
        }
        assertThat(console.output(),
                   is("foo, bar\n"
                      + "foo, baz\n"
                      + "num: 1,23400\n"
                      + "null ll, fizz\n"));
    }
}
