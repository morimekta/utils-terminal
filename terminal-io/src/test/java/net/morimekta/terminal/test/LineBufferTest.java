/*
 * Copyright 2021 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test;

import net.morimekta.io.tty.TTYMode;
import net.morimekta.terminal.LineBuffer;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import net.morimekta.testing.junit5.ConsoleExtension;
import net.morimekta.testing.junit5.ConsoleMode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

import static net.morimekta.strings.EscapeUtil.javaEscape;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(ConsoleExtension.class)
@ConsoleDumpOutputOnFailure
@ConsoleDumpErrorOnFailure
@ConsoleMode(TTYMode.RAW)
public class LineBufferTest {
    private LineBuffer buffer;
    private Console    console;

    @BeforeEach
    public void setUp(Console console) {
        this.console = console;
        this.buffer = new LineBuffer(System.out);
    }

    @Test
    public void testClearLastInvalid() {
        buffer.addAll(List.of("first", "second", "third"));
        buffer.clearLast(0);
        assertThat(buffer.asList(), is(List.of("first", "second", "third")));
        assertThrows(IllegalArgumentException.class, () -> {
            buffer.clearLast(-1);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            buffer.clearLast(4);
        });
        assertThat(buffer.asList(), is(List.of("first", "second", "third")));
        buffer.clearLast(1);
        assertThat(buffer.asList(), is(List.of("first", "second")));
        buffer.clearLast(2);
        assertThat(buffer.asList(), is(List.of()));
    }

    @Test
    public void testAdd() {
        assertThat(buffer.addAll(List.of("first", "second", "third")), is(true));
        assertThat(buffer.addAll(List.of()), is(false));
        assertThat(console.output(),
                   is("first" +
                      "\r\n" +
                      "second" +
                      "\r\n" +
                      "third"));

        console.reset();
        buffer.add("fourth");
        assertThat(console.output(),
                   is("\r\n" +
                      "fourth"));

        buffer.add(1, "fifth");
        assertThat(buffer.asList(), is(List.of("first", "fifth", "second", "third", "fourth")));

        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.add(-1, "meh");
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.add(6, "meh");
        });
        buffer.add(5, "more");
        assertThat(buffer.asList(), is(List.of("first", "fifth", "second", "third", "fourth", "more")));
    }

    @Test
    public void testSetAll() {
        assertThat(buffer.setAll(0, List.of("first", "second", "third")), is(true));
        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.setAll(4, List.of("fourth"));
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.setAll(-1, List.of("negative"));
        });
        assertThat(buffer.setAll(1, List.of()), is(false));
        buffer.setAll(1, List.of("second-2"));
        buffer.setAll(2, List.of("third-2"));
        assertThat(buffer.asList(), is(List.of("first", "second-2", "third-2")));

        console.reset();
        buffer.setAll(2, List.of("third-3", "fourth"));
        assertThat(console.output(), is("\r\033[Kthird-3\r\nfourth"));

        assertThat(buffer.asList(), is(List.of("first", "second-2", "third-3", "fourth")));

        console.reset();
        assertThat(buffer.setAll(2, List.of("third", "fourth-2")), is(true));
        assertThat(console.output(), is("\r\033[1A\033[Kthird\r\n\033[Kfourth-2"));

        assertThat(buffer.asList(), is(List.of("first", "second-2", "third", "fourth-2")));
    }

    @Test
    public void testUpdate1() {
        buffer.addAll(List.of("first", "second", "third"));
        console.reset();

        buffer.set(1, "fourth");
        assertThat(console.output(),
                   is("\r\033[1A\033[K" +
                      "fourth" +
                      "\r\033[1B\033[5C"));
    }

    @Test
    public void testGet() {
        buffer.addAll(List.of("first", "second", "third"));
        assertThat(buffer.get(0), is("first"));
        assertThat(buffer.get(1), is("second"));
        assertThat(buffer.get(2), is("third"));
        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.get(-1);
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.get(3);
        });
    }

    @Test
    public void testAllAddIndex() {
        buffer.addAll(0, List.of("first", "second", "third"));
        buffer.addAll(1, List.of("fourth", "fifth"));
        assertThat(buffer.asList(), is(List.of("first", "fourth", "fifth", "second", "third")));

        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.addAll(-1, List.of("negative"));
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.addAll(7, List.of("too high"));
        });
        assertThat(buffer.addAll(1, List.of()), is(false));
        assertThat(buffer.addAll(5, List.of("foo")), is(true));
        assertThat(buffer.asList(), is(List.of("first", "fourth", "fifth", "second", "third", "foo")));
    }

    @Test
    public void testSetIndex() {
        buffer.addAll(List.of("first", "second", "third"));
        console.reset();
        assertThat(buffer.set(1, "second-2"), is("second"));
        assertThat(console.output(), is("\r\033[1A\033[Ksecond-2\r\033[1B\033[5C"));

        assertThat(buffer.asList(), is(List.of("first", "second-2", "third")));

        console.reset();
        assertThat(buffer.set(3, "fourth"), is(nullValue()));
        assertThat(console.output(), is("\r\nfourth"));

        assertThat(buffer.asList(), is(List.of("first", "second-2", "third", "fourth")));
    }

    @Test
    public void testRemove() {
        buffer.addAll(List.of("first", "second", "third"));
        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.remove(-1);
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            buffer.remove(3);
        });
        console.reset();
        // remove internal.
        assertThat(buffer.remove(0), is("first"));
        assertThat(javaEscape(console.output()),
                   is(javaEscape(
                           "\r\033[K"
                           + "\033[2A"
                           + "\033[Ksecond\r\n"
                           + "\033[Kthird")));
        console.reset();
        // remove last
        assertThat(buffer.remove(1), is("third"));
        assertThat(javaEscape(console.output()),
                   is(javaEscape("\r\033[K\033[A\033[6C")));
        console.reset();
        // remove only.
        assertThat(buffer.remove(0), is("second"));
        assertThat(javaEscape(console.output()),
                   is(javaEscape("\r\033[K")));
        assertThat(buffer.asList(), is(List.of()));
    }

    @Test
    public void testRemoveObject() {
        buffer.addAll(List.of("first", "second", "third"));
        console.reset();
        // remove existing.
        assertThat(buffer.remove("second"), is(true));
        assertThat(javaEscape(console.output()),
                   is(javaEscape("\r\033[K\033[1A\033[Kthird")));
        console.reset();
        // remove non-existing.
        assertThat(buffer.remove("second"), is(false));
        assertThat(javaEscape(console.output()), is(""));
    }

    @Test
    public void testRemoveAll() {
        buffer.addAll(List.of("first", "second", "third", "fourth", "fifth"));
        console.reset();
        assertThat(buffer.removeAll(List.<String>of()), is(false));
        assertThat(console.output(), is(""));
        assertThat(buffer.removeAll(List.of("third", "eight")), is(true));
        assertThat(javaEscape(console.output()),
                   is(javaEscape(
                           "\r\033[K"
                           + "\033[A\033[K"
                           + "\033[A\033[K"
                           + "\033[A\033[K"
                           + "\033[A\033[K"
                           + "first\r\n"
                           + "second\r\n"
                           + "fourth\r\n"
                           + "fifth")));
        console.reset();
        assertThat(buffer.removeAll(List.of("eight")), is(false));
        assertThat(javaEscape(console.output()),
                   is(javaEscape("")));

        assertThat(buffer.removeAll(List.of("first", "second", "third", "fourth", "fifth")), is(true));
        assertThat(javaEscape(console.output()),
                   is(javaEscape(
                           "\r\033[K"
                           + "\033[A\033[K"
                           + "\033[A\033[K"
                           + "\033[A\033[K")));

        assertThat(buffer.asList(), is(List.of()));
    }

    @Test
    public void testRetainAll() {
        buffer.addAll(List.of("first", "second", "third", "fourth", "fifth"));
        console.reset();
        assertThat(buffer.retainAll(List.of("first", "second", "third", "fourth", "fifth", "sixth")),
                   is(false));
        assertThat(console.output(), is(""));

        assertThat(buffer.retainAll(List.of("first", "third", "fifth", "seventh")), is(true));
        assertThat(javaEscape(console.output()),
                   is(javaEscape(
                           "\r\033[K"
                           + "\033[A\033[K"
                           + "\033[A\033[K"
                           + "\033[A\033[K"
                           + "\033[A\033[K"
                           + "first\r\n"
                           + "third\r\n"
                           + "fifth")));
    }

    @Test
    public void testSubList() {
        buffer.addAll(List.of("first", "second", "third", "fourth", "fifth"));
        assertThat(buffer.subList(1, 3), is(List.of("second", "third")));
    }

    @Test
    public void testIndexOf() {
        buffer.addAll(List.of("first", "second", "third", "second", "first"));
        assertThat(buffer.indexOf("first"), is(0));
        assertThat(buffer.lastIndexOf("first"), is(4));
        assertThat(buffer.indexOf("second"), is(1));
        assertThat(buffer.lastIndexOf("second"), is(3));
        assertThat(buffer.indexOf("third"), is(2));
        assertThat(buffer.lastIndexOf("third"), is(2));
    }

    @Test
    public void testContains() {
        buffer.addAll(List.of("first", "second", "third", "fourth", "fifth"));

        assertThat(buffer.contains("second"), is(true));
        assertThat(buffer.contains("sixth"), is(false));
        assertThat(buffer.containsAll(List.of("first", "second")), is(true));
        assertThat(buffer.containsAll(List.of("first", "sixth")), is(false));
    }

    @Test
    public void testClear() {
        buffer.addAll(List.of("first", "second", "third"));
        console.reset();

        buffer.clear();
        assertThat(console.output(),
                   is("\r" +
                      "\033[K\033[A" +
                      "\033[K\033[A" +
                      "\033[K"));
        buffer.clear();
        assertThat(buffer.asList(), is(List.of()));
    }

    @Test
    public void testClearLastN() {
        buffer.addAll(List.of("first", "second", "third"));
        console.reset();

        buffer.clearLast(2);
        assertThat(javaEscape(console.output()),
                   is(javaEscape("\r" +
                                 "\033[K\033[A" +
                                 "\033[K\033[A" +
                                 "\033[5C")));

        console.reset();
        buffer.clearLast(1);
        assertThat(console.output(),
                   is("\r\033[K"));
    }

    @Test
    public void testToArray() {
        buffer.addAll(List.of("first", "second", "third"));
        assertArrayEquals(buffer.toArray(), new Object[]{"first", "second", "third"});
        assertArrayEquals(buffer.toArray(new String[0]), new String[]{"first", "second", "third"});
    }

    @Test
    public void testIterator() {
        buffer.addAll(List.of("first", "second", "third"));
        var it = buffer.iterator();
        assertThat(it.hasNext(), is(true));
        assertThat(it.next(), is("first"));
        assertThat(it.hasNext(), is(true));
        assertThat(it.next(), is("second"));
        assertThat(it.hasNext(), is(true));
        assertThat(it.next(), is("third"));
        assertThat(it.hasNext(), is(false));
        assertThrows(IndexOutOfBoundsException.class, it::next);
    }

    @Test
    public void testIteratorRemove() {
        buffer.addAll(List.of("first", "second", "third"));
        var it = buffer.iterator();
        assertThat(it.hasNext(), is(true));
        assertThat(it.next(), is("first"));
        it.remove();

        // remove a removed item.
        assertThrows(IndexOutOfBoundsException.class, it::remove);

        var remaining = new ArrayList<String>();
        it.forEachRemaining(remaining::add);
        assertThat(remaining, is(List.of("second", "third")));
        assertThat(buffer.asList(), is(remaining));
    }

    @Test
    public void testListIterator() {
        buffer.addAll(List.of("first", "second", "third"));
        var it = buffer.listIterator();
        assertThat(it.nextIndex(), is(0));
        assertThat(it.previousIndex(), is(-1));
        assertThat(it.hasNext(), is(true));
        assertThat(it.hasPrevious(), is(false));

        assertThat(it.next(), is("first"));

        assertThat(it.hasNext(), is(true));
        assertThat(it.hasPrevious(), is(true));

        assertThat(it.previous(), is("first"));

        assertThrows(IndexOutOfBoundsException.class, it::previous);
    }

    @Test
    public void testListIteratorSet() {
        buffer.addAll(List.of("first", "second", "third"));
        var it = buffer.listIterator();
        assertThrows(IndexOutOfBoundsException.class, () -> {
            it.set("foo");
        });
        assertThat(it.next(), is("first"));
        it.set("first-1");
        assertThat(it.next(), is("second"));
        assertThat(it.previous(), is("second"));
        it.set("second-2");
        assertThat(it.next(), is("second-2"));
        assertThat(buffer.asList(), is(List.of("first-1", "second-2", "third")));
    }

    @Test
    public void testListIteratorAdd() {
        buffer.addAll(List.of("first"));
        var it = buffer.listIterator();

        assertThat(it.hasPrevious(), is(false));
        assertThat(it.hasNext(), is(true));

        it.add("second");

        assertThat(it.hasPrevious(), is(true));
        assertThat(it.hasNext(), is(true));
        assertThat(it.next(), is("first"));

        assertThat(it.hasPrevious(), is(true));
        assertThat(it.hasNext(), is(false));

        it.add("third");

        assertThat(it.hasPrevious(), is(true));
        assertThat(it.hasNext(), is(false));
        assertThat(it.nextIndex(), is(-1));

        assertThat(it.previous(), is("third"));

        assertThat(buffer.asList(), is(List.of("second", "first", "third")));
    }

    @Test
    public void testListIteratorIndex() {
        buffer.addAll(List.of("first", "second", "third"));
        var it = buffer.listIterator(2);
        assertThat(it.next(), is("third"));
        assertThat(it.hasNext(), is(false));
    }

    @Test
    public void testLineBufferObject() {
        buffer.addAll(List.of("first", "second", "third"));
        assertThat(buffer, is(buffer));
        assertThat(buffer.hashCode(), is(buffer.hashCode()));
        assertThat(buffer, is(not("foo")));
        var other = new LineBuffer(console.getConsoleOut());
        assertThat(other.toString(), is("[]"));
        assertThat(buffer, is(not(other)));
        assertThat(buffer.hashCode(), is(not(other.hashCode())));
        other.addAll(List.of("first", "second", "third"));
        assertThat(buffer, is(other));
        assertThat(buffer.hashCode(), is(other.hashCode()));
        assertThat(buffer.toString(), is("[\"first\", \"second\", \"third\"]"));
    }

    @Test
    public void testReversed() {
        buffer.addAll(List.of("first", "second", "third"));
        assertThat(buffer.reversed(), is(List.of("third", "second", "first")));
    }
}
