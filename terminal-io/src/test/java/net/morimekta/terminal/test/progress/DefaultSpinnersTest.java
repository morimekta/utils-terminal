/*
 * Copyright 2022 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test.progress;

import org.junit.jupiter.api.Test;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofMinutes;
import static java.time.Duration.ofSeconds;
import static net.morimekta.strings.StringUtil.stripNonPrintable;
import static net.morimekta.terminal.progress.DefaultSpinners.ASCII;
import static net.morimekta.terminal.progress.DefaultSpinners.BLOCKS;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DefaultSpinnersTest {
    @Test
    public void testAscii() {
        assertThat(stripNonPrintable(ASCII.atProgress(0.0, 0, null, 40)),
                   is("[-------------------]   0% |            "));
        assertThat(stripNonPrintable(ASCII.atProgress(0.33, 1, ofMillis(2468), 40)),
                   is("[######-------------]  33% / +     2.4 s"));
        assertThat(stripNonPrintable(ASCII.atProgress(0.50, 2, ofSeconds(123), 40)),
                   is("[##########---------]  50% - +  2:03 min"));
        assertThat(stripNonPrintable(ASCII.atProgress(0.66, 3, ofMinutes(135), 40)),
                   is("[#############------]  66% \\ +  2:15 Hrs"));
        assertThat(stripNonPrintable(ASCII.atProgress(1.0, 4, ofMinutes(1357), 40)),
                   is("[###################] 100% | + 22:37 Hrs"));

        assertThat(stripNonPrintable(ASCII.atComplete(ofMinutes(3210), 40)),
                   is("[###################] 100% v @ 53:30 Hrs"));
        assertThat(stripNonPrintable(ASCII.atStopped(0.67, "Cancelled", 40)),
                   is("[#############------]  67% Cancelled    "));
        assertThat(stripNonPrintable(ASCII.atStopped(0.99, "Yes, Truly Aborted", 40)),
                   is("[###################]  99% Yes, Truly Ab"));
    }

    @Test
    public void testBlocks() {
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.0, 0, null, 70)),
                   is("[⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]   0% ▁            "));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.05, 1, ofMinutes(100), 70)),
                   is("[▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]   5% ▂ +  1:40 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.10, 2, ofMinutes(95), 70)),
                   is("[▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  10% ▃ +  1:35 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.15, 3, ofMinutes(90), 70)),
                   is("[▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  15% ▄ +  1:30 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.20, 4, ofMinutes(85), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  20% ▅ +  1:25 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.25, 5, ofMinutes(80), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  25% ▆ +  1:20 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.30, 6, ofMinutes(75), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  30% ▇ +  1:15 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.35, 7, ofMinutes(70), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  35% █ +  1:10 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.40, 8, ofMinutes(65), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  40% ▇ +  1:05 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.45, 9, ofMinutes(60), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  45% ▆ +  1:00 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.50, 10, ofMinutes(55), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  50% ▅ + 55:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.55, 11, ofMinutes(50), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  55% ▄ + 50:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.60, 12, ofMinutes(45), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  60% ▂ + 45:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.65, 13, ofMinutes(40), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  65% ▁ + 40:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.70, 14, ofMinutes(35), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  70% ▁ + 35:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.75, 15, ofMinutes(30), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  75% ▂ + 30:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.80, 16, ofMinutes(25), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  80% ▃ + 25:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.85, 17, ofMinutes(20), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅]  85% ▄ + 20:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.90, 18, ofMinutes(15), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅]  90% ▅ + 15:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(0.95, 19, ofMinutes(10), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅]  95% ▆ + 10:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atProgress(1.00, 20, ofMinutes(5), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100% ▇ +  5:00 min"));
        assertThat(stripNonPrintable(BLOCKS.atComplete(ofMinutes(103), 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100% ✓ @  1:43 Hrs"));
        assertThat(stripNonPrintable(BLOCKS.atStopped(0.67, "Aborted, Really", 70)),
                   is("[▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅]  67% Aborted, Real"));
    }
}
