/*
 * Copyright 2021 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test;

import net.morimekta.io.tty.TTYMode;
import net.morimekta.io.tty.TTYSize;
import net.morimekta.strings.chr.Char;
import net.morimekta.terminal.Terminal;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import net.morimekta.testing.junit5.ConsoleExtension;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static net.morimekta.strings.EscapeUtil.javaEscape;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(ConsoleExtension.class)
@ConsoleDumpOutputOnFailure
@ConsoleDumpErrorOnFailure
public class TerminalTest {
    @Test
    @SuppressWarnings("CatchAndPrintStackTrace")
    public void testImpossible() {
        // To gather coverage on methods that cannot work properly both
        // in embedded (non-interactive) unit testing and in a real terminal.
        try (Terminal term = new Terminal()) {
            TTYSize size = term.tty().getTerminalSize();
            System.err.println("Terminal: " + size);
        } catch (Exception e) {
            // silence everything. It is sadly not possible to test the
            // actual output, as it differs between platforms.
            e.printStackTrace(System.err);
        }
    }

    @Test
    @SuppressWarnings("CatchAndPrintStackTrace")
    public void testImpossibleRaw() {
        // To gather coverage on methods that cannot work properly both
        // in embedded (non-interactive) unit testing and in a real terminal.
        try (Terminal term = new Terminal(TTYMode.RAW)) {
            TTYSize size = term.tty().getTerminalSize();
            System.err.println("Terminal: " + size);
        } catch (Exception e) {
            // silence everything. It is sadly not possible to test the
            // actual output, as it differs between platforms.
            e.printStackTrace(System.err);
        }
    }

    @Test
    public void testConfirm(Console console) {
        assertConfirm(console, "Test", "Test [y/n]: Yes.\r\n", true, 'y');
        assertConfirm(console, "Boo", "Boo [y/n]: Yes.\r\n", true, '\n', 'y');
        assertConfirm(console, "Test", "Test [y/n]: No.\r\n", false, 'n');
        assertConfirm(console, "Test", "Test [y/n]: No.\r\n", false, ' ', 'n');

        assertConfirm(console,
                      "Test",
                      "Test [y/n]: 'g' is not valid input." +
                      "\033[23D\033[KNo.\r\n", false, 'g', 'n');

        console.reset();
        console.setInput(' ');

        try (var terminal = new Terminal(console.tty())) {
            assertThat(terminal.confirm("Test", true), is(true));
            assertThat(console.output(), is("Test [Y/n]: Yes.\r\n"));
        }

        console.reset();
        console.setInput('\n');

        try (var terminal = new Terminal(console.tty())) {
            assertThat(terminal.confirm("Test", false), is(false));
            assertThat(console.output(), is("Test [y/N]: No.\r\n"));
        }

        console.reset();
        console.setInput('\t', Char.ESC);
        try (var terminal = new Terminal(console.tty())) {
            terminal.confirm("Test");
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: User interrupted: <ESC>"));
            assertThat(javaEscape(console.output()), is(javaEscape(
                    "Test [y/n]: '\\t' is not valid input." +
                    "\033[24D\033[KUser interrupted." +
                    "\r\n")));
        }

        console.reset();
        try (var terminal = new Terminal(console.tty())) {
            terminal.confirm("Test");
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: End of stream."));
            assertThat(console.output(), is("Test [y/n]:\r\n"));
        }
    }

    private void assertConfirm(Console console,
                               String msg,
                               String out,
                               boolean value,
                               Object... in) {
        console.reset();
        console.setInput(in);

        try (var terminal = new Terminal(console.tty())) {
            assertThat(terminal.confirm(msg), is(value));
            assertThat(javaEscape(console.output()), is(javaEscape(out)));
        }
    }

    @Test
    public void testPressToContinue(Console console) {
        console.setInput('\n');
        try (var terminal = new Terminal(console.tty())) {
            terminal.pressToContinue("Continue?");
            assertThat(console.output(), is("Continue?\r\n"));
        }
        assertThat(console.output(), is("Continue?\r\n"));
        console.reset();
        console.setInput(Char.ESC);
        try (var terminal = new Terminal(console.tty())) {
            terminal.pressToContinue("Continue?");
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: User interrupted: <ESC>"));
        }
        assertThat(console.output(), is("Continue? User interrupted.\r\n"));

        console.reset();
        try (var terminal = new Terminal(console.tty())) {
            terminal.pressToContinue("Continue?");
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: End of stream."));
        }
        assertThat(console.output(), is("Continue?\r\n"));
    }

    @Test
    public void testReadLine(Console console) {
        console.setInput('\n');
        try (var terminal = new Terminal(console.tty())) {
            assertThat(terminal.readLine("Something"), is(""));
            assertThat(console.output(), is("Something: \r\n"));
        }

        console.reset();
        try (var terminal = new Terminal(console.tty())) {
            terminal.readLine("Something");
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: End of stream."));
            assertThat(console.output(), is(
                    "Something: " +
                    "\r\n"));
        }
    }

    @Test
    @SuppressWarnings("CatchAndPrintStackTrace")
    public void testExecuteAbortable(Console console) throws IOException, ExecutionException, InterruptedException {
        ExecutorService service = Executors.newSingleThreadExecutor();
        try (Terminal term = new Terminal(console.tty())) {
            term.executeAbortable(service, () -> {
                // no-op
            });
            String s = term.executeAbortable(service, () -> "test");
            assertThat(s, is("test"));
        }

        console.reset();
        console.setInput(Char.ESC);

        try (Terminal term = new Terminal(console.tty())) {
            term.executeAbortable(service, () -> {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace(System.out);
                }
            });
            fail("No exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Aborted with '<ESC>'"));
        }

        console.reset();
        console.setInput(Char.ABR);

        try (Terminal term = new Terminal(console.tty())) {
            String res = term.executeAbortable(service, () -> {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace(System.out);
                }
                return "fail";
            });
            fail("No exception: " + res);
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Aborted with '<ABR>'"));
        }
    }

    @Test
    public void testExecuteAbortable_aborted(Console console) throws ExecutionException, InterruptedException {
        ExecutorService service = Executors.newSingleThreadExecutor();

        console.setInput(Char.ABR);
        try (Terminal term = new Terminal(console.tty())) {
            term.executeAbortable(service, () -> {
                try {
                    Thread.sleep(100L);
                } catch (InterruptedException ignore) {
                }
            });
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Aborted with '<ABR>'"));
        }

        console.setInput(Char.ABR);
        try (Terminal term = new Terminal(console.tty())) {
            String s = term.executeAbortable(service, () -> {
                try {
                    Thread.sleep(100L);
                } catch (InterruptedException ignore) {
                }
                return "";
            });
            fail("no exception: " + s);
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Aborted with '<ABR>'"));
        }
    }

    @Test
    public void testPrinter(Console console) {
        try (Terminal terminal = new Terminal(console.tty(), TTYMode.COOKED)) {
            new IOException("foo").printStackTrace(terminal.printStream());
        }

        List<String> lines = Arrays.asList(console.output().split("[\r\n]+"));
        assertThat(lines.size(), Matchers.greaterThan(2));
        assertThat(lines.get(0), is("java.io.IOException: foo"));
        assertThat(lines.get(1),
                   containsString("net.morimekta.terminal.test.TerminalTest.testPrinter(TerminalTest.java:"));
    }

    @Test
    public void testOpenClose(Console console) {
        assertThat(console.tty().getCurrentMode(), is(TTYMode.COOKED));

        try (var raw1 = new Terminal(console.tty(), TTYMode.RAW)) {
            raw1.lp().println("One");
            try (var cooked1 = raw1.withMode(TTYMode.COOKED)) {
                cooked1.lp().println("Two");
                try (var raw2 = cooked1.withMode(TTYMode.RAW)) {
                    raw2.lp().println("Three");
                    try (var cooked2 = raw2.withMode(TTYMode.COOKED)) {
                        cooked2.lp().println("Four");
                    }
                    raw2.lp().println("Five");
                    raw2.lp().println("Six");
                }
                cooked1.lp().println("Seven");
                cooked1.lp().println("Eight");
            }
            raw1.lp().println("Nine");
            raw1.lp().println("Ten");
        }
        assertThat(console.output(),
                   is("One\r\n"
                      + "Two\n"
                      + "Three\r\n"
                      + "Four\n"
                      + "Five\r\n"
                      + "Six\r\n"
                      + "Seven\n"
                      + "Eight\n"
                      + "Nine\r\n"
                      + "Ten\r\n"));
    }

    @Test
    public void testOpenCloseWithMessages(Console console) {
        assertThat(console.tty().getCurrentMode(), is(TTYMode.COOKED));

        try (var raw1 = new Terminal(console.tty(), TTYMode.RAW)) {
            raw1.lp().println("One");
            try (var cooked1 = raw1.withMode(TTYMode.COOKED)) {
                cooked1.printStream().println("Two");
                try (var raw2 = cooked1.withMode(TTYMode.RAW)) {
                    raw2.printWriter().println("Three");
                    try (var cooked2 = raw2.withMode(TTYMode.COOKED)) {
                        cooked2.lp().println("Four");
                    }
                    raw2.printWriter().println("Five");
                }
                cooked1.printStream().println("Six");
            }
            raw1.lp().println("Seven");
        }

        assertThat(console.output(), is(
                "One\r\n"
                + "Two\n"
                + "Three\r\n"
                + "Four\n"
                + "Five\r\n"
                + "Six\n"
                + "Seven\r\n"));
    }
}
