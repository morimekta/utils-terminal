/*
 * Copyright 2024 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test.selection;

import net.morimekta.strings.chr.Char;
import net.morimekta.terminal.selection.EntryAction;
import net.morimekta.terminal.selection.Selection;
import net.morimekta.terminal.selection.SelectionBuilder;
import net.morimekta.terminal.selection.SelectionReaction;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import net.morimekta.testing.junit5.ConsoleExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.strings.chr.Char.CR;
import static net.morimekta.strings.chr.Char.ESC;
import static net.morimekta.strings.chr.Char.LF;
import static net.morimekta.strings.chr.Control.DELETE;
import static net.morimekta.strings.chr.Control.DOWN;
import static net.morimekta.strings.chr.Control.DPAD_MID;
import static net.morimekta.strings.chr.Control.END;
import static net.morimekta.strings.chr.Control.HOME;
import static net.morimekta.strings.chr.Control.LEFT;
import static net.morimekta.strings.chr.Control.PAGE_DOWN;
import static net.morimekta.strings.chr.Control.PAGE_UP;
import static net.morimekta.strings.chr.Control.RIGHT;
import static net.morimekta.strings.chr.Control.UP;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Testing the line input.
 */
@ExtendWith(ConsoleExtension.class)
@ConsoleDumpOutputOnFailure
@ConsoleDumpErrorOnFailure
public class SelectionTest {
    @SuppressWarnings("unchecked")
    private final EntryAction<String> action = mock(EntryAction.class);

    @Test
    public void testEOF(Console console) {
        try (var select = selection(1).tty(console.tty()).build()) {
            select.runSelection();
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: End of input"));
        }
    }

    @Test
    public void testUserInterrupt(Console console) {
        try (var ignore = selection(1).tty(console.tty()).build()) {
            console.setInput(ESC);
            ignore.runSelection();
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: User interrupted: <ESC>"));
        }
    }

    public static Stream<Arguments> argsSelectOfFive() {
        return Stream.of(
                arguments("entry 3", listOf('3', CR)),
                arguments("entry 4", listOf(RIGHT, UP, CR)),
                arguments("entry 3", listOf(DOWN, DOWN, LF)),
                arguments("entry 2", listOf(DOWN, DOWN, UP, CR)),
                arguments("entry 1", listOf(RIGHT, HOME, CR)),
                arguments("entry 2", listOf(END, LEFT, DOWN, LF)),
                arguments("entry 1", listOf(RIGHT, PAGE_UP, CR)),
                arguments("entry 5", listOf(PAGE_DOWN, CR)),
                arguments(null, listOf('x')));
    }

    @ParameterizedTest
    @MethodSource("argsSelectOfFive")
    public void testSelectOfFive(String expected, List<Object> input, Console console) {
        console.setInput(input.toArray());
        try (var select = selection(5).tty(console.tty()).build()) {
            assertThat(select.runSelection(), is(expected));
        }
    }

    @Test
    public void testSimpleSelect(Console console) {
        console.setInput(DELETE, DPAD_MID, CR);
        try (var selection = selection(50).tty(console.tty()).build()) {
            assertThat(selection.runSelection(), is("entry 50"));
        }
    }

    @Test
    public void testSimpleAction(Console console) {
        console.setInput(PAGE_DOWN, 'a', 'x');
        try (var selection = selection(15).tty(console.tty()).build()) {
            assertThat(selection.runSelection(), is(nullValue()));
        }
        verify(action).call(eq(14), eq("entry 15"), any());
        verifyNoMoreInteractions(action);
    }

    private SelectionBuilder<String> selection(int num) {
        ArrayList<String> content = new ArrayList<>();
        for (int i = 1; i <= num; ++i) {
            content.add("entry " + i);
        }

        return Selection
                .newBuilder(content)
                .lineWidth(80)
                .pageSize(10)
                .pageMargin(5)
                .on('a', "Action", action)
                .on(DELETE, "Reverse-1", s -> {
                    Collections.reverse(content);
                    return SelectionReaction.UPDATE_KEEP_ITEM;
                })
                .on(DPAD_MID, "Reverse-2", (i, e, lp) -> {
                    Collections.reverse(content);
                    return SelectionReaction.UPDATE_KEEP_POSITION;
                })
                .on('x', "Exit", SelectionReaction.EXIT)
                .hiddenOn(Char.TAB, (i, e, lp) -> SelectionReaction.STAY)
                .hiddenOn(CR, SelectionReaction.SELECT);
    }
}
