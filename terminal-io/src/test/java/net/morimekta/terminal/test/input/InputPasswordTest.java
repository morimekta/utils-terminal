/*
 * Copyright 2021 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test.input;

import net.morimekta.strings.chr.Char;
import net.morimekta.strings.chr.Control;
import net.morimekta.terminal.Terminal;
import net.morimekta.terminal.input.InputPassword;
import net.morimekta.terminal.test.TerminalExtension;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.UncheckedIOException;

import static net.morimekta.strings.EscapeUtil.javaEscape;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Testing the line input.
 */
@ExtendWith(TerminalExtension.class)
@ConsoleDumpOutputOnFailure
@ConsoleDumpErrorOnFailure
public class InputPasswordTest {
    @Test
    public void testReadPassword(Console console, Terminal terminal) {
        try (InputPassword pw = new InputPassword(terminal, "Test")) {
            console.setInput('a',
                             Control.LEFT,
                             'b',
                             Char.CR);

            assertThat(pw.readPassword(), is("ba"));
        }
        assertThat(console.output(),
                   is("Test: \r\n"));
    }

    @Test
    public void testReadPasswordWithReplacement(Console console, Terminal terminal) {
        try (InputPassword pw = new InputPassword(terminal, "Test", "*")) {
            console.setInput('a',
                             Control.LEFT,
                             'b',
                             Char.CR);

            assertThat(pw.readPassword(), is("ba"));
        }
        assertThat(javaEscape(console.output()),
                   is(javaEscape("Test: *" +
                                 "\r\033[KTest: *\033[1D" +
                                 "\r\033[KTest: **\033[1D\r\n")));
    }

    @Test
    public void testEOF(Terminal terminal) {
        try (var pw = new InputPassword(terminal, "test")) {
            pw.readPassword();
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertEquals("java.io.IOException: End of input.", e.getMessage());
        }
    }

    @Test
    public void testMovements(Console console, Terminal terminal) {
        console.setInput(
                "aba",
                Control.LEFT,
                'b',
                Control.HOME,
                '-',
                Control.RIGHT,
                Control.END,
                Char.DEL,
                Control.LEFT,
                "e\t",
                Control.DELETE,
                "fg",
                Char.CR);
        try (var pw = new InputPassword(terminal, "test")) {
            assertThat(pw.readPassword(), is("-abefg"));
        }
    }

    @Test
    public void testInterrupt(Console console, Terminal terminal) {
        console.setInput("ab", Char.ESC);
        try (var pw = new InputPassword(terminal, "test")) {
            fail("Expected failure: " + pw.readPassword());
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(),
                       is("java.io.IOException: User interrupted: <ESC>"));
        }
    }
}
