/*
 * Copyright 2021 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test.input;

import net.morimekta.io.tty.TTYMode;
import net.morimekta.strings.chr.Char;
import net.morimekta.strings.chr.Control;
import net.morimekta.terminal.Terminal;
import net.morimekta.terminal.input.InputLine;
import net.morimekta.terminal.test.TerminalExtension;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import net.morimekta.testing.junit5.ConsoleMode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.stream.Collectors;

import static net.morimekta.strings.EscapeUtil.javaEscape;
import static net.morimekta.strings.StringUtil.stripNonPrintable;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Testing the line input.
 */
@ExtendWith(TerminalExtension.class)
@ConsoleDumpErrorOnFailure
@ConsoleDumpOutputOnFailure
public class InputLineTest {
    @Test
    public void testOutput_simple(Console console, Terminal terminal) {
        try (var li = new InputLine(terminal, "Test")) {
            assertThat(console.output(), is(""));

            console.setInput('a', Control.LEFT, 'b', Char.CR);

            assertThat(li.readLine(), is("ba"));
        }
        assertThat(javaEscape(console.output()),
                   is(javaEscape("Test: a\033[Dba\033[1D\r\n")));
    }

    @Test
    public void testEOF(Terminal terminal) {
        try (var input = new InputLine(terminal, "test")) {
            input.readLine();
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(),
                       is("java.io.IOException: End of stream."));
        }
    }

    @Test
    public void testMovements_1(Console console, Terminal terminal) {
        console.setInput("aba",
                         Control.LEFT,
                         Control.LEFT,
                         "cd",
                         Control.CTRL_LEFT,
                         Char.DEL, // BS
                         "e",
                         Control.RIGHT,
                         "f",
                         Control.CTRL_RIGHT,
                         "g",
                         Char.CR);
        try (var input = new InputLine(terminal, "test")) {
            assertThat(input.readLine(), is("eafcdbag"));
        }
    }

    @Test
    public void testMovements_2(Console console, Terminal terminal) throws IOException {
        console.setInput("aba",
                         Control.HOME,
                         "cd",
                         Control.END,
                         Char.DEL, // BS
                         "g",
                         Char.CR);
        try (var input = new InputLine(terminal, "test")) {
            assertThat(input.readLine(), is("cdabg"));
        }
    }

    @Test
    public void testMovements_3(Console console, Terminal terminal) {
        console.setInput(Control.CTRL_LEFT,
                         Control.ALT_W,
                         Control.HOME,
                         Control.CTRL_RIGHT,
                         Control.ALT_D,
                         Char.CR);
        try (var input = new InputLine(terminal, "test")) {
            assertThat(input.readLine("first second third fourth"), is("first fourth"));
        }
    }

    @Test
    public void testMovements_4(Console console, Terminal terminal) {
        console.setInput(Control.CTRL_LEFT,
                         Control.CTRL_LEFT,
                         Control.DELETE,
                         Control.ALT_K,
                         Char.CR);
        try (var input = new InputLine(terminal, "test")) {
            assertThat(input.readLine("first second third fourth"),
                       is("first second "));
        }

        console.setInput(Control.CTRL_LEFT,
                         Control.CTRL_LEFT,
                         Control.ALT_U,
                         Char.LF);
        try (var input = new InputLine(terminal, "test")) {
            assertThat(input.readLine("first second third fourth"),
                       is("third fourth"));
        }
    }

    @Test
    public void testMovements_5(Console console, Terminal terminal) {
        console.setInput("foo",
                         Control.ALT_W,
                         Control.ALT_W,
                         "bar",
                         Control.CTRL_LEFT,
                         Control.ALT_D,
                         Control.ALT_D,
                         Char.CR);
        try (var input = new InputLine(terminal, "test")) {
            assertThat(input.readLine(), is(""));
        }
    }

    @Test
    public void testMovements_6(Console console, Terminal terminal) {
        try (var input = new InputLine(terminal, "test")) {
            console.setInput("foo",
                             Control.CTRL_UP,
                             Control.CTRL_DOWN,
                             Char.CR);
            assertThat(input.readLine(), is("foo"));
        }
        assertThat(stripNonPrintable(console.output()), is(
                "" +
                "test: foo\r" +
                "Invalid control: <C-up>\n\r" +
                "test: foo\r" +
                "Invalid control: <C-down>\n\r" +
                "test: foo\r\n"));
    }

    @Test
    public void testCharValidator(Console console, Terminal terminal) {
        console.setInput('\t', 'a', '\n');
        try (var input = new InputLine(terminal, "test")) {
            assertThat(input.readLine(), is("a"));
        }
        assertThat(stripNonPrintable(console.output()),
                   is("test: " +
                      "\rInvalid character: '\\t'\n" +
                      "\rtest: a\r\n"));

        // Custom char validator.
        console.reset();
        console.setInput('æ', 'a', '\n');
        try (var input = new InputLine(terminal,
                                       "test",
                                       (c, lp) -> {
                                           if ('a' <= c.codepoint() && c.codepoint() <= 'z') {
                                               return true;
                                           }
                                           lp.println("Char: " + c);
                                           return false;
                                       },
                                       null,
                                       null)) {
            assertThat(input.readLine(), is("a"));
        }
        assertThat(stripNonPrintable(console.output()),
                   is("test: " +
                      "\rChar: æ\n" +
                      "\rtest: a\r\n"));
    }

    @Test
    public void testLineValidator(Console console, Terminal terminal) {
        console.setInput("\nb\n");
        try (var input = new InputLine(terminal,
                                       "test",
                                       null,
                                       (line, lp) -> {
                                           if (line.isEmpty()) {
                                               lp.println("Output needs at least 1 character.");
                                               return false;
                                           }
                                           return true;
                                       },
                                       null)) {
            assertThat(input.readLine(), is("b"));
        }
        assertThat(console.output(),
                   is("test: " +
                      "\r\033[KOutput needs at least 1 character.\n" +
                      "\r\033[Ktest: b\r\n"));

        // Custom char validator.
        console.reset();
        console.setInput("a\nb\n");
        try (var input = new InputLine(terminal,
                                       "test",
                                       null,
                                       (l, lp) -> {
                                           if (l.length() > 1) {
                                               return true;
                                           }
                                           lp.println("Line: " + l.length());
                                           return false;
                                       },
                                       null)) {
            assertThat(input.readLine(), is("ab"));
        }
        assertThat(console.output(),
                   is("test: a" +
                      "\r\033[KLine: 1\n" +
                      "\r\033[Ktest: ab\r\n"));

        // Specified initial value not valid.
        try (var input = new InputLine(terminal,
                                       "test",
                                       null,
                                       (l, lp) -> {
                                           if (l.length() > 1) {
                                               return true;
                                           }
                                           lp.println("Line: " + l.length());
                                           return false;
                                       },
                                       null)) {
            input.readLine("a");
            Assertions.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid initial value: a"));
        }
    }

    @Test
    @ConsoleMode(TTYMode.RAW)
    public void testLineValidator_ParentRaw(Console console, Terminal terminal) {
        console.setInput("\nb\n");
        try (var input = new InputLine(
                terminal,
                "test",
                null,
                (line, lp) -> {
                    if (line.isEmpty()) {
                        lp.println("Output needs at least 1 character.");
                        return false;
                    }
                    return true;
                },
                null)) {
            assertThat(input.readLine(), is("b"));
        }
        assertThat(console.output(),
                   is("test: " +
                      "\r\033[KOutput needs at least 1 character.\n" +
                      "\r\033[Ktest: b\r\n"));

        // Custom char validator.
        console.reset();
        console.setInput("a\nb\n");
        try (var input = new InputLine(terminal,
                                       "test",
                                       null,
                                       (l, lp) -> {
                                           if (l.length() > 1) {
                                               return true;
                                           }
                                           lp.println("Line: " + l.length());
                                           return false;
                                       },
                                       null)) {
            assertThat(input.readLine(), is("ab"));
        }
        assertThat(console.output(),
                   is("test: a" +
                      "\r\033[KLine: 1\n" +
                      "\r\033[Ktest: ab\r\n"));

        // Specified initial value not valid.
        try (var input = new InputLine(terminal,
                                       "test",
                                       null,
                                       (l, lp) -> {
                                           if (l.length() > 1) {
                                               return true;
                                           }
                                           lp.println("Line: " + l.length());
                                           return false;
                                       },
                                       null)) {
            input.readLine("a");
            Assertions.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid initial value: a"));
        }
    }

    @Test
    public void testTabCompletion(Console console, Terminal terminal) {
        List<String> vals = List.of(
                "editors",
                "edmonds");
        InputLine.TabCompletion completion = (pre, lp) -> {
            List<String> opts = vals.stream()
                                    .filter(s -> s.startsWith(pre))
                                    .collect(Collectors.toList());
            if (opts.size() == 1) {
                return opts.get(0);
            }
            if (opts.size() > 1) {
                lp.println("pre: " + String.join(", ", opts));
            }
            return null;
        };

        console.setInput("a\t", Char.BS, "e\tdi\t\n");
        try (var input = new InputLine(terminal,
                                       "test",
                                       null,
                                       null,
                                       completion)) {
            assertThat(input.readLine(), is("editors"));
        }
        assertThat(javaEscape(console.output()),
                   is(javaEscape(
                           "test: a\033[D\033[Ke" +
                           "\r\033[Kpre: editors, edmonds\n" +
                           "\r\033[Ktest: editors\r\n")));
    }

    @Test
    public void testInterrupt(Console console, Terminal terminal) {
        console.setInput("\033");
        try (var input = new InputLine(terminal, "test")) {
            input.readLine();
            fail();
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(),
                       is("java.io.IOException: User interrupted: <ESC>"));
        }
    }
}
