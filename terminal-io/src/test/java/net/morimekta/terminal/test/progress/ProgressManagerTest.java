/*
 * Copyright 2021 Terminal Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.terminal.test.progress;

import net.morimekta.strings.chr.Char;
import net.morimekta.strings.chr.CharUtil;
import net.morimekta.terminal.Terminal;
import net.morimekta.terminal.progress.DefaultSpinners;
import net.morimekta.terminal.progress.Progress;
import net.morimekta.terminal.progress.ProgressManager;
import net.morimekta.terminal.progress.Spinner;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import net.morimekta.testing.junit5.ConsoleExtension;
import net.morimekta.testing.junit5.ConsoleSize;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static net.morimekta.strings.chr.CharStream.lenientStream;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(ConsoleExtension.class)
@ConsoleDumpOutputOnFailure
@ConsoleDumpErrorOnFailure
@ConsoleSize(cols = 80)
public class ProgressManagerTest {
    private static class TestProgressManager extends ProgressManager {
        protected TestProgressManager(Terminal terminal,
                                      Spinner spinner) {
            super(terminal, spinner);
        }

        protected TestProgressManager(Terminal terminal,
                                      Spinner spinner,
                                      int maxTasks) {
            super(terminal, spinner, maxTasks);
        }

        @Override
        public List<String> lines() {
            return super.lines();
        }
    }

    private Console  console;
    private Terminal terminal;

    @BeforeEach
    public void setUp(Console console) throws IOException {
        this.console = console;
        this.terminal = new Terminal(console.tty());
    }

    @AfterEach
    public void tearDown() throws IOException {
        terminal.close();
    }

    @Test
    public void testSingleThread() throws InterruptedException, ExecutionException {
        ArrayList<ProgressManager.InternalTask<String>> started = new ArrayList<>();

        try (TestProgressManager progress = new TestProgressManager(
                terminal, DefaultSpinners.ASCII, 1)) {

            Future<String> first = progress.addTask(
                    "First",
                    (a, b) -> {
                        try {
                            Thread.sleep(100L);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        started.add((ProgressManager.InternalTask<String>) a);
                    });
            Future<String> second = progress.addTask(
                    "Second",
                    (a, b) -> started.add((ProgressManager.InternalTask<String>) a));

            assertThat(stripNonPrintableLines(progress.lines()),
                       is(List.of()));

            Thread.sleep(250L);  // does the render

            assertThat(
                    stripNonPrintableLines(progress.lines()),
                    is(List.of(
                            "First: [----------------------------------------------------]   0% |            ",
                            " -- And 1 more...")));

            assertThat(started, hasSize(1));

            started.get(0).onNext(new Progress(1000, 10000));

            Thread.sleep(250L);  // does the render

            assertThat(
                    stripNonPrintableLines(progress.lines()),
                    is(List.of(
                            "First: [#####-----------------------------------------------]  10% /            ",
                            " -- And 1 more...")));

            started.get(0).completeExceptionally(new Exception("Failed"));

            Thread.sleep(250L);  // does the render

            assertThat(stripNonPrintableLines(progress.lines()),
                       is(List.of(
                               "First: [#####-----------------------------------------------]  10% Failed       ",
                               "Second: [---------------------------------------------------]   0% |            ")));

            assertThat(started, hasSize(2));

            started.get(1).onNext(new Progress(1000, 10000));

            Thread.sleep(250L);  // does the render

            assertThat(stripNonPrintableLines(progress.lines()),
                       is(List.of(
                               "First: [#####-----------------------------------------------]  10% Failed       ",
                               "Second: [#####----------------------------------------------]  10% /            ")));

            started.get(1).complete("OK");

            Thread.sleep(250L);  // does the render

            try {
                first.get();
                fail("No exception");
            } catch (ExecutionException e) {
                assertThat(e.getCause().getMessage(), is("Failed"));
            }
            assertThat(second.get(), is("OK"));

            assertThat(stripNonPrintableLines(progress.lines()),
                       is(List.of(
                               "First: [#####-----------------------------------------------]  10% Failed       ",
                               "Second: [###################################################] 100% v @     0.5 s")));
        }
    }

    @Test
    public void testMultiThread() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        try (TestProgressManager progress = new TestProgressManager(terminal, DefaultSpinners.ASCII, 5)) {
            Future<String> first = progress.addTask("First", task -> {
                try {
                    Thread.sleep(75);
                    task.onNext(new Progress(1000, 10000));
                } catch (InterruptedException ignore) {
                }
                throw new RuntimeException("Failed");
            });
            Future<String> second = progress.addTask("Second", task -> {
                try {
                    Thread.sleep(75);
                    task.onNext(new Progress(1000, 10000));
                    Thread.sleep(150);
                    task.onNext(new Progress(10000, 10000));
                } catch (InterruptedException ignore) {
                }
                return "OK";
            });

            assertThat(String.join("\n", stripNonPrintableLines(progress.lines())),
                       is(""));

            Thread.sleep(40L);

            assertThat(String.join("\n", stripNonPrintableLines(progress.lines())),
                       is(""
                          + "First: [----------------------------------------------------]   0% |            \n"
                          + "Second: [---------------------------------------------------]   0% |            "));

            Thread.sleep(100L);

            assertThat(String.join("\n", stripNonPrintableLines(progress.lines())),
                       is(""
                          + "First: [#####-----------------------------------------------]  10% Failed       \n"
                          + "Second: [#####----------------------------------------------]  10% |            "));

            progress.waitAbortable();

            try {
                first.get(10L, MILLISECONDS);
                fail("No exception");
            } catch (ExecutionException e) {
                assertThat(e.getCause().getMessage(), is("Failed"));
            }
            assertThat(second.get(10L, MILLISECONDS), is("OK"));

            assertThat(String.join("\n", stripNonPrintableLines(progress.lines())),
                       is(""
                          + "First: [#####-----------------------------------------------]  10% Failed       \n"
                          + "Second: [###################################################] 100% v @     0.2 s"));
        }
    }

    @Test
    @SuppressWarnings("CatchAndPrintStackTrace")
    public void testAbort() throws InterruptedException, ExecutionException {
        OutputStream out = console.createInputSource();

        try (TestProgressManager progress = new TestProgressManager(terminal, DefaultSpinners.ASCII)) {
            Future<String> first = progress.addTask("First", task -> {
                Thread.sleep(20);
                task.onNext(new Progress(1000, 10000));
                throw new RuntimeException("Failed");
            });
            Future<String> second = progress.addTask("Second", task -> {
                Thread.sleep(50);
                task.onNext(new Progress(1000, 10000));
                Thread.sleep(520);
                task.onNext(new Progress(10000, 10000));
                return "OK";
            });

            assertThat(stripNonPrintableLines(progress.lines()),
                       is(List.of()));

            Executors.newSingleThreadExecutor().execute(() -> {
                try {
                    Thread.sleep(100L);
                    out.write(CharUtil.inputBytes(Char.ABR));
                    out.close();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace(System.err);
                }
            });

            try {
                progress.waitAbortable();
                fail("No exception");
            } catch (IOException e) {
                assertThat(e.getMessage(), is("Aborted with '<ABR>'"));
            }

            assertThat(first.isDone(), is(true));
            try {
                first.get();
                fail("No exception");
            } catch (ExecutionException e) {
                assertThat(e.getCause().getMessage(), is("Failed"));
            } catch (CancellationException e) {
                Thread.sleep(10L);
                e.printStackTrace(System.err);
                fail(e.getMessage());
            }

            assertThat(second.isCancelled(), is(true));
            try {
                String s = second.get();
                fail("No exception: " + s);
            } catch (CancellationException e) {
                // nothing to verify on exception.
            }

            assertThat(String.join("\n", stripNonPrintableLines(progress.lines())),
                       is(""
                          + "First: [#####-----------------------------------------------]  10% Failed       \n"
                          + "Second: [#####----------------------------------------------]  10% Cancelled    "));
        }
    }

    private static List<String> stripNonPrintableLines(List<String> lines) {
        return lines.stream()
                    .map(ProgressManagerTest::stripNonPrintable)
                    .collect(Collectors.toList());
    }

    private static String stripNonPrintable(String str) {
        return lenientStream(str)
                .filter(c -> c.printableWidth() > 0)
                .map(Objects::toString)
                .collect(Collectors.joining());
    }
}
