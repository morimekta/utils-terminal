module net.morimekta.terminal.test {
    exports net.morimekta.terminal.test;
    exports net.morimekta.terminal.test.input;
    exports net.morimekta.terminal.test.progress;
    exports net.morimekta.terminal.test.selection;

    requires net.morimekta.collect;

    requires transitive net.morimekta.strings;
    requires transitive net.morimekta.terminal;
    requires transitive net.morimekta.testing;
    requires transitive net.morimekta.testing.junit5;
    requires transitive net.morimekta.io;

    requires transitive org.junit.jupiter;
    requires transitive org.junit.jupiter.api;
    requires org.hamcrest;
    requires org.mockito.junit.jupiter;
    requires org.mockito;
    requires net.bytebuddy;
    requires net.bytebuddy.agent;

    requires java.logging;
}